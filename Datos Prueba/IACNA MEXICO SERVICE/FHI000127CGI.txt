folio= "000127"
serie= "HI"
fecha= "2013-09-28T20:47:08"
formaDePago= "PAGO EN UNA SOLA EXHIBICION"
tipoDeComprobante= "INVOICE"
condicionesDePago= "NET 30"
importeConLetra= " ( Sesenta y Seis Millones Trescientos Cincuenta Mil Doscientos Noventa y Nueve pesos 70/100 M.N.)"
metodoDePago= "TRANSFERENCIAS ELECTRONICAS DE FONDOS"
ordenCompra= " "
addenda= "10 "
rfcEmisor= "IMS070226LF6"
nombreEmisor= "IACNA Mexico Service Company"
calleEmisor= "Calle Halcon"
noExtEmisor= "2 "
coloniaEmisor= "Parque Industrial FINSA "
municipioEmisor= "Ramos Arizpe "
estadoEmisor= "COAH "
paisEmisor= "MEXICO"
cpEmisor= "25900"
rfcReceptor= "IHE070904PW4"
nombreReceptor= "IACNA Hermosillo S RL de CV"
calleReceptor= "Calle Halcon"
noExtReceptor= "2 "
coloniaReceptor= "Parque Industrial FINSA "
municipioReceptor= "Ramos Arizpe "
estadoReceptor= "COAH "
paisReceptor= "MEXICO"
cpReceptor= "25904 "
rfcEnviarA= "IHE070904PW4"
nombreEnviarA= "IACNA Hermosillo S RL de CV"
calleEnviarA= "Calle Halcon "
noExtEnviarA= "2 "
coloniaEnviarA= "Parque Industrial FINSA "
municipioEnviarA= "Ramos Arizpe "
estadoEnviarA= "COAH "
paisEnviarA= "MEXICO"
cpEnviarA= "25904"
numProveedor= ""
numSucursal= " "
division= "VW"
cantidad[1]= "        1.00"
unidad[1]= "PZA"
sku[1]= " "
noIdentificacion[1]= "Fact Service"
descripcion[1]= "Mes Septiembre"
precioNeto[1]= "57198534.2200"
importeNeto[1]= "  57198534.22"
precioBruto[1]= "57198534.2200"
vendorPack[1]= ""
importeBruto[1]= "  57198534.22"
partNum[1]= "Fact Service"
partFechaRecibo[1]= "2013-09-28"
ordenCompra[1]= ""
Amendment[1]= ""
codigoCargosdet[1]= ""
billOfLading="HI000127"
packingList="HI000127"
subTotalAD="   57198534.22"
subTotalDD="   66350299.70"
Total= "   66350299.70"
porcentajeImpuesto="16"
importeImpuesto= "    9151765.48 "
observaciones=" "
CadOriginal=" "
Sello=" "
NoAprob=" "
AnioAprob=" "
NoCert=" "
PrinterName="AS_FACTURA "
tipoAddenda="PPR"
tipoDoc="FA"
version="1.0"
referenciaProveedor="Einvher1"
cancelNum=" "
strTipoCambio="1"
proveedorCodigo=""
proveedorNombre="IACNA Mexico Service Company"
proveedorSufijo=" "
origenCodigo=""
origenNombre=""
origenSufijo=" "
destinoCodigo="Einvher1"
destinoNombre=""
destinoSufijo=" "
receivingCodigo=" "
receivingNombre=" "
receivingSufijo=" "
deptNum=" "
cuentaNum=" "
nota=" "
refChrysler=" "
consecutivo=" "
montoLinea=" "
factura=" "
codigoCargos="V6"
montoCargo="    9151765.48"
transporte=" | | "
divisa="PSO"
tipoDeCambio="  1.0000"
ImpuestoRet="          0.00"
nombreExpedidoEn="IACNA Mexico Service Company"
calleExpedidoEn="Calle Halcon"
noExtExpedidoEn="2 "
coloniaExpedidoEn="Parque Industrial FINSA "
municipioExpedidoEn="Ramos Arizpe "
estadoExpedidoEn="COAH "
paisExpedidoEn="MEXICO"
cpExpedidoEn="25900"
shipper="HI000127"
Path_File=""
plantaEmisor="IACNA"
totalPesos="   66350299.70"
impuestoPesos="    9151765.48"
numeroCliente="Einvher1"
documentStatus="ORIGINAL"
alternatePartyIdentification="SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY"
referenceIdentification="ON"
referenceIdentificationtype="999"
AdditionalInformation="ACE"
AdditionalInformationType="HI000127"
correoContacto="bquintero@iacgroup.com"
nombreSolicitante=""
correoSolicitante=""
NumCtaPago="9397"
RegimenFiscal="Regimen de Consolidacion"
LugarExpedicion="Ramos Arizpe,COAH,MX,25900"
