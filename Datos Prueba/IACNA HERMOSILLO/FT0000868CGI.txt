folio= "0000868"
serie= "T"
fecha= "2013-10-10T15:30:30"
formaDePago= "PAGO EN UNA SOLA EXHIBICION"
tipoDeComprobante= "INVOICE"
condicionesDePago= "NET 45"
importeConLetra= " ( Ciento Cuarenta y Nueve Mil Setecientos Setenta Dolares 00/100 USD )"
metodoDePago= "TRANSFERENCIAS ELECTRONICAS DE FONDOS"
ordenCompra= " "
addenda= "10 "
rfcEmisor= "IHE070904PW4"
nombreEmisor= "IACNA Hermosillo S de RL de"
calleEmisor= "Blvd. Henry Ford"
noExtEmisor= "33 "
coloniaEmisor= "Parq Industrial Dynatech Sur "
municipioEmisor= "Hermosillo "
estadoEmisor= "SON "
paisEmisor= "MEXICO"
cpEmisor= "83299"
rfcReceptor= "FMO8304236C5"
nombreReceptor= "FORD MOTOR COMPANY SA DE CV"
calleReceptor= "GUILLERMO GONZALEZ C #1500"
noExtReceptor= "CENTRO SANTA FE "
coloniaReceptor= "MEXICO "
municipioReceptor= "MEXICO "
estadoReceptor= "DF "
paisReceptor= "MEXICO"
cpReceptor= "01210 "
rfcEnviarA= "FMO8304236C5"
nombreEnviarA= "FORD MOTOR COMPANY SA DE CV"
calleEnviarA= "GUILLERMO GONZALEZ C #1500 "
noExtEnviarA= "CENTRO SANTA FE "
coloniaEnviarA= "MEXICO "
municipioEnviarA= "MEXICO "
estadoEnviarA= "DF "
paisEnviarA= "MEXICO"
cpEnviarA= "01210"
numProveedor= ""
numSucursal= " "
division= "VW"
cantidad[1]= "        1.00"
unidad[1]= "PZA"
sku[1]= " "
noIdentificacion[1]= "T925821-00-001"
descripcion[1]= "Refurbish 2 Injectio"
precioNeto[1]= "  149770.0000"
importeNeto[1]= "    149770.00"
precioBruto[1]= "  149770.0000"
vendorPack[1]= ""
importeBruto[1]= "    149770.00"
partNum[1]= "T925821-00-001"
partFechaRecibo[1]= "2013-10-10"
ordenCompra[1]= "925821"
Amendment[1]= ""
codigoCargosdet[1]= ""
billOfLading="T0000868"
packingList="T0000868"
subTotalAD="     149770.00"
subTotalDD="     173733.20"
Total= "     149770.00"
porcentajeImpuesto="16"
importeImpuesto= "      23963.20 "
observaciones=" "
CadOriginal=" "
Sello=" "
NoAprob=" "
AnioAprob=" "
NoCert=" "
PrinterName="AS_FACTURA "
tipoAddenda="TOO"
tipoDoc="FA"
version="1.0"
referenciaProveedor="T10001"
cancelNum=" "
strTipoCambio="13.1417"
proveedorCodigo=""
proveedorNombre="IACNA Hermosillo S de RL de"
proveedorSufijo=" "
origenCodigo=""
origenNombre="HERMOSILLO"
origenSufijo=" "
destinoCodigo="T10001"
destinoNombre=""
destinoSufijo=" "
receivingCodigo=" "
receivingNombre=" "
receivingSufijo=" "
deptNum=" "
cuentaNum=" "
nota="Pedimento 2144460, Item 925821-00-002 is canceled. "
refChrysler=" "
consecutivo=" "
montoLinea=" "
factura=" "
codigoCargos="V6"
montoCargo="      23963.20"
transporte="LING"
divisa="USD"
tipoDeCambio=" 13.1417"
ImpuestoRet="      23963.20"
nombreExpedidoEn="IACNA Hermosillo S de RL de"
calleExpedidoEn="Blvd. Henry Ford"
noExtExpedidoEn="33 "
coloniaExpedidoEn="Parq Industrial Dynatech Sur "
municipioExpedidoEn="Hermosillo "
estadoExpedidoEn="SON "
paisExpedidoEn="MEXICO"
cpExpedidoEn="83299"
shipper="T0000868"
Path_File=""
plantaEmisor="HER"
totalPesos="    1968232.41"
impuestoPesos="     314917.19"
numeroCliente="T10001"
documentStatus="ORIGINAL"
alternatePartyIdentification="SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY"
referenceIdentification="ON"
referenceIdentificationtype="999"
AdditionalInformation="ACE"
AdditionalInformationType="T0000868"
correoContacto="fcarral@iacgroup.com"
nombreSolicitante="ANGELITA VALDEZ"
correoSolicitante=""
NumCtaPago="4616"
RegimenFiscal="Regimen de Consolidacion"
LugarExpedicion="Hermosillo,SON,MX,83299"
