﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using ASFactura.IacnaActualizadorBd.Model;

namespace ASFactura.IacnaActualizadorBd
{
    class Program
    {
        static void Main(string[] args)
        {
            var asFacturaDb = ConfigurationManager.AppSettings["ASFacturaDb"];
            var consulta = "SELECT DocumentoId, Cfdi FROM Documento WHERE Estatus_Valor = 2 AND Version = '3.2' AND Fecha >= '" + ConfigurationManager.AppSettings["FechaInicial"] + "' AND (Moneda is null OR TipoCambio is null) ORDER BY Fecha";
            Console.WriteLine("Inicia proceso de actualización");
            try
            {
                var documentos = new DataTable();
                using (var cn = new SqlConnection(asFacturaDb))
                using (var command = new SqlCommand(consulta, cn))
                {
                    cn.Open();
                    command.ExecuteNonQuery();
                    documentos.Load(command.ExecuteReader());

                    Console.WriteLine("No. Documentos a afectar: " + documentos.Rows.Count);
                    Console.WriteLine("Procesando...");

                    foreach (DataRow documento in documentos.Rows)
                    {
                        var documentoId = documento["DocumentoId"];
                        var cfdi = documento["Cfdi"].ToString();
                        var serializer = new XmlSerializer(typeof (Comprobante));

                        using (var str = new MemoryStream(Encoding.UTF8.GetBytes(cfdi)))
                        {
                            var comprobante = (Comprobante) serializer.Deserialize(str);
                            str.Close();
                            
                            var actualizacion = string.Format("UPDATE Documento SET Moneda = '{0}', TipoCambio = '{1}' WHERE DocumentoId = {2} AND Estatus_Valor = 2", !string.IsNullOrEmpty(comprobante.Moneda) ? comprobante.Moneda : "", !string.IsNullOrEmpty(comprobante.TipoCambio) ? comprobante.TipoCambio : "", documentoId);
                            using (var upCommand = new SqlCommand(actualizacion, cn))
                            {
                                upCommand.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var applicationEventLog = new EventLog { Source = "AsFactura" };
                applicationEventLog.WriteEntry(string.Format("Error al actualizar --> Argumentos: {0}, Mensaje: {1}.", String.Join(",", args), ex.Message), EventLogEntryType.Error);
            }

            Console.WriteLine("Termina proceso de actualización");
            Console.ReadKey();
        }
    }
}