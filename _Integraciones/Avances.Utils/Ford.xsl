﻿<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <xsl:template match="/">
    <xsl:element name="fomadd:addenda" xmlns:fomadd="http://www.ford.com.mx/cfdi/addenda">
      <xsl:attribute name="xsi:schemaLocation">http://www.ford.com.mx/cfdi/addenda http://www.ford.com.mx/cfdi/addenda/cfdiAddendaFord_1_0.xsd</xsl:attribute>
      <!--xsi:schemaLocation="http://www.ford.com.mx/cfdi/addenda http://www.ford.com.mx/cfdi/addenda/cfdiAddendaFord_1_0.xsd">-->
      <xsl:element name="fomadd:FOMASN">
        <xsl:attribute name="version">
          <xsl:text>1.0</xsl:text>
        </xsl:attribute>
        <xsl:element name="fomadd:GSDB">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@numProveedor"/>
        </xsl:element>
        <xsl:element name="fomadd:ASN">
          <xsl:value-of select="/Avances/Otros/@asn"/>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>