<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:GPC="http://www.honda.net.mx/GPC">
  <xsl:template match="/">
    <xsl:element name="GPC:Honda">
      <xsl:if test ="/Avances/Encabezado/Comprobante/@tipoDeComprobante">
        <xsl:attribute name="tipoComprobante">
          <xsl:choose>
            <xsl:when test="(/Avances/Encabezado/Comprobante/@tipoDeComprobante='INVOICE')">
              <xsl:text>ingreso</xsl:text>
            </xsl:when>
            <xsl:when test="(/Avances/Encabezado/Comprobante/@tipoDeComprobante='CREDIT_NOTE')">
              <xsl:text>egreso</xsl:text>
            </xsl:when>
            <xsl:when test="(/Avances/Encabezado/Comprobante/@tipoDeComprobante='DEBIT_NOTE')">
              <xsl:text>ingreso</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoDeComprobante"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="tipoDocumento">
        <xsl:value-of select="/Avances/Otros/@tipoAddenda"/>
      </xsl:attribute>
      <xsl:if test ="/Avances/Encabezado/Moneda/@codigoISO">
        <xsl:attribute name="moneda">
          <xsl:value-of select="/Avances/Otros/@divisa"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test ="substring(/Avances/Encabezado/Comprobante/@fecha,0,11)">
        <xsl:attribute name="fecha">
          <xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,1,4)"/>
          <xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,6,2)"/>
          <xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,9,2)"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test ="/Avances/Encabezado/Comprobante/@folio">
        <xsl:attribute name="folio">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="PlantCode">
        <xsl:text>HCL</xsl:text>
      </xsl:attribute>
      <xsl:if test ="/Avances/Otros/@asnHonda">
        <xsl:attribute name="ASNNumber">
          <xsl:value-of select="/Avances/Otros/@asnHonda"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:element name="GPC:Proveedor">
        <xsl:attribute name="rfc">
          <xsl:value-of select="/Avances/Encabezado/Emisor/@rfc"/>
        </xsl:attribute>
        <xsl:attribute name="numeroProveedor">
          <xsl:value-of select="/Avances/Otros/@numProveedor"/>
        </xsl:attribute>
        <xsl:attribute name="ShipTo">
          <xsl:value-of select="/Avances/Otros/@destinoCodigo"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:element name="GPC:Conceptos">
        <xsl:for-each select="/Avances/Detalles/Detalle">
          <xsl:element name="GPC:Concepto">
            <xsl:if test ="@importeNeto">
              <xsl:attribute name="importe">
                <xsl:value-of select='format-number(@importeBruto, "#.00")'/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test ="@precioNeto">
              <xsl:attribute name="valorUnitario">
                <xsl:value-of select='format-number(@precioNeto, "#.0000")'/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test ="@descripcion">
              <xsl:attribute name="descripcion">
                <xsl:value-of select="@descripcion"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test ="@cantidad">
              <xsl:attribute name="cantidad">
                <xsl:value-of select="@cantidad"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test ="@noIdentificacion">
              <xsl:attribute name="partnumber">
                <xsl:if test ="string-length(@noIdentificacion) &gt; 15">
                  <xsl:value-of select="substring(@noIdentificacion, 1, 15)"/>
                </xsl:if>
                <xsl:if test ="string-length(@noIdentificacion) &lt; 16">
                  <xsl:value-of select="@noIdentificacion"/>
                </xsl:if>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="nolinea">
              <xsl:value-of select="position()"/>
            </xsl:attribute>
            <xsl:if test="@partcolor">
              <xsl:if test ="string-length(@partcolor) &gt; 0">
                <xsl:attribute name="partcolor">
                  <xsl:value-of select="@partcolor"/>
                </xsl:attribute>
              </xsl:if>
            </xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>