<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="ADDENDAGM">
      <xsl:element name="HEADER">
        <xsl:if test ="/Avances/Encabezado/Comprobante/@folio">
          <xsl:element name="NUMEROREMISION">
            <!--<xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>-->
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test ="/Avances/Encabezado/Comprobante/@fecha">
          <xsl:element name="FECHARECIBO">
            <xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,9,2)"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,6,2)"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,1,4)"/>
          </xsl:element>
        </xsl:if>
        <xsl:if test ="/Avances/Otros/@asnHonda">
          <xsl:element name="FOLIOINTERNO">
            <!--<xsl:value-of select="/Avances/Otros/@asnHonda"/>-->
            <xsl:value-of select="/Avances/Otros/@asn"/>
          </xsl:element>
        </xsl:if>
        <!--<xsl:if test ="/Avances/Encabezado/Moneda/@codigoISO">-->
        <xsl:if test ="/Avances/Otros/@refChrysler">
          <xsl:element name="MONEDA">
            <!--<xsl:choose>
              <xsl:when test="(/Avances/Otros/@refChrysler='1')">
                <xsl:text>MXN</xsl:text>
              </xsl:when>
              <xsl:when test="(/Avances/Otros/@refChrysler='2')">
                <xsl:text>USD</xsl:text>
              </xsl:when>
            </xsl:choose>-->
            <xsl:value-of select="/Avances/Otros/@refChrysler"/>
          </xsl:element>
        </xsl:if>
        <xsl:for-each select="/Avances/Detalles/Detalle">
          <xsl:element name="ITEM">
            <xsl:if test ="/Avances/Detalles/Detalle/@ordenCompra">
              <xsl:element name="ORDENCOMPRA">
                <xsl:value-of select="@ordenCompra"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test ="/Avances/Detalles/Detalle/@noIdentificacion">
              <xsl:element name="NUMEROPARTE">
                <xsl:value-of select="@noIdentificacion"/>
              </xsl:element>
            </xsl:if>
            <!--<xsl:if test ="/Avances/Encabezado/Comprobante/@folio">-->
              <xsl:element name="MATERIAL">
                <!--<xsl:text>1</xsl:text>-->
                <xsl:value-of select="/Avances/Otros/@depNum"/>
              </xsl:element>
            <!--</xsl:if>-->
            <xsl:if test ="/Avances/Detalles/Detalle/@cantidad">
              <xsl:element name="CANTIDAD">
                <xsl:value-of select="@cantidad"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test ="/Avances/Detalles/Detalle/@precioNeto">
              <xsl:element name="PRECIOUNITARIO">
                <xsl:value-of select="@precioNeto"/>
              </xsl:element>
            </xsl:if>
            <xsl:if test ="/Avances/Detalles/Detalle/@descripcion">
              <xsl:element name="DESCRIPCION">
                <xsl:value-of select="@descripcion"/>
              </xsl:element>
            </xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
