﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:PMT="http://www.sas-automotive.com/en/locations/local-offices-and-plants/mexico/plant-puebla.html">
  <xsl:template match="/">
    <xsl:element name="PMT:Factura">
      <xsl:attribute name="version">
        <xsl:text>1.0</xsl:text>
      </xsl:attribute>
      <xsl:element name="PMT:Moneda">
        <xsl:attribute name="tipoMoneda">
          <xsl:value-of select="/Avances/Otros/@divisa"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:element name="PMT:Proveedor">
        <xsl:attribute name="codigo">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@numProveedor"/>
        </xsl:attribute>
        <xsl:attribute name="nombre">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@proveedorNombre"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:element name="PMT:Referencias">
        <xsl:attribute name="referenciaProveedor">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:element name="PMT:Partes">
        <xsl:for-each select="/Avances/Detalles/Detalle">
          <xsl:element name="PMT:Parte">
            <xsl:attribute name="posicion">
              <xsl:value-of select="position()"/>
            </xsl:attribute>
            <xsl:attribute name="numeroMaterial">
              <xsl:value-of select="@noIdentificacion"/>
            </xsl:attribute>
            <xsl:attribute name="descripcionMaterial">
              <xsl:value-of select="@descripcion"/>
            </xsl:attribute>
            <xsl:if test="(string-length(@ordenCompra) &gt; 0) and (string-length(/Avances/Otros/@shipper) &gt; 0)">
              <xsl:element name="PMT:Referencias">
                <xsl:attribute name="ordenCompra">
                  <xsl:value-of select="@ordenCompra"/>
                </xsl:attribute>
                <xsl:attribute name="numeroASN">
                  <xsl:value-of select="/Avances/Otros/@shipper"/>
                </xsl:attribute>
              </xsl:element>
            </xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>