﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <xsl:element name="cfdi:Comprobante" namespace="http://www.sat.gob.mx/cfd/3">
      <xsl:attribute name="xsi:schemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">
        <xsl:text>http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="version">
        <xsl:text>3.2</xsl:text>
      </xsl:attribute>
      <xsl:if test="/Avances/Encabezado/Comprobante/@serie">
        <xsl:attribute name="serie">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/Avances/Encabezado/Comprobante/@folio">
        <xsl:attribute name="folio">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="fecha">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@fecha"/>
      </xsl:attribute>
      <xsl:attribute name="sello">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="formaDePago">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@formaDePago"/>
      </xsl:attribute>
      <xsl:attribute name="tipoDeComprobante">
        <xsl:choose>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'INVOICE1'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'INVOICE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'DEBIT_NOTE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CREDIT_NOTE'">
            <xsl:text>egreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'NOTA DE CREDITO'">
            <xsl:text>egreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CHARGE_NOTE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'HONORARY_RECIPT'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'PARTIAL_INVOICE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'TRANSPORT_DOCUMENT'">
            <xsl:text>traslado</xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>      
      <xsl:attribute name="noCertificado">
        <xsl:text>00000000000000000000</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="certificado">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
      <xsl:if test="string-length(/Avances/Encabezado/Comprobante/@condicionesDePago) &gt; 0">
        <xsl:attribute name="condicionesDePago">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@condicionesDePago"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="subTotal">
        <xsl:value-of select="/Avances/Totales/@subTotalantesDeDescuentos"/>
      </xsl:attribute>
      <xsl:if test="/Avances/Totales/Descuento">
        <xsl:attribute name="descuento">
          <xsl:value-of select="/Avances/Totales/@importe"/>
        </xsl:attribute>
        <xsl:attribute name="motivoDescuento">
          <xsl:value-of select="/Avances/Totales/@motivo"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="/Avances/Encabezado/Moneda">
        <xsl:attribute name="TipoCambio">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@tipoCambio"/>
        </xsl:attribute>
        <xsl:attribute name="Moneda">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="total">
        <xsl:value-of select="/Avances/Totales/@total"/>
      </xsl:attribute>
      <xsl:attribute name="metodoDePago">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@metodoDePago"/>
      </xsl:attribute>
      <xsl:attribute name="LugarExpedicion">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@LugarExpedicion"/>
      </xsl:attribute>
      <xsl:if test="string-length(/Avances/Encabezado/Comprobante/@numctapago) &gt; 0">
        <xsl:attribute name="NumCtaPago">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@numCtaPago"/>
        </xsl:attribute>
      </xsl:if>
      <!--<xsl:attribute name="tipoDeComprobante">
				<xsl:choose>
					<xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'INVOICE' or
										/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'DEBIT_NOTE' or
										/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'LEASE_NOTE' or
										/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'HONORARY_RECIPT' or
										/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'PARTIAL_INVOICE' ">
						<xsl:text>ingreso</xsl:text>
					</xsl:when>
					<xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CREDIT_NOTE' ">
						<xsl:text>egreso</xsl:text>
					</xsl:when>
					<xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'TRANSPORT_DOCUMENT' ">
						<xsl:text>traslado</xsl:text>
					</xsl:when>
				</xsl:choose>
			</xsl:attribute>-->
      <xsl:apply-templates />
    </xsl:element>
  </xsl:template>
  <xsl:template match="Emisor">
    <xsl:element name="cfdi:Emisor"  namespace="http://www.sat.gob.mx/cfd/3">
      <xsl:attribute name="rfc">
        <xsl:value-of select="@rfc"/>
      </xsl:attribute>
      <xsl:attribute name="nombre">
        <xsl:value-of select="@nombre"/>
      </xsl:attribute>
      <xsl:element name="cfdi:DomicilioFiscal" namespace="http://www.sat.gob.mx/cfd/3">
        <xsl:call-template name="DomicilioFiscal">
          <xsl:with-param name="direccion" select="../Emisor"/>
        </xsl:call-template>
      </xsl:element>
      <xsl:if test="../ExpedidoEn">
        <xsl:element name="cfdi:ExpedidoEn" namespace="http://www.sat.gob.mx/cfd/3">
          <xsl:call-template name="Domicilio">
            <xsl:with-param name="direccion" select="../ExpedidoEn"/>
          </xsl:call-template>
        </xsl:element>
        <xsl:element name="cfdi:RegimenFiscal" namespace="http://www.sat.gob.mx/cfd/3">
          <xsl:attribute name="Regimen">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@Regimen"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>
  <xsl:template match="Receptor">
    <xsl:element name="cfdi:Receptor"  namespace="http://www.sat.gob.mx/cfd/3">
      <xsl:attribute name="rfc">
        <xsl:value-of select="/Avances/Encabezado/Receptor/@rfc"/>
      </xsl:attribute>
      <xsl:if test="/Avances/Encabezado/Receptor/@nombre">
        <xsl:attribute name="nombre">
          <xsl:value-of select="/Avances/Encabezado/Receptor/@nombre"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:element name="cfdi:Domicilio" namespace="http://www.sat.gob.mx/cfd/3">
        <xsl:call-template name="Domicilio">
          <xsl:with-param name="direccion" select="../Receptor"/>
        </xsl:call-template>
      </xsl:element>
    </xsl:element>
  </xsl:template>

  <xsl:template match="Detalles">
    <xsl:element name="cfdi:Conceptos"  namespace="http://www.sat.gob.mx/cfd/3">
      <xsl:for-each select="Detalle">
        <xsl:element name="cfdi:Concepto"  namespace="http://www.sat.gob.mx/cfd/3">
          <xsl:attribute name="cantidad">
            <xsl:value-of select="@cantidad"/>
          </xsl:attribute>
          <xsl:if test="@unidad">
            <xsl:attribute name="unidad">
              <xsl:value-of select="@unidad"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test ="@noIdentificacion">
            <xsl:attribute name="noIdentificacion">
              <xsl:value-of select="@noIdentificacion"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="descripcion">
            <xsl:value-of select="@descripcion"/>
          </xsl:attribute>
          <xsl:attribute name="valorUnitario">
            <xsl:value-of select="@precioBruto"/>
          </xsl:attribute>
          <xsl:attribute name="importe">
            <xsl:value-of select="@importeBruto"/>
          </xsl:attribute>
          <xsl:for-each select="InformacionAduanera">
            <xsl:element name="cfdi:InformacionAduanera" namespace="http://www.sat.gob.mx/cfd/3">
              <xsl:attribute name="numero">
                <xsl:value-of select="@numero"/>
              </xsl:attribute>
              <xsl:attribute name="fecha">
                <xsl:value-of select="@fecha"/>
              </xsl:attribute>
              <xsl:attribute name="aduana">
                <xsl:value-of select="@nombre"/>
              </xsl:attribute>
            </xsl:element>
          </xsl:for-each>
        </xsl:element>
      </xsl:for-each>
    </xsl:element>
  </xsl:template>

  <xsl:template match="Totales">
    <!--<xsl:element name="cfdi:Impuestos"  namespace="http://www.sat.gob.mx/cfd/3">
			<xsl:if test="Impuesto[@categoria='RETENCION']">
				<xsl:element name="cfdi:Retenciones" namespace="http://www.sat.gob.mx/cfd/3">
					<xsl:for-each select="Impuesto[@categoria='RETENCION']">
						<xsl:element name="cfdi:Retencion"  namespace="http://www.sat.gob.mx/cfd/3">
							<xsl:attribute name ="impuesto">
								<xsl:value-of select="@tipo"/>
							</xsl:attribute>
							<xsl:attribute name="importe">
								<xsl:value-of select="@importe"/>
							</xsl:attribute>
						</xsl:element>	
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
			<xsl:if test="Impuesto[@categoria='TRASLADO']">
				<xsl:element name="cfdi:Traslados" namespace="http://www.sat.gob.mx/cfd/3">
					<xsl:for-each select="Impuesto[@categoria='TRASLADO']">
						<xsl:element name="cfdi:Traslado"  namespace="http://www.sat.gob.mx/cfd/3">
							<xsl:attribute name ="impuesto">
								<xsl:value-of select="@tipo"/>
							</xsl:attribute>
							<xsl:attribute name ="tasa">
								<xsl:value-of select="@porcentaje"/>
							</xsl:attribute>
							<xsl:attribute name="importe">
								<xsl:value-of select="@importe"/>
							</xsl:attribute>
						</xsl:element>	
					</xsl:for-each>
				</xsl:element>
			</xsl:if>
      <xsl:if test="Impuesto[@categoria='TRANSFERIDO']">
        <xsl:element name="cfdi:Traslados" namespace="http://www.sat.gob.mx/cfd/3">
          <xsl:for-each select="Impuesto[@categoria='TRANSFERIDO']">
            <xsl:element name="cfdi:Traslado"  namespace="http://www.sat.gob.mx/cfd/3">
              <xsl:attribute name ="impuesto">
                <xsl:value-of select="@tipo"/>
              </xsl:attribute>
              <xsl:attribute name ="tasa">
                <xsl:value-of select="@porcentaje"/>
              </xsl:attribute>
              <xsl:attribute name="importe">
                <xsl:value-of select="@importe"/>
              </xsl:attribute>
            </xsl:element>
          </xsl:for-each>
        </xsl:element>
      </xsl:if>
		</xsl:element>-->
    <xsl:element name="cfdi:Impuestos" namespace="http://www.sat.gob.mx/cfd/3">
      <xsl:if test ="string-length(/Avances/Totales/@importeImpuestoRetencion) &gt; 0">
        <xsl:attribute name="totalImpuestosRetenidos">
          <xsl:value-of select="/Avances/Totales/@importeImpuestoRetencion"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test ="string-length(/Avances/Totales/@importeImpuesto) &gt; 0">
        <xsl:attribute name="totalImpuestosTrasladados">
          <xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
        </xsl:attribute>
      </xsl:if>
      <!--<xsl:element name="Retenciones">
          <xsl:element name="Retencion">
            <xsl:attribute name ="impuesto">
              <xsl:text>IVA</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="importe">
              <xsl:choose>
                <xsl:when test ="string-length(/Avances/Totales/@importeImpuestoRetencion) &gt; 0">
                  <xsl:value-of select="/Avances/Totales/@importeImpuestoRetencion"/>
                  </xsl:when>
                <xsl:otherwise>
                  <xsl:text> 0.00 </xsl:text>
                </xsl:otherwise>
              </xsl:choose> 
            </xsl:attribute>
          </xsl:element>
        </xsl:element>-->
      <xsl:element name="cfdi:Traslados" namespace="http://www.sat.gob.mx/cfd/3">
        <!--<xsl:element name="cfdi:Traslado" namespace="http://www.sat.gob.mx/cfd/3">
          <xsl:attribute name="impuesto">
            <xsl:text>IEPS</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="tasa">
            <xsl:text> 0.00 </xsl:text>
          </xsl:attribute>
          <xsl:attribute name="importe">
            <xsl:text> 0.00 </xsl:text>
          </xsl:attribute>
        </xsl:element>-->
        <xsl:element name="cfdi:Traslado" namespace="http://www.sat.gob.mx/cfd/3">
          <xsl:attribute name="impuesto">
            <xsl:text>IVA</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="tasa">
            <xsl:value-of select="/Avances/Totales/@porcentajeImpuesto"/>
          </xsl:attribute>
          <xsl:attribute name="importe">
            <xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:element>
    </xsl:element>
    <xsl:element name="cfdi:Complemento" namespace="http://www.sat.gob.mx/cfd/3"></xsl:element>
  </xsl:template>



  <xsl:template name="Domicilio">
    <xsl:param name="direccion"/>
    <xsl:if test="string-length($direccion/@calle) &gt; 0">
      <xsl:attribute name="calle">
        <xsl:value-of select="$direccion/@calle"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@noExterior) &gt; 0">
      <xsl:attribute name="noExterior">
        <xsl:value-of select="$direccion/@noExterior"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@noInterior) &gt; 0">
      <xsl:attribute name="noInterior">
        <xsl:value-of select="$direccion/@noInterior"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@colonia) &gt; 0">
      <xsl:attribute name="colonia">
        <xsl:value-of select="$direccion/@colonia"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@localidad) &gt; 0">
      <xsl:attribute name="localidad">
        <xsl:value-of select="$direccion/@localidad"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@referencia) &gt; 0">
      <xsl:attribute name="referencia">
        <xsl:value-of select="$direccion/@referencia"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@municipio) &gt; 0">
      <xsl:attribute name="municipio">
        <xsl:value-of select="$direccion/@municipio"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@estado) &gt; 0">
      <xsl:attribute name="estado">
        <xsl:value-of select="$direccion/@estado"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="pais">
      <xsl:value-of select="$direccion/@pais"/>
    </xsl:attribute>
    <xsl:if test="string-length($direccion/@codigoPostal) &gt; 0">
      <xsl:attribute name="codigoPostal">
        <xsl:value-of select="$direccion/@codigoPostal"/>
      </xsl:attribute>
    </xsl:if>
  </xsl:template>
  <xsl:template name="DomicilioFiscal">
    <xsl:param name="direccion"/>
    <xsl:attribute name="calle">
      <xsl:value-of select="$direccion/@calle"/>
    </xsl:attribute>
    <xsl:if test="string-length($direccion/@noExterior) &gt; 0">
      <xsl:attribute name="noExterior">
        <xsl:value-of select="$direccion/@noExterior"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@noInterior) &gt; 0">
      <xsl:attribute name="noInterior">
        <xsl:value-of select="$direccion/@noInterior"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@colonia) &gt; 0">
      <xsl:attribute name="colonia">
        <xsl:value-of select="$direccion/@colonia"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@localidad) &gt; 0">
      <xsl:attribute name="localidad">
        <xsl:value-of select="$direccion/@localidad"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="string-length($direccion/@referencia) &gt; 0">
      <xsl:attribute name="referencia">
        <xsl:value-of select="$direccion/@referencia"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="municipio">
      <xsl:value-of select="$direccion/@municipio"/>
    </xsl:attribute>
    <xsl:attribute name="estado">
      <xsl:value-of select="$direccion/@estado"/>
    </xsl:attribute>
    <xsl:attribute name="pais">
      <xsl:value-of select="$direccion/@pais"/>
    </xsl:attribute>
    <xsl:attribute name="codigoPostal">
      <xsl:value-of select="$direccion/@codigoPostal"/>
    </xsl:attribute>
  </xsl:template>
  <xsl:template name="ToLower">
    <xsl:param name="inputString"/>
    <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyz'"/>
    <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
    <xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
  </xsl:template>
</xsl:stylesheet>