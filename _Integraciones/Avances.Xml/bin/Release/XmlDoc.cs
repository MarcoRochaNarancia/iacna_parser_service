using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;

namespace Avances.Xml
{
    /// <summary>
    /// Esta clase representa un xml.
    /// Contiene metodos para lectura/escritura tanto de atributos como de elementos. Asi como para usar Transformaciones y validaciones contra Schemas.
    /// </summary>
    public class XmlDoc
    {
        public XmlDocument xdoc;
        private int detalles;
        /// <summary>
        /// Inicializa una nueva instancia de la clase XmlDoc con el archivo xml especificado.
        /// </summary>
        /// <param name="xmlFile">El xml que se ca a cargar</param>
        public XmlDoc(string xmlFile)
        {
            xdoc = new XmlDocument();
            if (xmlFile.Contains("<"))
                xdoc.LoadXml(xmlFile);
            else
                xdoc.Load(xmlFile);
            XmlNode root = xdoc.DocumentElement;
            XmlNodeList nodel = root.SelectNodes("child::*/child::Detalle");
            detalles = nodel.Count;
        }
        /// <summary>
        /// Numero de elementos Concepto que contiene el xml.
        /// </summary>
        public int NumDetalles
        {
            get
            {
                return detalles;
            }
        }
        /// <summary>
        /// Lee el atributo especificado por AttributeName.
        /// </summary>
        /// <param name="AttributeName">Nombre del atributo que se va a leer.</param>
        /// <returns>El contenido del atributo especificado.</returns>
        public string XmlReadAttribute(string AttributeName)
        {
            try
            {
                XmlNode root = xdoc.DocumentElement;

                return root.Attributes.GetNamedItem(AttributeName, xdoc.NamespaceURI).InnerText;
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// Lee el atributo especificado por AttributeName, dentro del elemento ElementName.
        /// </summary>
        /// <param name="AttributeName">Nombre del atributo que se va a leer.</param>
        /// <param name="ElementName">Nombre del elemento que contiene el atributo a leer.</param>
        /// <returns>El contenido del atributo especificado.</returns>
        public string XmlReadAttribute(string AttributeName, string ElementName)
        {
            try
            {
                XmlElement root = xdoc.DocumentElement;

                XmlNode xmlnode = root.SelectSingleNode("//*[name()=\"" + ElementName + "\"]/@" + AttributeName);

                return xmlnode.InnerText;
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// Lee el atributo especificado por AttributeName, dentro del n=index elemento ElementName.
        /// </summary>
        /// <param name="AttributeName">Nombre del atributo que se va a leer.</param>
        /// <param name="ElementName">Nombre del elemento que contiene el atributo a leer.</param>
        /// <param name="index">Numero del elemento que contiene el atributo a leer.</param>
        /// <returns>El contenido del atributo especificado.</returns>
        public string XmlReadAttribute(string AttributeName, string ElementName, int index)
        {
            try
            {
                XmlElement root = xdoc.DocumentElement;

                XmlNode xmlnode = root.SelectSingleNode("//" + ElementName + "[" + index + "]" + "/@" + AttributeName);

                return xmlnode.InnerText;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        /// <summary>
        /// Lee el elemento especificado por ElementName.
        /// </summary>
        /// <param name="ElementName">Nombre del elemento que se va a leer.</param>
        /// <returns>El contenido del elemento especificado.</returns>
        public string XmlReadElement(string ElementName)
        {
            try
            {
                XmlNode root = xdoc.DocumentElement;

                XmlNode xmlnode = root.SelectSingleNode("//*[name()=\"" + ElementName + "\"]");

                if (xmlnode.InnerText == null)
                    return xmlnode.InnerXml;
                else
                    return xmlnode.InnerText;
            }
            catch
            {
                return "";
            }
        }
        public void XmlWriteAttribute(string AttributeName, string Value)
        {
            try
            {
                XmlElement root = xdoc.DocumentElement;

                // Write attribute value.
                root.SetAttribute(AttributeName, Value);
            }
            catch
            {

            }
        }
        public void XmlWriteAttribute(string AttributeName, string Value, string ElementName)
        {
            try
            {
                XmlElement root = xdoc.DocumentElement;

                XmlElement xmlelement = (XmlElement)root.SelectSingleNode("//*[name()=\"" + ElementName + "\"]");

                xmlelement.SetAttribute(AttributeName, Value);
            }
            catch
            {

            }
        }
        public byte[] XmlTransform(string xslFile)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                XPathDocument xml = new XPathDocument(xdoc.BaseURI);

                XslCompiledTransform xsl = new XslCompiledTransform();
                xsl.Load(xslFile);

                xsl.Transform(xml, null, ms);
            }
            catch (Exception e)
            {
                throw new Exception("Hubo un error al transformar el xml.", e.InnerException);
            }
            return ms.ToArray();
        }
        public void XmlWriteElement(string ElementName, string innerxml, bool isXml)
        {
            try
            {
                XmlNode root = xdoc.DocumentElement;

                XmlNode xmlnode = root.SelectSingleNode("//*[name()=\"" + ElementName + "\"]");

                if (isXml)
                    xmlnode.InnerXml = innerxml;
                else
                    xmlnode.InnerText = innerxml;

                xmlnode.Attributes.Remove(xmlnode.Attributes["xmlns"]);
            }
            catch (Exception ex)
            {

            }
        }
        public void XmlCreateElement(string elemname)
        {
            XmlElement elmXML = xdoc.CreateElement(elemname, null);

            xdoc.DocumentElement.AppendChild(elmXML);
        }
        public void Save(string xmlFile)
        {
            xdoc.Save(xmlFile);
        }
    }
}