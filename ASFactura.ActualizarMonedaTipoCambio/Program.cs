﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using ASFactura.ActualizarMonedaTipoCambio.Model;

namespace ASFactura.ActualizarMonedaTipoCambio
{
    class Program
    {
        static void Main(string[] args)
        {
            string asFacturaDb = args[3],
                rfcReceptor = args[4],
                serie = args[5],
                folio = args[6],
                total = args[7];

            var consulta = string.Format("SELECT DocumentoId, Cfdi FROM Documento WHERE rfc_Receptor = '{0}' AND serie = '{1}' AND folio = '{2}' AND total = '{3}'", rfcReceptor, serie, folio, total);
            try
            {
                var documentos = new DataTable();
                using (var cn = new SqlConnection(asFacturaDb))
                {
                    cn.Open();
                    var sqlcom = new SqlCommand(consulta, cn);
                    sqlcom.ExecuteNonQuery();
                    var reader = sqlcom.ExecuteReader();
                    documentos.Load(reader);

                    foreach (DataRow documento in documentos.Rows)
                    {
                        var documentoId = documento["DocumentoId"];
                        var cfdi = documento["Cfdi"].ToString();
                        var serializer = new XmlSerializer(typeof(Comprobante));
                        var str = new MemoryStream(Encoding.UTF8.GetBytes(cfdi));
                        var comprobante = (Comprobante)serializer.Deserialize(str);
                        str.Close();

                        var actualizacion = string.Format("UPDATE Documento SET Moneda = '{0}', TipoCambio = '{1}' WHERE DocumentoId = {2} AND Estatus_Valor = 2", !string.IsNullOrEmpty(comprobante.Moneda) ? comprobante.Moneda : "", !string.IsNullOrEmpty(comprobante.TipoCambio) ? comprobante.TipoCambio : "", documentoId);
                        sqlcom = new SqlCommand(actualizacion, cn);
                        sqlcom.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                var applicationEventLog = new EventLog {Source = "AsFactura"};
                applicationEventLog.WriteEntry(string.Format("Error al actualizar --> Argumentos: {0}, Mensaje: {1}.", String.Join(",",args), ex.Message), EventLogEntryType.Error);
            }
        }
    }
}