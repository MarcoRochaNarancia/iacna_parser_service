using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Text;

namespace Avances.Parsers
{
    public static class CGIParser
    {
        public static Dictionary<string, string>[] Parse(StreamReader tmpBuffer)
        {
            string sTmp;
            string[] sTmpArr;

            int facturas = 0; 

            Dictionary<string, string>[] buffer;
            Queue<string> tempQueue = new Queue<string>();

            do
            {
                sTmp = tmpBuffer.ReadLine();

                if (sTmp != "")
                    if (sTmp.Contains("="))
                    {
                        sTmp = sTmp.Trim();
                        tempQueue.Enqueue(sTmp);
                    }
            } while (!tmpBuffer.EndOfStream);

            tmpBuffer = null;

            foreach (string s in tempQueue)
            {
                if(s.Contains("folio"))
                    facturas++;
            }

            buffer = new Dictionary<string, string>[facturas];

            for (int i = 0; i < facturas; i++)
            {
                if(buffer[i] == null)
                    buffer[i] = new Dictionary<string, string>();
                bool finfact = false;

                string temporal = "";

                do
                {
                    try
                    {
                        sTmp = tempQueue.Dequeue();
                        sTmp = sTmp.Replace("\"", "");
                        temporal = sTmp;        

                        if (sTmp != "")
                            if (sTmp.Contains("="))
                            {
                                sTmpArr = sTmp.Split("=".ToCharArray());

                                if (sTmpArr[1].Contains("\""))
                                {
                                    try
                                    {
                                        sTmpArr[1] = sTmpArr[1].Split("\"".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                                    }
                                    catch
                                    {
                                        sTmpArr[1] = "";
                                    }
                                }
                                sTmpArr[0] = sTmpArr[0].Trim();
                                sTmpArr[1] = sTmpArr[1].Trim();
                                buffer[i].Add(sTmpArr[0], sTmpArr[1]);
                            }
                    }
                    catch (Exception e)
                    {
                        if ((i + 1) != facturas)
                        {
                            buffer[i + 1] = new Dictionary<string, string>();
                            if (temporal != "")
                                if (temporal.Contains("="))
                                {
                                    sTmpArr = temporal.Split("=".ToCharArray());

                                    if (sTmpArr[1].Contains("\""))
                                    {
                                        sTmpArr[1] = sTmpArr[1].Split("\"".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
                                    }
                                    sTmpArr[0] = sTmpArr[0].Trim();
                                    sTmpArr[1] = sTmpArr[1].Trim();
                                    buffer[i+1].Add(sTmpArr[0], sTmpArr[1]);
                                }
                        }
                        finfact = true;
                    }
                } while (!finfact);
            }

            //do
            //{
            //    sTmp = tmpBuffer.ReadLine();

            //    if (sTmp != "")
            //    if(sTmp.Contains("="))
            //    {
            //        sTmpArr = sTmp.Split("=".ToCharArray());

            //        if (sTmpArr[1].Contains("\""))
            //        {
            //            sTmpArr[1] = sTmpArr[1].Split("\"".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];
            //        }
            //        sTmpArr[0] = sTmpArr[0].Trim();
            //        sTmpArr[1] = sTmpArr[1].Trim();
            //        //if (sTmpArr[1] != "")
            //        //{
            //            buffer.Add(sTmpArr[0], sTmpArr[1]);
            //        //}
            //    }
               
            //} while (!tmpBuffer.EndOfStream);
            return buffer;
        }
    }
}