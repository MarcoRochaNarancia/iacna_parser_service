<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:PMT="http://www.vwnovedades.com/volkswagen/kanseilab/shcp/2009/Addenda/PMT">
  <xsl:template match="/">

    <xsl:element name="requestForPayment">
      <xsl:attribute name="type">
        <xsl:text>SimpleInvoiceType</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="contentVersion">
        <xsl:text>1.3.1</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="documentStructureVersion">
        <xsl:text>AMC7.1</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="documentStatus">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@documentStatus"/>
      </xsl:attribute>
      <xsl:element name="requestForPaymentIdentification">
        <xsl:element name="entityType">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoDeComprobante"/>
        </xsl:element>
        <!--<xsl:choose>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CREDIT_NOTE2'">
            <xsl:text>CREDIT_NOTE</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoDeComprobante"/>
          </xsl:otherwise>
        </xsl:choose>-->
        <xsl:element name="uniqueCreatorIdentification">
          <xsl:value-of select="/Avances/Detalles/Detalle/@billOfLading"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="orderIdentification">
        <xsl:element name="referenceIdentification">
          <xsl:attribute name="type">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@referenceIdentification"/>
          </xsl:attribute>
          <!--<xsl:text>DATO SPOOL</xsl:text>-->
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@referenceIdentificationtype"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="AdditionalInformation">
        <xsl:element name="referenceIdentification">
          <xsl:attribute name="type">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@AdditionalInformation"/>
          </xsl:attribute>
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@AdditionalInformationType"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="buyer">
        <xsl:element name="gln">
          <xsl:value-of select="/Avances/Encabezado/Receptor/@rfc"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="seller">
        <xsl:element name="gln">
          <xsl:value-of select="/Avances/Encabezado/Emisor/@rfc"/>
        </xsl:element>
        <xsl:element name="alternatePartyIdentification">
          <xsl:attribute name="type">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@alternatePartyIdentification"/>
          </xsl:attribute>
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@numProveedor"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="shipTo">
        <xsl:element name="gln">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoCodigo"/>
        </xsl:element>
        <xsl:element name="nameAndAddress">
          <xsl:element name="name">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoNombre"/>
          </xsl:element>
          <xsl:element name="streetAddressOne">
            <xsl:value-of select="/Avances/Encabezado/EnviarA/@calle"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="/Avances/Encabezado/EnviarA/@noExterior"/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="/Avances/Encabezado/EnviarA/@colonia"/>
          </xsl:element>
          <xsl:element name="city">
            <xsl:value-of select="/Avances/Encabezado/EnviarA/@municipio"/>
          </xsl:element>
          <xsl:element name="PostalCode">
            <xsl:value-of select="/Avances/Encabezado/EnviarA/@codigoPostal"/>
          </xsl:element>
        </xsl:element>
      </xsl:element>
      <xsl:element name="currency">
        <xsl:attribute name="currencyISOCode">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>
        </xsl:attribute>
        <xsl:element name="currencyFunction">
          <xsl:text>BILLING_CURRENCY</xsl:text>
        </xsl:element>
      </xsl:element>

      <xsl:for-each select="/Avances/Detalles/Detalle">

        <xsl:element name="lineItem">
          <xsl:attribute name="type">
            <xsl:text>SimpleInvoiceLineItemType</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="number">
            <xsl:value-of select="position()"/>
          </xsl:attribute>
          <xsl:element name="tradeItemIdentification">
            <xsl:element name="gtin">
              <xsl:value-of select="@noIdentificacion"/>
            </xsl:element>
          </xsl:element>

          <xsl:element name="invoicedQuantity">
            <xsl:attribute name="unitOfMeasure">
              <xsl:value-of select="@unidad"/>
            </xsl:attribute>
            <xsl:value-of select="@cantidad"/>
          </xsl:element>
          <xsl:element name="AdditionalInformation">
            <xsl:element name="referenceIdentification">
              <xsl:attribute name="type">
                <xsl:value-of select="/Avances/Encabezado/Comprobante/@referenceIdentification"/>
              </xsl:attribute>
              <xsl:value-of select="@ordenCompra"/>
            </xsl:element>
          </xsl:element>

          <xsl:element name="totalLineAmount">
            <xsl:element name="grossAmount">
              <xsl:element name="Amount">
                <xsl:value-of select="@importeBruto"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="netAmount">
              <xsl:element name="Amount">
                <xsl:value-of select="@importeNeto"/>
              </xsl:element>
            </xsl:element>
          </xsl:element>
        </xsl:element>

      </xsl:for-each>
      <xsl:element name="totalAmount">
        <xsl:element name="Amount">
          <!--<xsl:text>DATO SPOOL</xsl:text>-->
          <xsl:value-of select="/Avances/Totales/@total"/>
        </xsl:element>
      </xsl:element>

      <xsl:element name="tax">
        <xsl:attribute name="type">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@codigoCargos"/>
        </xsl:attribute>
        <xsl:element name="taxPercentage">
          <xsl:value-of select="/Avances/Totales/@porcentajeImpuesto"/>
        </xsl:element>
        <xsl:element name="taxAmount">
          <xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
        </xsl:element>
      </xsl:element>

    </xsl:element>
  </xsl:template>
</xsl:stylesheet>