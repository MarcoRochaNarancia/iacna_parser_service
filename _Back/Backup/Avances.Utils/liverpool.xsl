<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <xsl:element name="requestForPayment">
      <xsl:attribute name="type">
        <xsl:text>SimpleInvoiceType</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="contentVersion">
        <xsl:text>1.3.1</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="documentStructureVersion">
        <xsl:text>AMC006</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="documentStatus">
        <xsl:text>ORIGINAL</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="DeliveryDate">
        <xsl:value-of select="substring-before(/Avances/Encabezado/Comprobante/@fecha,'T')"/>
      </xsl:attribute>
      <xsl:element name="requestForPaymentIdentification">
        <xsl:element name="entityType">
          <xsl:choose>
            <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CREDIT_NOTE2'">
              <xsl:text>CREDIT_NOTE</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoDeComprobante"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:element>
        <xsl:element name="uniqueCreatorIdentification">
          <xsl:value-of
                        select="concat(/Avances/Encabezado/Comprobante/@serie,/Avances/Encabezado/Comprobante/@folio)"
                    />
        </xsl:element>
      </xsl:element>
      <xsl:if test="/Avances/Encabezado/Comprobante/@condicionesDePago">
        <xsl:element name="specialInstruction">
          <xsl:attribute name="code">AAB</xsl:attribute>
          <xsl:element name="text">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@condicionesDePago"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>
      <xsl:if test="/Avances/Encabezado/Comprobante/@informacionDeImpuestos">
        <xsl:element name="specialInstruction">
          <xsl:attribute name="code">DUT</xsl:attribute>
          <xsl:element name="text">
            <xsl:value-of
                            select="/Avances/Encabezado/Comprobante/@informacionDeImpuestos"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>
      <xsl:if test="/Avances/Encabezado/Comprobante/@informacionDeCompras">
        <xsl:element name="specialInstruction">
          <xsl:attribute name="code">PUR</xsl:attribute>
          <xsl:element name="text">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@informacionDeCompras"
                        />
          </xsl:element>
        </xsl:element>
      </xsl:if>
      <xsl:if test="/Avances/Encabezado/Comprobante/@importeConLetra">
        <xsl:element name="specialInstruction">
          <xsl:attribute name="code">ZZZ</xsl:attribute>
          <xsl:element name="text">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@importeConLetra"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>

      <xsl:if test="string-length(/Avances/Encabezado/Comprobante/@pedidoLiverpool) &gt; 1">
        <xsl:element name="orderIdentification">
          <xsl:element name="referenceIdentification">
            <xsl:attribute name="type">ON</xsl:attribute>
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@pedidoLiverpool"/>
          </xsl:element>
          <xsl:element name="ReferenceDate">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@fechaPedidoLiverpool"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>

      <xsl:element name="AdditionalInformation">
        <xsl:element name="referenceIdentification">
          <xsl:attribute name="type">ATZ</xsl:attribute>
          <xsl:text>IDKFAIDDQD2</xsl:text>
        </xsl:element>
        <xsl:element name="referenceIdentification">
          <xsl:attribute name="type">IV</xsl:attribute>
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>
        </xsl:element>
      </xsl:element>

      <xsl:if test="string-length(/Avances/Encabezado/Comprobante/@pedidoLiverpool) &gt; 1">
        <xsl:element name="DeliveryNote">
          <xsl:element name="referenceIdentification">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@reciboLiverpool"/>
          </xsl:element>
          <xsl:element name="ReferenceDate">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@fechaReciboLiverpool"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>

      <xsl:element name="buyer">
        <xsl:element name="gln">
          <xsl:value-of select="/Avances/Encabezado/Receptor/@gln"/>
        </xsl:element>
        <xsl:element name="contactInformation">
          <xsl:element name="personOrDepartmentName">
            <xsl:element name="text">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@numero"/>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
      <xsl:element name="seller">
        <xsl:element name="gln">
          <xsl:value-of select="/Avances/Encabezado/Emisor/@gln"/>
        </xsl:element>
        <xsl:element name="alternatePartyIdentification">
          <xsl:attribute name="type">
            <xsl:text>SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY</xsl:text>
          </xsl:attribute>
          <xsl:value-of select="/Avances/Encabezado/Emisor/@numeroProveedor"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="currency">
        <xsl:attribute name="currencyISOCode">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>
        </xsl:attribute>
        <xsl:element name="currencyFunction">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@funcion"/>
        </xsl:element>
        <xsl:element name="rateOfChange">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@tipoDeCambio"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="allowanceCharge">
        <xsl:attribute name="allowanceChargeType">
          <xsl:text>ALLOWANCE_GLOBAL</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="settlementType">
          <xsl:text>OFF_INVOICE</xsl:text>
        </xsl:attribute>
        <xsl:element name="specialServicesType">
          <xsl:text>AJ</xsl:text>
        </xsl:element>
        <xsl:element name="monetaryAmountOrPercentage">
          <xsl:element name="rate">
            <xsl:attribute name="base">
              <xsl:text>INVOICE_VALUE</xsl:text>
            </xsl:attribute>
            <xsl:element name="percentage">
              <xsl:text>2.5</xsl:text>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
      <xsl:for-each select="/Avances/Detalles/Detalle">
        <xsl:element name="lineItem">
          <xsl:attribute name="type">
            <xsl:text>SimpleInvoiceLineItemType</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="number">
            <xsl:value-of select="position()"/>
          </xsl:attribute>
          <xsl:element name="tradeItemIdentification">
            <xsl:element name="gtin">
              <xsl:value-of select="@amece"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="alternateTradeItemIdentification">
            <xsl:attribute name="type">
              <xsl:value-of select="@tipoSKU"/>
            </xsl:attribute>
            <xsl:value-of select="@SKU"/>
          </xsl:element>
          <xsl:element name="tradeItemDescriptionInformation">
            <xsl:element name="longText">
              <xsl:value-of select="@descripcion"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="invoicedQuantity">
            <xsl:attribute name="unitOfMeasure">
              <xsl:value-of select="@unidad"/>
            </xsl:attribute>
            <xsl:value-of select="@cantidad"/>
          </xsl:element>
          <xsl:element name="grossPrice">
            <xsl:element name="Amount">
              <xsl:value-of select="@precioBruto"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="netPrice">
            <xsl:element name="Amount">
              <xsl:value-of select="@precioNeto"/>
            </xsl:element>
          </xsl:element>
          <xsl:element name="totalLineAmount">
            <xsl:element name="grossAmount">
              <xsl:element name="Amount">
                <xsl:value-of select="@importeBruto"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="netAmount">
              <xsl:element name="Amount">
                <xsl:value-of select="@importeNeto"/>
              </xsl:element>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:for-each>
      <xsl:element name="totalAmount">
        <xsl:element name="Amount">
          <xsl:value-of select="/Avances/Totales/@subTotalantesDeDescuentos"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="TotalAllowanceCharge">
        <xsl:attribute name="allowanceOrChargeType">
          <xsl:text>ALLOWANCE</xsl:text>
        </xsl:attribute>
        <xsl:element name="Amount">
          <xsl:value-of select="/Avances/Totales/@importeDescuento"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="baseAmount">
        <xsl:element name="Amount">
          <xsl:value-of select="/Avances/Totales/@subTotalantesDeDescuentos"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="tax">
        <xsl:attribute name="type">
          <xsl:value-of select="/Avances/Totales/@tipoImpuesto"/>
        </xsl:attribute>
        <xsl:element name="taxPercentage">
          <xsl:value-of select="/Avances/Totales/@porcentajeImpuesto"/>
        </xsl:element>
        <xsl:element name="taxAmount">
          <xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
        </xsl:element>
        <xsl:element name="taxCategory">
          <xsl:value-of select="/Avances/Totales/@categoriaImpuesto"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="payableAmount">
        <xsl:element name="Amount">
          <xsl:value-of select="/Avances/Totales/@total"/>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>