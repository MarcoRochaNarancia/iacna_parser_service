<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<xsl:element name="avances">
		<xsl:attribute name="importe1">
			<xsl:value-of select="/Avances/Totales/@totalPesos"/>
		</xsl:attribute>
		<xsl:attribute name="iva1">
      <xsl:value-of select="/Avances/Totales/@importeImpuestoPesos"/>
		</xsl:attribute>
		<xsl:attribute name="importe2">
			<xsl:value-of select="/Avances/Totales/@totalDolares"/>
		</xsl:attribute>
		<xsl:attribute name="iva2">
      <xsl:value-of select="/Avances/Totales/@importeImpuestoDolares"/>
		</xsl:attribute>
	</xsl:element>
	</xsl:template>
</xsl:stylesheet>