using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Avances;
using Avances.Xml;
using System.Diagnostics;

namespace Parser
{
    class Program
    {
        private FileInfo fiStd;
        private FileInfo fiCfd;
        private FileInfo fiAddenda;
        private FileInfo fiXsl;
        private string sTmp;
        static void Main(string[] args)
        {
            string spoolType = args[0];
            string pdfPath = args[1];
            string spoolPath = args[2];
            string asfactpath = args[3];


            //Console.WriteLine(spoolType);
            //Console.WriteLine(pdfPath);
            //Console.WriteLine(spoolPath);
            //Console.WriteLine(asfactpath);
            //Console.ReadLine();

            Program p = new Program();
            switch (spoolType)
            {
                case "CGI":


                    p.CGIParse(spoolPath, asfactpath, pdfPath);
                    break;
            }
            borrarTemps();
        }

        protected void CGIParse(string spoolPath, string asfactpath, string pdfPath)
        {
            Dictionary<string, string>[] buffer = Avances.Parsers.CGIParser.Parse(new System.IO.StreamReader(spoolPath, Encoding.Default));

            string fiResult = "";

            for (int i = 0; i < buffer.Length; i++)
            {
                fiStd = new FileInfo(Path.GetTempFileName());
                fiCfd = new FileInfo(Path.GetTempFileName());
                fiAddenda = new FileInfo(Path.GetTempFileName());
                fiXsl = new FileInfo(Path.GetTempFileName());

                //Console.WriteLine(fiStd.FullName);
                //Console.WriteLine(fiCfd.FullName);
                //Console.WriteLine(fiAddenda.FullName);
                //Console.WriteLine(fiXsl.FullName);
                //Console.ReadLine();

                try
                {
                    KPToXML xml = new KPToXML(buffer[i]);
                    xml.XmlStd.Save(fiStd.FullName);

                    XmlDoc std = new XmlDoc(fiStd.FullName);

                    //File.Delete(fiXsl.FullName);
                    //File.Delete(fiCfd.FullName);
                    //File.Delete(fiAddenda.FullName);

                    fiXsl = Utils.getTempResourceFile("Avances.Utils.cfd.xsl");
                    byte[] bTmp = std.XmlTransform(fiXsl.FullName);
                    sTmp = Encoding.UTF8.GetString(bTmp, 1, bTmp.Length - 1);
                    sTmp = sTmp.Replace("<Comprobante", "<Comprobante xmlns=\"http://www.sat.gob.mx/cfd/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv2.xsd\"");
                    if (sTmp[0] != '<')
                    {
                        sTmp = sTmp.Remove(0, sTmp.IndexOf("<"));
                    }
                    XmlDoc cfd = new XmlDoc(sTmp);
                    string param = "0";
                    switch (std.XmlReadAttribute("addenda", "Comprobante"))
                    {
                        case "4":

                            param = "8";
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.HomeDepot.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            fiXsl = Utils.getTempResourceFile("Avances.Utils.avances.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp += Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            cfd.xdoc.InnerXml = cfd.xdoc.InnerXml.Replace("</Comprobante>", "<Addenda>" + sTmp + "</Addenda></Comprobante>");
                            break;
                        case "10":
                            param = "8";
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.Chrysler.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            fiXsl = Utils.getTempResourceFile("Avances.Utils.avances.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp += Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            cfd.xdoc.InnerXml = cfd.xdoc.InnerXml.Replace("</Comprobante>", "<Addenda>" + sTmp + "</Addenda></Comprobante>");
                            break;
                        case "13":
                            param = "8";
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.Volkswagen.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            fiXsl = Utils.getTempResourceFile("Avances.Utils.avances.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp += Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            cfd.xdoc.InnerXml = cfd.xdoc.InnerXml.Replace("</Comprobante>", "<Addenda>" + sTmp + "</Addenda></Comprobante>");
                            break;
                        case "15":
                            param = "8";
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.Nissan.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            fiXsl = Utils.getTempResourceFile("Avances.Utils.avances.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp += Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            cfd.xdoc.InnerXml = cfd.xdoc.InnerXml.Replace("</Comprobante>", "<Addenda>" + sTmp + "</Addenda></Comprobante>");
                            break;


                        default:

                            fiXsl = Utils.getTempResourceFile("Avances.Utils.avances.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            cfd.xdoc.InnerXml = cfd.xdoc.InnerXml.Replace("</Comprobante>", "<Addenda>" + sTmp + "</Addenda></Comprobante>");
                            break;
                    }
                    string serie = cfd.XmlReadAttribute("serie");
                    string folio = cfd.XmlReadAttribute("folio");
                    string rfcemisor = cfd.XmlReadAttribute("rfc", "Emisor");
                    string rfcreceptor = cfd.XmlReadAttribute("rfc", "Receptor");
                    string PrinterName = std.XmlReadAttribute("PrinterName", "Comprobante");
                    string llave = rfcemisor + serie;
                    string file = rfcemisor + "_" + serie + "_" + folio + ".xml";
                    string filePDF = rfcemisor + serie + folio + ".pdf";
                    string filePDF2 = rfcemisor + "_" + serie + "_" + folio + ".pdf";
                    fiResult = file + ".xml";
                    cfd.Save(file);

                    Process asfactura = new Process();
                    asfactura.StartInfo.UseShellExecute = false;
                    asfactura.StartInfo.Arguments = "SignEx " + llave + " " + file + " " + fiResult + " " + param;
                    asfactura.StartInfo.FileName = asfactpath + "xmldse.exe";

                    asfactura.Start();
                    asfactura.WaitForExit();
                    try
                    {
                        if (!Directory.Exists(asfactpath + @"\PorEnviar\"))
                            Directory.CreateDirectory(asfactpath + @"\PorEnviar\");

                        string planta = std.XmlReadAttribute("plantaEmisor", "Emisor");
                        string numProveedor = std.XmlReadAttribute("numProveedor", "Comprobante");
                        File.Copy(asfactpath + @"\CFD\Comprobantes\XMLFirmados\" + file, asfactpath + @"\PorEnviar\" + planta + "_" + numProveedor + "_" + rfcreceptor + "_" + file);
                    }
                    catch (Exception ex)
                    {

                        ////Console.WriteLine("Error: " + ex.Message);
                        EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                        ApplicationEventLog.Source = "ASFactura";
                        ApplicationEventLog.WriteEntry(ex.ToString(), EventLogEntryType.Error);

                    }

                    XmlDoc rsult = new XmlDoc(fiResult);

                    if (rsult.XmlReadElement("Sellado") == "OK")
                    {
                        std.XmlWriteAttribute("cadenaOriginal", rsult.XmlReadElement("CadenaOriginal"), "Comprobante");
                        std.XmlWriteAttribute("sello", rsult.XmlReadElement("Sello"), "Comprobante");
                        std.XmlWriteAttribute("noAprobacion", rsult.XmlReadElement("noAprobacion"), "Comprobante");
                        std.XmlWriteAttribute("anoAprobacion", rsult.XmlReadElement("anoAprobacion"), "Comprobante");
                        std.XmlWriteAttribute("noCertificado", rsult.XmlReadElement("noCertificado"), "Comprobante");
                        std.Save(fiStd.FullName);
                        rsult = null;

                        xmlPdf.xmlpdf a = new xmlPdf.xmlpdf();
                        a.XmlStdPdf(std, pdfPath + filePDF);


                        //Process print = new Process();
                        //print.StartInfo.FileName = "AcroRd32.exe";
                        //print.StartInfo.Arguments = @"/t /p /h " + pdfPath + filePDF2 + " \"" + PrinterName+"\"";
                        //print.StartInfo.CreateNoWindow = true;
                        //print.StartInfo.UseShellExecute = true;
                        //print.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;
                        //print.Start();

                    }
                    else
                    {
                        string exc = rsult.XmlReadElement("Mensaje").ToString();
                        EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                        ApplicationEventLog.Source = "ASFactura";
                        ApplicationEventLog.WriteEntry(exc.ToString(), EventLogEntryType.Error);

                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.Message);
                    //Console.WriteLine(sTmp);
                    EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                    ApplicationEventLog.Source = "ASFactura";
                    ApplicationEventLog.WriteEntry(e.ToString(), EventLogEntryType.Error);

                }
                finally
                {
                    try
                    {
                        File.Delete(fiResult);
                        File.Delete(fiStd.FullName);
                        File.Delete(fiCfd.FullName);
                        File.Delete(fiAddenda.FullName);
                        File.Delete(fiXsl.FullName);
                    }
                    catch { }

                }
            }
        }
        public static void borrarTemps()
        {

            string[] archivos = Directory.GetFiles(System.Configuration.ConfigurationManager.AppSettings["TempPath"].ToString(), "tmp*.tmp");
            //string[] archivos = Directory.GetFiles(System.IO.Path.GetTempPath()+"tmp*.tmp");
            DateTime ultimoAcceso;
            foreach (string s in archivos)
            {

                ultimoAcceso = File.GetLastAccessTime(s);
                if (ultimoAcceso.Day < DateTime.Now.Day || ultimoAcceso.Hour < DateTime.Now.Hour - 1)
                    File.Delete(s);
            }
        }
    }
}