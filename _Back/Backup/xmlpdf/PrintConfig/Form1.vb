Imports System.Drawing.Printing
Imports XMLIO

Public Class Form1

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim pkInstalledPrinters As String
        Dim i As Integer

        For i = 0 To PrinterSettings.InstalledPrinters.Count - 1
            pkInstalledPrinters = PrinterSettings.InstalledPrinters(i)
            cbImpre.Items.Add(pkInstalledPrinters)
        Next

        Dim xml As New XMLIO.XMLIO()
        Dim _xmlContent As String = "PrintConfig1.xml"
        cbImpre.Text = xml.XmlReadElement("impresora", _xmlContent)
        If xml.XmlReadElement("orientacion", _xmlContent) = "1" Then

        End If
    End Sub


    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        paper.Height = 50
        paper.Width = 42

    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        paper.Height = 40
        paper.Width = 60
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            txtCopias.Enabled = True
        Else
            txtCopias.Enabled = False
            txtCopias.Text = ""
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If CheckBox2.Checked Then
            lbEscala.Enabled = True
            lbEscala.SelectedIndex = 0

        Else

            lbEscala.Enabled = False

        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If CheckBox3.Checked Then
            txtSize.Enabled = True
        Else
            txtSize.Enabled = False
            txtSize.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim xml As New XMLIO.XMLIO()
        Dim _xmlContent As String = "PrintConfig1.xml"
        xml.XmlWriteElement("impresora", cbImpre.Text, False, _xmlContent)
        If RadioButton1.Checked Then
            xml.XmlWriteElement("orientacion", "1", False, _xmlContent)
        ElseIf RadioButton2.Checked Then
            xml.XmlWriteElement("orientacion", "2", False, _xmlContent)
        End If
        If chkduplex.Checked Then
            xml.XmlWriteElement("duplex", "1", False, _xmlContent)
        Else
            xml.XmlWriteElement("duplex", "0", False, _xmlContent)
        End If
        If CheckBox1.Checked Then
            xml.XmlWriteElement("copias", txtCopias.Text, False, _xmlContent)
        End If
        If CheckBox2.Checked Then
            xml.XmlWriteElement("escala", lbEscala.SelectedItem.ToString, False, _xmlContent)
        End If
        If CheckBox3.Checked Then
            xml.XmlWriteElement("papel", txtSize.Text, False, _xmlContent)
        End If

        MessageBox.Show("Se han guardado los cambios satisfactoriamente", "PrintConfig", MessageBoxButtons.OK)




    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        End
    End Sub
End Class
