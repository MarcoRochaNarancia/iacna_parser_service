using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Xml;
using Avances.Xml;

namespace FileWatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSystemWatcher fsw = null;
          
            switch(args.Length)
            {
                default:
                    Console.WriteLine("[folderpath] [filter]");// [parserPath] [xmldsePath] [pdfsPath]");
                    break;
                case 1:
                    fsw = new FileSystemWatcher(args[0]);
                    break;
                case 2:
                    fsw = new FileSystemWatcher(args[0],args[1]);
                    break;
            }

            if (fsw != null)
            {
                fsw.Created += new FileSystemEventHandler(fsw_Created);
                fsw.EnableRaisingEvents = true;

                Console.WriteLine("Presione \'q\' para salir.");
                while (Console.Read() != 'q') ;
            }
        }

        static void fsw_Created(object sender, FileSystemEventArgs e)
        {
            System.Threading.Thread.Sleep(15000);

            try
            {

                Configuracion c = new Configuracion("c:\\xmldse\\config.xml");

                if (c.SpoolType == "")
                    throw new Exception("No se definio el tipo de spool.");
                if (c.ASFactPath == "")
                    throw new Exception("No se definio la ruta de ASFactura.");
                if (c.PdfPath == "")
                    throw new Exception("No se definio la ruta de destino de los PDF.");

                Process parser = new Process();
                parser.StartInfo.UseShellExecute = false;
                parser.StartInfo.Arguments = c.SpoolType + " " + c.PdfPath + " " + e.FullPath + " " + c.ASFactPath;
                parser.StartInfo.FileName = c.ASFactPath + "parser.exe";

                parser.Start();
                parser.WaitForExit();
            }
            catch (InvalidOperationException ioex)
            {
                Console.WriteLine(ioex.Message + ". El proceso no se pudo ejecutar.");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ". El proceso no se pudo ejecutar.");

            }
            finally
            {
                System.Threading.Thread.Sleep(2000);
                File.Delete(e.FullPath);
            }
        }
    }
    class Configuracion
    {
        private string pdfPath;
        private string asFactPath;
        private string spoolType;
        private FileInfo config;

        public string PdfPath
        {
            get
            {
                return pdfPath;
            }
        }
        public string ASFactPath
        {
            get
            {
                return asFactPath;
            }
        }
        public string SpoolType
        {
            get
            {
                return spoolType;
            }
        }

        public Configuracion(string path)
        {
            config = new FileInfo(path);

            if (!config.Exists)
            {
                crearConfig(config.FullName);
            }

            leerConfig(config.FullName);
        }

        private void crearConfig(string path)
        {
            XmlDocument cfg = new XmlDocument();
            cfg.AppendChild(cfg.CreateXmlDeclaration("1.0", "UTF-8", null));

            XmlElement root = cfg.CreateElement("Avances");

            XmlElement pdf = cfg.CreateElement("PdfPath");
            pdf.InnerText = "Ruta de la carpeta donde de guardaran los PDF.";

            XmlElement asfact = cfg.CreateElement("ASFactPath");
            asfact.InnerText = "Ruta de la ubicacion de ASFactura.";

            XmlElement spool = cfg.CreateElement("SpoolType");
            spool.InnerText = "Tipo de spool que se genera(CGI,RDI,Texto,etc.)";

            root.AppendChild(pdf);
            root.AppendChild(asfact);
            root.AppendChild(spool);
            cfg.AppendChild(root);

            cfg.Save(path);

            cfg = null;
        }
        private void leerConfig(string path)
        {
            XmlDoc cfg = new XmlDoc(path);

            pdfPath = cfg.XmlReadElement("PdfPath");
            asFactPath = cfg.XmlReadElement("ASFactPath");
            spoolType = cfg.XmlReadElement("SpoolType");

            cfg = null;
        }
    }
}
