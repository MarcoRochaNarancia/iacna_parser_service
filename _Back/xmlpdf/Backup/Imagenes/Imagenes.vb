Imports System.Reflection
Imports System.io
Public Class imagenes
    Public Shared Function getTempResourceFile(ByVal resource As String) As FileInfo
        Dim _assembly As Assembly = Assembly.GetExecutingAssembly
        Dim tmpfile As FileInfo = New FileInfo(Path.GetTempFileName)
        ' Using 
        Dim ofs As FileStream = New FileStream(tmpfile.FullName, FileMode.Create, FileAccess.Write, FileShare.Write)
        Try
            ' Using 
            Dim xml_wtr As StreamWriter = New StreamWriter(ofs, System.Text.Encoding.Default)
            Try
                Dim data As String
                Dim aby_rdr As StreamReader = New StreamReader(_assembly.GetManifestResourceStream(resource), System.Text.Encoding.Default)
                data = aby_rdr.ReadToEnd
                aby_rdr.Close()
                xml_wtr.Write(data)
                xml_wtr.Flush()
                xml_wtr.Close()
            Finally
                CType(xml_wtr, IDisposable).Dispose()
            End Try
        Finally
            CType(ofs, IDisposable).Dispose()
        End Try
        Return tmpfile
    End Function

End Class
