Imports System
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports Avances.Xml.XmlDoc
Imports Utiles
Imports System.Drawing
Imports System.Text.RegularExpressions
Imports System.Collections.Generic


Public Class xmlpdf
    Dim doc As Document = New Document(PageSize.LETTER)
    Dim pasadoNumeroDePartidas As Integer = 0
    Dim paginaNo As Integer = 1
    Dim numCaracteres As Integer
    Dim numCaracteresCadOriginal As Integer = 125
    Dim a As Integer
    Dim filaDetalle As Integer
    Dim filaDetalleInicial As Integer
    Dim banderaCadOrinal As Boolean = False
    Dim numPartidasXPagina As New ArrayList
    Dim count As Integer = 0
    Dim numPaginas As Integer = 1
    Dim textoDescripcion As String = ""
    Dim numLineasXDesc As Decimal = 0
    Dim numLineasActuales As Integer = 0
    Dim totalLineasPagina As Integer
    Dim totalNumPartidas As Integer = 0
    Dim totalNumPartidasN1 As Integer = 0
    Dim totalNumPartidasN2 As Integer = 0
    Dim tipoDoc As String
    Dim qty As String
    Dim comentarios As Boolean = False

    Dim yTituloDetalles, xHeaderDetalles, yHeaderDetalles, xMarcoDetalles, yMarcoDetalles, xImagen, yImagen As Integer

    Private Sub contarpaginas(ByVal xmldoc As Avances.Xml.XmlDoc)



        For count = 1 To xmldoc.NumDetalles
            textoDescripcion = xmldoc.XmlReadAttribute("descripcion", "Detalle", count)
            If textoDescripcion.Length > numCaracteres Then
                numLineasXDesc = textoDescripcion.Length / numCaracteres
                If numLineasXDesc Mod numCaracteres <> 0 Then
                    numLineasXDesc = System.Math.Truncate(numLineasXDesc) + 1
                End If
                If xmldoc.XmlReadAttribute("nombreAduana", "Detalle", count) <> "" Then
                    numLineasXDesc += 2
                End If
                If xmldoc.XmlReadAttribute("noIdMaterial", "Detalle", count) <> "" Then
                    numLineasXDesc += 1
                End If
                If numLineasActuales + numLineasXDesc > totalLineasPagina Then
                    numPaginas += 1
                    For i As Integer = 0 To numPartidasXPagina.Count - 1
                        totalNumPartidasN2 += numPartidasXPagina.Item(i)
                    Next
                    numPartidasXPagina.Add(count - 1 - totalNumPartidasN2)
                    numLineasActuales = numLineasXDesc
                Else

                    numLineasActuales += numLineasXDesc
                End If

                If numLineasActuales = totalLineasPagina Then
                    numPaginas += 1
                    For i As Integer = 0 To numPartidasXPagina.Count - 1
                        totalNumPartidasN2 += numPartidasXPagina.Item(i)
                    Next
                    numPartidasXPagina.Add(count - totalNumPartidasN2)
                    numLineasActuales = 0
                End If
            Else
                If xmldoc.XmlReadAttribute("nombreAduana", "Detalle", count) <> "" Then
                    numLineasActuales += 3
                ElseIf xmldoc.XmlReadAttribute("noIdMaterial", "Detalle", count) <> "" Then
                    numLineasActuales += 2
                Else
                    numLineasActuales += 1
                End If

                If numLineasActuales >= totalLineasPagina Then
                    numPaginas += 1

                    For i As Integer = 0 To numPartidasXPagina.Count - 1
                        totalNumPartidas += numPartidasXPagina.Item(i)
                    Next
                    numPartidasXPagina.Add(count - totalNumPartidas)
                    totalNumPartidas = 0
                    numLineasActuales = 0
                End If
            End If
        Next
        For i As Integer = 0 To numPartidasXPagina.Count - 1
            totalNumPartidasN1 += numPartidasXPagina.Item(i)
        Next

        numPartidasXPagina.Add(xmldoc.NumDetalles - totalNumPartidasN1)
        Dim cadenaOriginal As String = xmldoc.XmlReadAttribute("cadenaOriginal", "Comprobante")
        Dim lineascadenaOriginal As Double = System.Math.Truncate(cadenaOriginal.Length / numCaracteresCadOriginal) + 1

        Dim selloDigital As String = xmldoc.XmlReadAttribute("sello", "Comprobante")
        Dim lineasselloDigital As Double = System.Math.Truncate(selloDigital.Length / numCaracteresCadOriginal) + 1

        Dim ultimaPagina As Integer

        ultimaPagina = numLineasActuales + 7 + lineasselloDigital + lineascadenaOriginal

        If ultimaPagina > totalLineasPagina Then
            numPaginas += System.Math.Truncate(ultimaPagina / totalLineasPagina)
            'numPartidasXPagina.Add(0)

        End If


        'numPartidasXPagina.Add(0)
    End Sub
    Private Sub ponernumerodepagina(ByVal writer)
        Dim a As New iTextSharp.text.pdf.PdfReader("prueba.pdf")


        Dim filaFactura As Integer = 10
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim cb As PdfContentByte = writer.DirectContent
        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.BeginText()
        cb.SetFontAndSize(bf, 9)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, writer.PageNumber & " de " & numPaginas, 10, filaFactura, 0)
        cb.EndText()
    End Sub
    Public Sub XmlStdPdf(ByVal xmldoc As Avances.Xml.XmlDoc, ByVal nombrePdf As String)



        tipoDoc = xmldoc.XmlReadAttribute("tipoDeComprobante", "Comprobante")

        'If tipoDoc = "INVOICE" If tipoDoc = "NOTA DE CREDITO" Then


        numCaracteres = 40
        totalLineasPagina = 23
        contarpaginas(xmldoc)
        filaDetalle = 420

        filaDetalleInicial = 420

        Dim writer As PdfWriter = PdfWriter.GetInstance(doc, New FileStream(nombrePdf, FileMode.Create))
        doc.Open()
        'ponernumerodepagina(writer)

        For COUNT As Integer = 0 To numPartidasXPagina.Count - 1
            crearFormato(doc, xmldoc, writer, nombrePdf)
            llenarHeader(writer, xmldoc)
            'llenarcomentarios(xmldoc, writer)

            If COUNT < numPaginas And numPartidasXPagina.Item(COUNT) <> 0 Then
                filaDetalle = filaDetalleInicial
                Detalles(writer, xmldoc, numPartidasXPagina.Item(COUNT))
                If COUNT < numPartidasXPagina.Count - 1 Then
                    doc.NewPage()
                    'ponernumerodepagina(writer)
                    crearFormato(doc, xmldoc, writer, nombrePdf)
                End If
            End If

        Next

        imprimirLeyenda(writer, xmldoc, nombrePdf)
        imprObservaciones(writer, xmldoc, nombrePdf)
        imprimirCadenaOriginal(writer, xmldoc, nombrePdf)
        imprimirSelloDigital(writer, xmldoc, nombrePdf)
        imprimirImporteConLetra(writer, xmldoc)
        imprimirTotales(writer, xmldoc)

        Dim cb As PdfContentByte = writer.DirectContent
        'Dim codigo1 As New BarcodeEAN
        'codigo1.CodeType = BarcodeEAN.EAN13
        'codigo1.Code = xmldoc.XmlReadAttribute("serie", "Comprobante") & xmldoc.XmlReadAttribute("folio", "Comprobante")
        'Dim imagen13 As Image = codigo1.CreateImageWithBarcode(cb, Nothing, Nothing)




        Dim codigo As New Barcode128()
        codigo.CodeType = Barcode128.CODE128
        codigo.Code = xmldoc.XmlReadAttribute("serie", "Comprobante") & xmldoc.XmlReadAttribute("folio", "Comprobante")
        Dim imagen128 As Image = codigo.CreateImageWithBarcode(cb, Nothing, Nothing)
        doc.Add(New Phrase(New Chunk(imagen128, 440, -130, False)))


        Try
            doc.Close()
        Catch ex As Exception
            Console.Error.WriteLine(ex.Message)
        End Try
        'End If

        insertarNumeroDePagina(xmldoc, nombrePdf)




        'PrintingTest()


    End Sub
    Public Sub llenarcomentarios(ByVal docxml As Avances.Xml.XmlDoc, ByVal writer As PdfWriter)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        If comentarios = False Then
            Dim com1 As String = docxml.XmlReadAttribute("comentarios1", "Comprobante")
            Dim com2 As String = docxml.XmlReadAttribute("comentarios2", "Comprobante")
            Dim com3 As String = docxml.XmlReadAttribute("comentarios3", "Comprobante")
            Dim cb As PdfContentByte = writer.DirectContent
            If com1 <> "" Then
                escribeLinea(writer, 9, bf, com1, 40, filaDetalle)
                filaDetalle = filaDetalle - 7

            End If
            If com2 <> "" Then
                escribeLinea(writer, 9, bf, com1, 40, filaDetalle)
                filaDetalle = filaDetalle - 7

            End If
            If com3 <> "" Then
                escribeLinea(writer, 9, bf, com3, 40, filaDetalle)
                filaDetalle = filaDetalle - 15

            End If

            comentarios = True
        End If

    End Sub
    Public Sub insertarNumeroDePagina(ByVal XMLDOC As Avances.Xml.XmlDoc, ByVal nombrePDF As String)
        Try
            Dim reader As PdfReader = New PdfReader(nombrePDF)
            Dim n As Integer = reader.NumberOfPages
            Dim psize As Rectangle = reader.GetPageSize(1)
            Dim width As Single = psize.Width
            Dim height As Single = psize.Height
            Dim document As Document = New Document(psize, 50, 50, 50, 50)
            Dim nombrePDFResult As String = ""
            nombrePDFResult &= XMLDOC.XmlReadAttribute("rfc", "Emisor")
            nombrePDFResult &= "_"
            nombrePDFResult &= XMLDOC.XmlReadAttribute("serie", "Comprobante")
            nombrePDFResult &= "_"
            nombrePDFResult &= XMLDOC.XmlReadAttribute("folio", "Comprobante")
            nombrePDFResult &= ".pdf"


            Dim pdfPath As New System.IO.FileInfo(nombrePDF)


            Dim writer As PdfWriter = PdfWriter.GetInstance(document, New FileStream(pdfPath.DirectoryName & "\" & nombrePDFResult, FileMode.Create))

            document.Open()
            Dim cb As PdfContentByte = writer.DirectContent
            Dim i As Integer = 1

            While i <= n
                document.NewPage()
                Dim page1 As PdfImportedPage = writer.GetImportedPage(reader, i)
                cb.AddTemplate(page1, 0, 0)
                Dim filaFactura As Integer = 800
                escribeLinea(writer, 9, BaseFont.HELVETICA, i.ToString + " de " + n.ToString, 510, 5)

                System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
            End While

            document.Close()

            System.IO.File.Delete(nombrePDF)
        Catch de As Exception
            Console.Error.WriteLine(de.Message)
        End Try









    End Sub
    Public Sub tipodocumento(ByVal writer, ByVal xmldoc)
        tipoDoc = xmldoc.XmlReadAttribute("tipoDeComprobante", "Comprobante")

        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        If tipoDoc = "INVOICE" Then
            escribeLinea(writer, 15, bf, "F a c t u r a".ToUpper(), 460, 760)
        End If
        If tipoDoc = "CREDIT_NOTE" Then
            escribeLinea(writer, 15, bf, "Nota de credito".ToUpper(), 460, 760)
        End If
        If tipoDoc = "CHARGE_NOTE" Then
            escribeLinea(writer, 15, bf, "Nota de cargo".ToUpper(), 460, 760)
        End If
    End Sub
    Private Sub llenarHeader(ByVal writer, ByVal xmlDoc)

        tipodocumento(writer, xmlDoc)
        vendidoA(writer, xmlDoc)
        Emisor(writer, xmlDoc)
        Emisor2(writer, xmlDoc)
        EnviarA(writer, xmlDoc)
        shipTo(writer, xmlDoc)
        factura(writer, xmlDoc)
        certificado(writer, xmlDoc)
        anoNumAprobacion(writer, xmlDoc)
        fechaDeFacturacion(writer, xmlDoc)
        transportista(writer, xmlDoc)
        shipper(writer, xmlDoc)
        fechaRecibo(writer, xmlDoc)
        condicionesDePago(writer, xmlDoc)
        formaDePago(writer, xmlDoc)
        metodoDePago(writer, xmlDoc)


        'imprimeAtributo(writer, xmlDoc, "ordenCompra ", 535, 80)
        'imprimeAtributo(writer, xmlDoc, "numeroOrden", 535, 80)
        imprimeAtributo(writer, xmlDoc, "numProveedor", 465, 350)
        'imprimeAtributo(writer, xmlDoc, "fechaPedido", 660, 328)
        imprimeAtributo(writer, xmlDoc, "fechaEntrega", 550, 405)

        observaciones(writer, xmlDoc)
        imprimeAtributo(writer, xmlDoc, "numeroCliente", 465, 40)
        'imprimeAtributo(writer, xmlDoc, "numero", "Receptor", 465, 40)
        imprimeAtributo(writer, xmlDoc, "numeroProveedor", "Emisor", 530, 400)
        imprimeAtributo(writer, xmlDoc, "numero", "EnviarA", 660, 328)


    End Sub
    Private Sub vendidoA(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)


        Dim nombre As String = docXml.XmlReadAttribute("nombre", "Receptor")
        Dim numero As String = docXml.XmlReadAttribute("noExterior", "Receptor")
        If numero = Nothing Or numero = "" Then
            numero = docXml.XmlReadAttribute("noInterior", "Receptor")
            If numero = Nothing Then
                numero = ""

            End If
        End If
        Dim direccion1 As String = docXml.XmlReadAttribute("calle", "Receptor") & ", " & numero
        If direccion1 = Nothing Then
            direccion1 = ""
        End If

        Dim direccion2 As String = docXml.XmlReadAttribute("colonia", "Receptor")
        If direccion2 = Nothing Then
            direccion2 = ""
        End If

        Dim direccion3 As String = docXml.XmlReadAttribute("municipio", "Receptor") & ", " & docXml.XmlReadAttribute("estado", "Receptor") & "   " & docXml.XmlReadAttribute("pais", "Receptor") & " " & docXml.XmlReadAttribute("codigoPostal", "Receptor")
        If direccion3 = Nothing Then
            direccion3 = ""
        End If

        Dim rfc As String = docXml.XmlReadAttribute("rfc", "Receptor")
        If (rfc = Nothing) Then
            rfc = ""
        End If

        Dim noCliente As String = docXml.XmlReadAttribute("numero", "Receptor")
        If (noCliente = Nothing) Then
            noCliente = ""
        End If




        Dim filaVendidoA As Integer = 660
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim ct As New ColumnText(cb)
        Dim tipoLetra As Font = New Font(bf, 7)

        ct.SetSimpleColumn(30, filaVendidoA, 450, 580, 11, Element.ALIGN_JUSTIFIED)
        ct.AddText(New Phrase(nombre.ToUpper & vbCrLf & direccion1.ToUpper & vbCrLf & direccion2.ToUpper & vbCrLf & direccion3.ToUpper & vbCrLf & rfc.ToUpper, tipoLetra))
        ct.Go()


        'escribeLinea(writer, 7, BaseFont.HELVETICA, nombre.ToUpper, 188, 617)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, direccion1.ToUpper, 188, 607)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, direccion2.ToUpper, 188, 597)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, rfc.ToUpper, 188, 587)


        cb.BeginText()
        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, noCliente, 105, 540, 0)

        cb.EndText()

    End Sub
    Private Sub Emisor(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)


        Dim nombre As String = docXml.XmlReadAttribute("nombre", "Emisor")
        Dim numero As String = docXml.XmlReadAttribute("noExterior", "Emisor")
        If numero = Nothing Or numero = "" Then
            numero = docXml.XmlReadAttribute("noInterior", "Emisor")
            If numero = Nothing Then
                numero = ""

            End If
        End If
        Dim direccion1 As String = docXml.XmlReadAttribute("calle", "Emisor") & ", " & numero
        If direccion1 = Nothing Then
            direccion1 = ""
        End If

        Dim direccion2 As String = docXml.XmlReadAttribute("colonia", "Emisor")
        If direccion2 = Nothing Then
            direccion2 = ""
        End If

        Dim direccion3 As String = docXml.XmlReadAttribute("municipio", "Emisor") & ", " & docXml.XmlReadAttribute("estado", "Emisor") & "   " & docXml.XmlReadAttribute("pais", "Emisor") & " " & docXml.XmlReadAttribute("codigoPostal", "Emisor")
        If direccion3 = Nothing Then
            direccion3 = ""
        End If

        Dim rfc As String = docXml.XmlReadAttribute("rfc", "Emisor")
        If (rfc = Nothing) Then
            rfc = ""
        End If

        Dim noCliente As String = docXml.XmlReadAttribute("numero", "Emisor")
        If (noCliente = Nothing) Then
            noCliente = ""
        End If




        Dim filaEmisor As Integer = 660
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim ct As New ColumnText(cb)
        Dim tipoLetra As Font = New Font(bf, 7)

        ct.SetSimpleColumn(290, filaEmisor, 550, 580, 11, Element.ALIGN_JUSTIFIED)
        ct.AddText(New Phrase(nombre.ToUpper & vbCrLf & direccion1.ToUpper & vbCrLf & direccion2.ToUpper & vbCrLf & direccion3.ToUpper & vbCrLf & rfc.ToUpper, tipoLetra))
        ct.Go()


        'escribeLinea(writer, 7, BaseFont.HELVETICA, nombre.ToUpper, 188, 617)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, direccion1.ToUpper, 188, 607)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, direccion2.ToUpper, 188, 597)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, rfc.ToUpper, 188, 587)


        cb.BeginText()
        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, noCliente, 105, 540, 0)
        cb.EndText()

    End Sub
    Private Sub Emisor2(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)


        Dim nombre As String = docXml.XmlReadAttribute("nombre", "Emisor")
        Dim numero As String = docXml.XmlReadAttribute("noExterior", "Emisor")
        If numero = Nothing Or numero = "" Then
            numero = docXml.XmlReadAttribute("noInterior", "Emisor")
            If numero = Nothing Then
                numero = ""

            End If
        End If
        Dim direccion1 As String = docXml.XmlReadAttribute("calle", "Emisor") & ", " & numero
        If direccion1 = Nothing Then
            direccion1 = ""
        End If

        Dim direccion2 As String = docXml.XmlReadAttribute("colonia", "Emisor")
        If direccion2 = Nothing Then
            direccion2 = ""
        End If

        Dim direccion3 As String = docXml.XmlReadAttribute("municipio", "Emisor") & ", " & docXml.XmlReadAttribute("estado", "Emisor") & "   " & docXml.XmlReadAttribute("pais", "Emisor") & " " & docXml.XmlReadAttribute("codigoPostal", "Emisor")
        If direccion3 = Nothing Then
            direccion3 = ""
        End If

        Dim rfc As String = docXml.XmlReadAttribute("rfc", "Emisor")
        If (rfc = Nothing) Then
            rfc = ""
        End If

        Dim noCliente As String = docXml.XmlReadAttribute("numero", "Emisor")
        If (noCliente = Nothing) Then
            noCliente = ""
        End If




        Dim filaEmisor2 As Integer = 765
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim ct As New ColumnText(cb)
        Dim tipoLetra As Font = New Font(bf, 7)

        ct.SetSimpleColumn(300, filaEmisor2, 455, 400, 11, Element.ALIGN_RIGHT)
        ct.AddText(New Phrase(nombre.ToUpper & vbCrLf & direccion1.ToUpper & vbCrLf & direccion2.ToUpper & vbCrLf & direccion3.ToUpper & vbCrLf & rfc.ToUpper, tipoLetra))
        ct.Go()


        'escribeLinea(writer, 7, BaseFont.HELVETICA, nombre.ToUpper, 188, 617)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, direccion1.ToUpper, 188, 607)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, direccion2.ToUpper, 188, 597)
        'escribeLinea(writer, 7, BaseFont.HELVETICA, rfc.ToUpper, 188, 587)


        cb.BeginText()
        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, noCliente, 100, 520, 0)
        cb.EndText()

    End Sub
    Private Sub escribeLinea(ByVal writer, ByVal size, ByVal font, ByVal texto, ByVal x, ByVal y)
        Dim cb As PdfContentByte = writer.DirectContent
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.BeginText()

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, size)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, texto, x, y, 0)
        cb.EndText()
    End Sub
    Private Sub escribeLinea2(ByVal writer, ByVal size, ByVal font, ByVal texto, ByVal x, ByVal y)
        Dim cb As PdfContentByte = writer.DirectContent
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.BeginText()

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, size)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, texto, x, y, 0)
        cb.EndText()
    End Sub
    Private Sub escribeLinea(ByVal writer, ByVal size, ByVal font, ByVal texto, ByVal x, ByVal y, ByVal align)
        Dim cb As PdfContentByte = writer.DirectContent
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.BeginText()

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, size)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(align, texto, x, y, 0)

        cb.EndText()
    End Sub
    Private Sub shipper(ByVal writer, ByVal docxml)
        Dim shipper As String = docxml.XmlReadAttribute("shipper", "Comprobante")
        If shipper = Nothing Then
            shipper = ""
        End If
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea(writer, 7, bf, shipper.ToUpper, 535, 465)


    End Sub
    Private Sub shipTo(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent


        Dim nombre As String = docXml.XmlReadAttribute("nombre", "shipTo")

        Dim direccion1 As String = docXml.XmlReadAttribute("calle", "shipTo")
        If direccion1 = Nothing Then
            direccion1 = ""
        Else
            direccion1 = direccion1 & ", " & docXml.XmlReadAttribute("noExterior", "shipTo")
        End If

        Dim direccion2 As String = docXml.XmlReadAttribute("colonia", "shipTo")
        If direccion2 = Nothing Then
            direccion2 = ""
        End If

        Dim direccion3 As String = docXml.XmlReadAttribute("municipio", "shipTo")
        If direccion3 = Nothing Then
            direccion3 = ""
        Else
            direccion3 = direccion3 & ", " & docXml.XmlReadAttribute("estado", "shipTo") & "   " & docXml.XmlReadAttribute("pais", "shipTo") & " " & docXml.XmlReadAttribute("codigoPostal", "shipTo")
        End If

        Dim rfc As String = docXml.XmlReadAttribute("rfc", "shipTo")
        If (rfc = Nothing) Then
            rfc = ""
        End If

        Dim noCliente As String = docXml.XmlReadAttribute("numero", "shipTo")
        If (noCliente = Nothing) Then
            noCliente = ""
        End If

        Dim filashipTo As Integer = 100
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim ct As New ColumnText(cb)
        Dim tipoLetra As Font = New Font(bf, 7)
        ct.SetSimpleColumn(30, filashipTo, 250, 570, 11, Element.ALIGN_JUSTIFIED)
        ct.AddText(New Phrase(nombre.ToUpper & vbCrLf & direccion1.ToUpper & vbCrLf & direccion2.ToUpper & vbCrLf & direccion3.ToUpper & vbCrLf & rfc.ToUpper, tipoLetra))
        ct.Go()


    End Sub
    Private Sub EnviarA(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim nombre As String = docXml.XmlReadAttribute("nombre", "EnviarA")

        Dim direccion1 As String = docXml.XmlReadAttribute("calle", "EnviarA")
        If direccion1 = Nothing Then
            direccion1 = ""
        Else
            direccion1 = direccion1 & ", " & docXml.XmlReadAttribute("noExterior", "EnviarA")
        End If

        Dim direccion2 As String = docXml.XmlReadAttribute("colonia", "EnviarA")
        If direccion2 = Nothing Then
            direccion2 = ""
        End If

        Dim direccion3 As String = docXml.XmlReadAttribute("municipio", "EnviarA")
        If direccion3 = Nothing Then
            direccion3 = ""
        Else
            direccion3 = direccion3 & ", " & docXml.XmlReadAttribute("estado", "EnviarA") & "   " & docXml.XmlReadAttribute("pais", "EnviarA") & " " & docXml.XmlReadAttribute("codigoPostal", "EnviarA")
        End If

        Dim rfc As String = docXml.XmlReadAttribute("rfc", "EnviarA")
        If (rfc = Nothing) Then
            rfc = ""
        End If

        Dim noCliente As String = docXml.XmlReadAttribute("numero", "EnviarA")
        If (noCliente = Nothing) Then
            noCliente = ""
        End If

        Dim filaEnviarA As Integer = 300
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim ct As New ColumnText(cb)
        Dim tipoLetra As Font = New Font(bf, 7)
        ct.SetSimpleColumn(30, filaEnviarA, 550, 570, 11, Element.ALIGN_JUSTIFIED)
        ct.AddText(New Phrase(nombre.ToUpper & vbCrLf & direccion1.ToUpper & vbCrLf & direccion2.ToUpper & vbCrLf & direccion3.ToUpper & vbCrLf & rfc.ToUpper, tipoLetra))
        ct.Go()
    End Sub
    Private Sub factura(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent

        Dim filaFactura As Integer = 730

        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        cb.BeginText()

        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)

        Dim folio As String = docXml.XmlReadAttribute("folio", "Comprobante")
        If folio = Nothing Then
            folio = ""
        End If

        Dim serie As String = docXml.XmlReadAttribute("serie", "Comprobante")
        If serie = Nothing Then
            serie = ""
        End If

        bf = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 13)
        cb.SetColorFill(New iTextSharp.text.Color(255, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, serie & " " & folio, 470, filaFactura, 0)
        cb.EndText()

    End Sub
    Private Sub certificado(ByVal writer, ByVal docXml)
        Dim filaCertificado As Integer = 675
        Dim certificado As String = docXml.XmlReadAttribute("noCertificado", "Comprobante")
        If certificado = Nothing Then
            certificado = ""
        End If

        escribeLinea(writer, 8, BaseFont.HELVETICA, certificado, 395, filaCertificado, PdfContentByte.ALIGN_CENTER)

    End Sub
    Private Sub anoNumAprobacion(ByVal writer, ByVal docXml)
        Dim filaCertificado As Integer = 675
        Dim anoAprobacion As String = docXml.XmlReadAttribute("anoAprobacion", "Comprobante")
        Dim numAprobacion As String = docXml.XmlReadAttribute("noAprobacion", "Comprobante")
        If anoAprobacion = Nothing Then
            anoAprobacion = ""
        End If
        If numAprobacion = Nothing Then
            numAprobacion = ""
        End If
        If anoAprobacion <> "" And numAprobacion <> "" Then
            escribeLinea(writer, 8, BaseFont.HELVETICA, anoAprobacion & " / " & numAprobacion, 470, filaCertificado, PdfContentByte.ALIGN_LEFT)

        End If

    End Sub

    Private Sub fechaDeFacturacion(ByVal writer, ByVal docXml)
        Dim filafechaDeFacturacion As Integer = 700
        Dim fecha As String = docXml.XmlReadAttribute("fecha", "Comprobante")
        If fecha = Nothing Then
            fecha = ""
        End If
        Dim paginaNo As Integer = 1

        fecha = fecha.Replace("-", "/")
        fecha = fecha.Replace("T", " ")
        escribeLinea(writer, 7, BaseFont.HELVETICA, fecha, 470, filafechaDeFacturacion, PdfContentByte.ALIGN_LEFT)

    End Sub
    Private Sub transportista(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim filaTransportista As Integer = 465
        cb.BeginText()

        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim transportista As String = docXml.XmlReadAttribute("transportista", "EnviarA")
        If transportista = Nothing Then
            transportista = ""
        End If
        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, transportista, 150, filaTransportista, 0)

        cb.EndText()

    End Sub
    Private Sub imprimeAtributo(ByVal writer, ByVal docXml, ByVal atributo, ByVal fila, ByVal columna)
        atributo = Convert.ToString(atributo)
        Dim atributo1 As String = docXml.XmlReadAttribute(atributo, "Comprobante")
        If atributo1 = Nothing Then
            atributo1 = ""
        End If
        escribeLinea(writer, 8, BaseFont.HELVETICA, atributo1, columna, fila, PdfContentByte.ALIGN_CENTER)
    End Sub
    Private Sub imprimeAtributo(ByVal writer, ByVal docXml, ByVal atributo, ByVal elemento, ByVal fila, ByVal columna)
        atributo = Convert.ToString(atributo)
        elemento = Convert.ToString(elemento)
        Dim atributo1 As String = docXml.XmlReadAttribute(atributo, elemento)
        If atributo1 = Nothing Then
            atributo1 = ""
        End If
        escribeLinea(writer, 7, BaseFont.HELVETICA, atributo1, columna, fila, PdfContentByte.ALIGN_CENTER)
    End Sub
    Private Sub condicionesDePago(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim filacondicionesDePago As Integer = 550

        cb.BeginText()
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim condicionesDePago As String = docXml.XmlReadAttribute("condicionesDePago", "Comprobante")
        If condicionesDePago = Nothing Then
            condicionesDePago = ""
        End If

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, condicionesDePago, 290, filacondicionesDePago, 0)

        cb.EndText()
    End Sub
    Private Sub metodoDePago(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim filametodoDePago As Integer = 540

        cb.BeginText()
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim metodoDePago As String = docXml.XmlReadAttribute("metodoDePago", "Comprobante")
        If metodoDePago = Nothing Then
            metodoDePago = ""
        End If

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, metodoDePago, 290, filametodoDePago, 0)

        cb.EndText()
    End Sub
    Private Sub formaDePago(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim filaformaDePago As Integer = 560

        cb.BeginText()
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim formaDePago As String = docXml.XmlReadAttribute("formaDePago", "Comprobante")
        If formaDePago = Nothing Then
            formaDePago = ""
        End If

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 7)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, formaDePago, 290, filaformaDePago, 0)

        cb.EndText()
    End Sub
    Private Sub observaciones(ByVal writer, ByVal docXml)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim filaObservaciones As Integer = 540

        cb.BeginText()

        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim observaciones As String = docXml.XmlReadAttribute("agente", "Comprobante")
        If observaciones = Nothing Then
            observaciones = ""
        End If

        bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, 9)
        cb.SetColorFill(New iTextSharp.text.Color(0, 0, 0))
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, observaciones, 350, filaObservaciones, 0)

        cb.EndText()
    End Sub

    Private Sub Detalles(ByVal writer, ByVal docxml, ByVal numPartidasEnPagina)
        descripcion(writer, docxml, numPartidasEnPagina)
    End Sub
    Private Sub pedido(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim numeroPedido As String = docxml.XmlReadAttribute("numeroPedido", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea(writer, 7, bf, numeroPedido, 100, filaDetalle, PdfContentByte.ALIGN_CENTER)

    End Sub
    Private Sub ordenCompra(ByVal writer, ByVal docxml, ByVal numpartida)

        Dim ordenCompra As String = docxml.XmlReadAttribute("ordenCompra", "Detalle", numpartida)
        Dim Amendment As String = docxml.XmlReadAttribute("Amendment", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea(writer, 7, bf, ordenCompra & Amendment, 45, filaDetalle, PdfContentByte.ALIGN_CENTER)

    End Sub
    'Private Sub ordenCompra(ByVal writer, ByVal docxml, ByVal numpartida)
    '    Dim numeroPedido As String = docxml.XmlReadAttribute("ordenCompra", "Detalle", numpartida)
    '    Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
    '    escribeLinea(writer, 7, bf, numeroPedido, 50, filaDetalle, PdfContentByte.ALIGN_CENTER)
    'End Sub

    Private Sub fechaRecibo(ByVal writer, ByVal docXml)
        Dim filafechaRecibo As Integer = 465
        Dim fecha As String = docXml.XmlReadAttribute("fecha", "Comprobante")
        If fecha = Nothing Then
            fecha = ""
        End If

        fecha = fecha.Replace("-", "/")
        fecha = fecha.Substring(0, fecha.IndexOf("T"))
        escribeLinea(writer, 7, BaseFont.HELVETICA, fecha, 430, filafechaRecibo, PdfContentByte.ALIGN_LEFT)
    End Sub

    Private Sub lote(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim lote As String = docxml.XmlReadAttribute("noIdentificacion", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        If lote = Nothing Then
            lote = ""
        End If
        'If lote.Length > 7 Then
        '    lote = lote.Substring(0, 7)
        'End If
        escribeLinea(writer, 7, bf, lote, 150, filaDetalle)


    End Sub
    Private Sub sku(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim sku As String = docxml.XmlReadAttribute("SKU", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        If sku = Nothing Then
            sku = ""
        End If
        If sku.Length > 7 Then
            sku = sku.Substring(0, 7)
        End If
        escribeLinea(writer, 8, bf, sku, 50, filaDetalle)


    End Sub
    Private Sub cantidad(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim cantidad As String = docxml.XmlReadAttribute("cantidad", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea2(writer, 8, bf, cantidad, 101, filaDetalle)
    End Sub
    Private Sub cantidadEntregada(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim cantidadEntregada As String = docxml.XmlReadAttribute("cantidadEntregada", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea(writer, 15, bf, cantidadEntregada, 400, filaDetalle)
    End Sub
    Private Sub unidad(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim unidad As String = docxml.XmlReadAttribute("unidad", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea(writer, 8, bf, unidad, 100, filaDetalle)

    End Sub
    Private Sub noIdMaterial(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim noIdMaterial As String = docxml.XmlReadAttribute("noIdMaterial", "Detalle", numpartida)
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        escribeLinea(writer, 8, bf, noIdMaterial, 373, filaDetalle)

    End Sub
    Function Decimal2Digitos(ByVal valor As String) As String
        Try
            valor = (Math.Truncate(Convert.ToDecimal(valor) * 100)) / 100


        Catch ex As Exception
            valor = "0.00"
        End Try

        Return valor


    End Function
    Private Sub precioUnitario(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim precioUnitario As String = docxml.XmlReadAttribute("precioNeto", "Detalle", numpartida)

        precioUnitario = Decimal2Digitos(precioUnitario)
        precioUnitario = String.Format("{0:#,###.##}", Convert.ToDecimal(precioUnitario))

        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.BeginText()
        cb.SetFontAndSize(bf, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, precioUnitario, 490, filaDetalle, 0)
        cb.EndText()
        'escribeLinea(writer, 9, bf, precioUnitario, 500, PdfContentByte.ALIGN_RIGHT)

    End Sub 'price
    Private Sub precioImporte(ByVal writer, ByVal docxml, ByVal numpartida)
        Dim cb As PdfContentByte = writer.DirectContent

        Dim importeNeto As String = docxml.XmlReadAttribute("importeNeto", "Detalle", numpartida)
        importeNeto = Decimal2Digitos(importeNeto)
        importeNeto = String.Format("{0:#,###.##}", Convert.ToDecimal(importeNeto))
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.BeginText()
        cb.SetFontAndSize(bf, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, importeNeto, 557, filaDetalle, 0)
        cb.EndText()
        'escribeLinea(writer, 9, bf, importeNeto, 585, PdfContentByte.ALIGN_RIGHT)

    End Sub 'amount
    Private Sub descripcion(ByVal writer, ByVal docXml, ByVal numPartidasEnPagina)

        Dim cb As PdfContentByte = writer.DirectContent
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)


        For count = pasadoNumeroDePartidas + 1 To pasadoNumeroDePartidas + numPartidasEnPagina

            'fechaPedido(writer, docXml, count)
            'pedido(writer, docXml, count)
            ordenCompra(writer, docXml, count)
            sku(writer, docXml, count)
            lote(writer, docXml, count)
            cantidad(writer, docXml, count)
            cantidadEntregada(writer, docXml, count)
            precioUnitario(writer, docXml, count)
            precioImporte(writer, docXml, count)
            'unidad(writer, docXml, count)

            Dim descripcion As String = docXml.XmlReadAttribute("descripcion", "Detalle", count)
            Dim noIdMaterial As String = docXml.XmlReadAttribute("noIdMaterial", "Detalle", count)
            If noIdMaterial = Nothing Then
                noIdMaterial = ""
            ElseIf noIdMaterial <> "" Then
                noIdMaterial &= Space(numCaracteres - noIdMaterial.Length)
            End If

            descripcion = noIdMaterial & descripcion
            Dim numFilas As Decimal
            numFilas = descripcion.Length / numCaracteres
            If numFilas > 1 And numFilas Mod numCaracteres <> 0 Then
                numFilas = System.Math.Truncate(numFilas) + 1
            Else
                numFilas = 1
            End If



            If descripcion.Length <= numCaracteres Then
                cb.BeginText()
                cb.SetFontAndSize(bf, 7)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcion, 250, filaDetalle, 0)
                cb.EndText()
                Dim Space1 As Integer = 15
                filaDetalle = filaDetalle - 15
                If docXml.XmlReadAttribute("numeroAduana", "Detalle", count) <> "" Then
                    Dim numeroAduana As String = docXml.XmlReadAttribute("numeroAduana", "Detalle", count)
                    Dim nombreAduana As String = docXml.XmlReadAttribute("nombreAduana", "Detalle", count)

                    'Dim fechaAduana As String = docXml.XmlReadAttribute("fechaAduana", "Detalle", count)
                    Dim aduanas() As String = nombreAduana.Split("|")
                    Dim numerosAd() As String = numeroAduana.Split("|")
                    'Dim fechas() As String = fechaAduana.Split("|")
                    Dim i As Integer = 0
                    Dim adns() As String
                    ReDim adns(aduanas.Length - 1)




                    For i = 0 To numerosAd.Length - 1

                        adns(i) = "Aduana:" & numerosAd(i) & "  " & aduanas(i)

                        cb.SetFontAndSize(bf, 7)
                        cb.BeginText()
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, " " & adns(i), 50, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 11
                    Next

                    'For i = 0 To aduanas.Rank


                    '    cb.SetFontAndSize(bf, 7)
                    '    cb.BeginText()
                    '    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ADUANA: ", 200, filaDetalle, 0)
                    '    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "        " & aduanas(i), 225, filaDetalle, 0)
                    '    cb.EndText()
                    '    filaDetalle = filaDetalle - 11
                    'Next




                    'cb.SetFontAndSize(bf, 7)
                    'cb.BeginText()

                    'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ADUANA: ", 200, filaDetalle, 0)
                    'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "        " & nombreAduana, 200, filaDetalle, 0)
                    'cb.EndText()
                    'filaDetalle = filaDetalle - 15
                    'cb.BeginText()

                    'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NUMERO: ", 200, filaDetalle, 0)
                    'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "        " & numeroAduana, 200, filaDetalle, 0)
                    'cb.EndText()

                    'cb.BeginText()

                    'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FECHA: ", 280, filaDetalle, 0)
                    'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "       " & fechaAduana, 280, filaDetalle, 0)
                    'cb.EndText()
                    'filaDetalle = filaDetalle - 15
                End If
            Else
                Dim descripcionXdeY As String = ""
                Dim posSubstring As Integer = 0
                For count2 As Integer = 1 To numFilas

                    If descripcion.Length - posSubstring >= numCaracteres Then
                        descripcionXdeY = descripcion.Substring(posSubstring, numCaracteres)

                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 230, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteres

                    ElseIf descripcion.Length - posSubstring < numCaracteres Then
                        descripcionXdeY = descripcion.Substring(posSubstring, descripcion.Length - (posSubstring))
                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 230, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteres
                        If docXml.XmlReadAttribute("numeroAduana", "Detalle", count) <> "" Then
                            Dim nombreAduana As String = docXml.XmlReadAttribute("nombreAduana", "Detalle", count)
                            Dim numeroAduana As String = docXml.XmlReadAttribute("numeroAduana", "Detalle", count)
                            Dim fechaAduana As String = docXml.XmlReadAttribute("fechaAduana", "Detalle", count)

                            cb.SetFontAndSize(bf, 7)
                            cb.BeginText()

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "ADUANA: ", 200, filaDetalle, 0)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "        " & nombreAduana, 200, filaDetalle, 0)
                            cb.EndText()
                            filaDetalle = filaDetalle - 15
                            cb.BeginText()

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "NUMERO: ", 200, filaDetalle, 0)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "        " & numeroAduana, 200, filaDetalle, 0)
                            cb.EndText()

                            cb.BeginText()

                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "FECHA: ", 280, filaDetalle, 0)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "       " & fechaAduana, 280, filaDetalle, 0)
                            cb.EndText()
                            filaDetalle = filaDetalle - 15
                        End If
                    End If
                Next
            End If
        Next

        pasadoNumeroDePartidas += numPartidasEnPagina
    End Sub 'description
    Private Sub imprimirTotales(ByVal writer, ByVal docxml)

        Dim cb As PdfContentByte = writer.DirectContent

        Dim subTotalAD As String = docxml.XmlReadAttribute("subTotalantesDeDescuentos", "Totales")
        Dim subTotalDD As String = docxml.XmlReadAttribute("subTotaldespuesDeDescuentos", "Totales")
        Dim impuestos As String = docxml.XmlReadAttribute("importeImpuesto", "Totales")
        Dim importeImpuestoDolares As String = docxml.XmlReadAttribute("importeImpuestoDolares", "Totales")

        Dim totalDolares As String = docxml.XmlReadAttribute("totalDolares", "Totales")
        Dim total As String = docxml.XmlReadAttribute("total", "Totales")
        Dim porcentajeImpuesto As String = docxml.XmlReadAttribute("porcentajeImpuesto", "Totales")

        Dim porcentajeImpuestoRetenido As String = docxml.XmlReadAttribute("porcentajeImpuestoRetenido", "Totales")
        Dim montoImpuestoRetenido As String = "" & docxml.XmlReadAttribute("importeImpuestoRetencion", "Totales")
        subTotalAD = Decimal2Digitos(subTotalAD)
        subTotalDD = Decimal2Digitos(subTotalDD)
        subTotalAD = String.Format("{0:#,###.##}", Convert.ToDecimal(subTotalAD))
        subTotalDD = String.Format("{0:#,###.##}", Convert.ToDecimal(subTotalDD))

        If (importeImpuestoDolares = impuestos) Then
            impuestos = Decimal2Digitos(impuestos)
            impuestos = String.Format("{0:#,###.##}", Convert.ToDecimal(impuestos))
        Else
            importeImpuestoDolares = Decimal2Digitos(importeImpuestoDolares)
            impuestos = String.Format("{0:#,###.##}", Convert.ToDecimal(importeImpuestoDolares))
        End If



        montoImpuestoRetenido = Decimal2Digitos(montoImpuestoRetenido)
        montoImpuestoRetenido = String.Format("{0:#,###.##}", Convert.ToDecimal(montoImpuestoRetenido))
        If (total = totalDolares) Then
            total = Decimal2Digitos(total)
            total = String.Format("{0:#,###.##}", Convert.ToDecimal(total))
        Else
            totalDolares = Decimal2Digitos(totalDolares)
            total = String.Format("{0:#,###.##}", Convert.ToDecimal(totalDolares))
        End If


        escribeLinea(writer, 7, BaseFont.HELVETICA_BOLD, subTotalAD, 570, 84, PdfContentByte.ALIGN_RIGHT)
        escribeLinea(writer, 7, BaseFont.HELVETICA_BOLD, impuestos, 570, 70, PdfContentByte.ALIGN_RIGHT)
        escribeLinea(writer, 7, BaseFont.HELVETICA_BOLD, subTotalDD, 570, 58, PdfContentByte.ALIGN_RIGHT)
        escribeLinea(writer, 7, BaseFont.HELVETICA_BOLD, montoImpuestoRetenido, 570, 44, PdfContentByte.ALIGN_RIGHT)
        escribeLinea(writer, 7, BaseFont.HELVETICA_BOLD, total, 570, 30, PdfContentByte.ALIGN_RIGHT)
        escribeLinea(writer, 9, BaseFont.HELVETICA, porcentajeImpuesto, 460, 70, PdfContentByte.ALIGN_LEFT)

        If (montoImpuestoRetenido <> "0.00") Then
            escribeLinea(writer, 9, BaseFont.HELVETICA, porcentajeImpuesto, 425, 44, PdfContentByte.ALIGN_LEFT)
        End If



        'imprimeAtributo(writer, docxml, "mstCuadrados", "Totales", 67, 150)
        'imprimeAtributo(writer, docxml, "piezas", "Totales", 52, 150)
        'imprimeAtributo(writer, docxml, "bultos", "Totales", 67, 300)
        'Dim peso As String = docxml.XmlReadAttribute("peso", "Totales")
        'imprimeAtributo(writer, docxml, "peso", "Totales", 52, 300)
        'escribeLinea(writer, 9, BaseFont.HELVETICA, peso & " Kg.", 300, 52, PdfContentByte.ALIGN_CENTER)


    End Sub
    Private Sub imprimirCadenaOriginal(ByVal writer, ByVal docxml, ByVal nombrePdf)
        nombrePdf = Convert.ToString(nombrePdf)
        Dim cb As PdfContentByte = writer.DirectContent
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        cb.BeginText()
        cb.SetFontAndSize(bf, 7)

        filaDetalle = filaDetalle - 15
        Dim cadenaOriginal As String = docxml.XmlReadAttribute("cadenaOriginal", "Comprobante")
        Dim numFilas As Decimal
        numFilas = cadenaOriginal.Length / numCaracteresCadOriginal

        If numFilas > 1 And numFilas Mod numCaracteres <> 0 Then
            numFilas = System.Math.Truncate(numFilas) + 1
        Else
            numFilas = 1
        End If

        If numFilas <= totalLineasPagina Then

            'If filaDetalle < 150 Then
            '    filaDetalle = 450

            'End If
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cadena Original:", 40, filaDetalle + 15, 0)
            cb.EndText()
            If filaDetalle - (numFilas * 15) < 50 Then
                doc.NewPage()
                ''ponernumerodepagina(writer)
                filaDetalle = filaDetalleInicial
                crearFormato(doc, docxml, writer, nombrePdf)
                llenarHeader(writer, docxml)
                imprimirCadenaOriginal(writer, docxml, nombrePdf)
            Else


                If cadenaOriginal.Length <= numCaracteresCadOriginal Then
                    cb.BeginText()
                    cb.SetFontAndSize(bf, 7)
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, cadenaOriginal, 40, filaDetalle, 0)

                    cb.EndText()
                    filaDetalle = filaDetalle - 15
                Else
                    Dim descripcionXdeY As String = ""
                    Dim posSubstring As Integer = 0
                    For count2 As Integer = 1 To numFilas

                        If cadenaOriginal.Length - posSubstring >= numCaracteresCadOriginal Then
                            descripcionXdeY = cadenaOriginal.Substring(posSubstring, numCaracteresCadOriginal)

                            cb.BeginText()
                            cb.SetFontAndSize(bf, 7)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                            cb.EndText()
                            filaDetalle = filaDetalle - 15
                            posSubstring += numCaracteresCadOriginal

                        ElseIf cadenaOriginal.Length - posSubstring < numCaracteresCadOriginal Then
                            descripcionXdeY = cadenaOriginal.Substring(posSubstring, cadenaOriginal.Length - (posSubstring))
                            cb.BeginText()
                            cb.SetFontAndSize(bf, 7)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                            cb.EndText()
                            filaDetalle = filaDetalle - 15
                            posSubstring += numCaracteresCadOriginal
                        End If
                    Next
                End If

            End If

            filaDetalle -= 15
        Else
            Dim loops As Decimal = System.Math.Truncate(numFilas / 30) + 1
            Dim posSubstring As Integer = 0
            For i As Integer = 0 To loops - 1
                doc.NewPage()
                ''ponernumerodepagina(writer)

                filaDetalle = filaDetalleInicial
                crearFormato(doc, docxml, writer, nombrePdf)
                llenarHeader(writer, docxml)
                For a As Integer = 0 To totalLineasPagina - 1
                    Dim pedazoCadenaOriginal As String
                    If cadenaOriginal.Length - posSubstring < 99 Then
                        pedazoCadenaOriginal = cadenaOriginal.Substring(posSubstring, cadenaOriginal.Length - posSubstring)

                        a = totalLineasPagina + 1
                        i = loops + 1
                    Else
                        pedazoCadenaOriginal = cadenaOriginal.Substring(posSubstring, 99)
                    End If
                    cb.BeginText()
                    cb.SetFontAndSize(bf, 7)
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, pedazoCadenaOriginal, 40, filaDetalle, 0)
                    cb.EndText()
                    filaDetalle = filaDetalle - 15
                    posSubstring += 99
                Next

            Next
            filaDetalle -= 15
        End If

    End Sub
    Private Sub imprimirSelloDigital(ByVal writer, ByVal docxml, ByVal nombrePdf)
        nombrePdf = Convert.ToString(nombrePdf)

        Dim cb As PdfContentByte = writer.DirectContent
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        cb.BeginText()
        cb.SetFontAndSize(bf, 7)

        filaDetalle = filaDetalle - 15
        Dim sello As String = docxml.XmlReadAttribute("sello", "Comprobante")
        Dim numFilas As Decimal
        numFilas = sello.Length / numCaracteresCadOriginal

        If numFilas > 1 And numFilas Mod numCaracteres <> 0 Then
            numFilas = System.Math.Truncate(numFilas) + 1
        Else
            numFilas = 1
        End If
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Sello Digital:", 40, filaDetalle + 15, 0)
        cb.EndText()
        If filaDetalle - (numFilas * 15) < 50 Then
            doc.NewPage()
            ''ponernumerodepagina(writer)
            filaDetalle = filaDetalleInicial
            crearFormato(doc, docxml, writer, nombrePdf)
            llenarHeader(writer, docxml)
            imprimirSelloDigital(writer, docxml, nombrePdf)
        Else


            If sello.Length <= numCaracteresCadOriginal Then
                cb.BeginText()
                cb.SetFontAndSize(bf, 7)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, sello, 40, filaDetalle, 0)
                cb.EndText()
                filaDetalle = filaDetalle - 15
            Else
                Dim descripcionXdeY As String = ""
                Dim posSubstring As Integer = 0
                For count2 As Integer = 1 To numFilas

                    If sello.Length - posSubstring >= numCaracteresCadOriginal Then
                        descripcionXdeY = sello.Substring(posSubstring, numCaracteresCadOriginal)

                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteresCadOriginal

                    ElseIf sello.Length - posSubstring < numCaracteresCadOriginal Then
                        descripcionXdeY = sello.Substring(posSubstring, sello.Length - (posSubstring))
                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteresCadOriginal
                    End If
                Next
            End If

        End If


    End Sub
    Private Sub imprimirLeyenda(ByVal writer, ByVal docxml, ByVal nombrePdf)
        nombrePdf = Convert.ToString(nombrePdf)

        Dim cb As PdfContentByte = writer.DirectContent
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        cb.BeginText()
        cb.SetFontAndSize(bf, 7)

        filaDetalle = filaDetalle - 15
        Dim leyenda As String = docxml.XmlReadAttribute("nota", "Comprobante")
        Dim numFilas As Decimal
        numFilas = leyenda.Length / numCaracteresCadOriginal

        If numFilas > 1 And numFilas Mod numCaracteres <> 0 Then
            numFilas = System.Math.Truncate(numFilas) + 1
        Else
            numFilas = 1
        End If
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, " ", 40, filaDetalle + 15, 0)
        cb.EndText()
        If filaDetalle - (numFilas * 15) < 50 Then
            doc.NewPage()
            ''ponernumerodepagina(writer)
            filaDetalle = filaDetalleInicial
            crearFormato(doc, docxml, writer, nombrePdf)
            llenarHeader(writer, docxml)
            imprimirLeyenda(writer, docxml, nombrePdf)
        Else


            If leyenda.Length <= numCaracteresCadOriginal Then
                cb.BeginText()
                cb.SetFontAndSize(bf, 7)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, leyenda, 40, filaDetalle, 0)
                cb.EndText()
                filaDetalle = filaDetalle - 15
            Else
                Dim descripcionXdeY As String = ""
                Dim posSubstring As Integer = 0
                For count2 As Integer = 1 To numFilas

                    If leyenda.Length - posSubstring >= numCaracteresCadOriginal Then
                        descripcionXdeY = leyenda.Substring(posSubstring, numCaracteresCadOriginal)

                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteresCadOriginal

                    ElseIf leyenda.Length - posSubstring < numCaracteresCadOriginal Then
                        descripcionXdeY = leyenda.Substring(posSubstring, leyenda.Length - (posSubstring))
                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteresCadOriginal
                    End If
                Next
            End If

        End If


    End Sub
    Private Sub imprObservaciones(ByVal writer, ByVal docxml, ByVal nombrePdf)
        nombrePdf = Convert.ToString(nombrePdf)

        Dim cb As PdfContentByte = writer.DirectContent
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetColorStroke(New Color(105, 105, 105))
        cb.SetLineWidth(0.6)
        cb.BeginText()
        cb.SetFontAndSize(bf, 7)

        'filaDetalle = filaDetalle - 15
        Dim observaciones As String = docxml.XmlReadAttribute("observaciones", "Comprobante")
        Dim numFilas As Decimal
        numFilas = observaciones.Length / numCaracteresCadOriginal

        If numFilas > 1 And numFilas Mod numCaracteres <> 0 Then
            numFilas = System.Math.Truncate(numFilas) + 1
        Else
            numFilas = 1
        End If
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, " ", 40, filaDetalle + 15, 0)
        cb.EndText()
        If filaDetalle - (numFilas * 15) < 50 Then
            doc.NewPage()
            ''ponernumerodepagina(writer)
            filaDetalle = filaDetalleInicial
            crearFormato(doc, docxml, writer, nombrePdf)
            llenarHeader(writer, docxml)
            imprimirLeyenda(writer, docxml, nombrePdf)
        Else


            If observaciones.Length <= numCaracteresCadOriginal Then
                cb.BeginText()
                cb.SetFontAndSize(bf, 7)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, observaciones, 40, filaDetalle, 0)
                cb.EndText()
                filaDetalle = filaDetalle - 15
            Else
                Dim descripcionXdeY As String = ""
                Dim posSubstring As Integer = 0
                For count2 As Integer = 1 To numFilas

                    If observaciones.Length - posSubstring >= numCaracteresCadOriginal Then
                        descripcionXdeY = observaciones.Substring(posSubstring, numCaracteresCadOriginal)

                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteresCadOriginal

                    ElseIf observaciones.Length - posSubstring < numCaracteresCadOriginal Then
                        descripcionXdeY = observaciones.Substring(posSubstring, observaciones.Length - (posSubstring))
                        cb.BeginText()
                        cb.SetFontAndSize(bf, 7)
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, descripcionXdeY, 40, filaDetalle, 0)
                        cb.EndText()
                        filaDetalle = filaDetalle - 15
                        posSubstring += numCaracteresCadOriginal
                    End If
                Next
            End If

        End If


    End Sub

    Private Sub imprimirTextoFormato(ByVal writer, ByVal tipoJustificacion, ByVal texto, ByVal posX, ByVal posY, ByVal tamLetra)
        Dim cb As PdfContentByte = writer.DirectContentUnder
        cb.BeginText()
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        cb.SetFontAndSize(bf, tamLetra)
        cb.ShowTextAligned(tipoJustificacion, texto, posX, posY, 0)
        cb.EndText()
    End Sub
    Private Sub imprimirImporteConLetra(ByVal writer, ByVal docxml)
        Dim importeConLetra As String = docxml.XmlReadAttribute("importeConLetra", "Comprobante")
        Dim decimales As String = docxml.XmlReadAttribute("importeConLetraDecimales", "Comprobante")
        Dim moneda As String = docxml.XmlReadAttribute("codigoISO", "Moneda")
        Dim bf As BaseFont = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        Dim parteCantLetra As String
        Dim y As Integer
        y = 75
        For Each parteCantLetra In IndentWordWrap(importeConLetra, 0, 0, 75)
            escribeLinea(writer, 9, bf, parteCantLetra.ToUpper, 20, y)
            y -= 10
        Next
    End Sub
    Private Function IndentWordWrap(ByVal txtIn As String, ByVal LeftIndent As Byte, ByVal RightIndent As Byte, ByVal TextWidth As Byte) As String()
        Dim i As Integer
        Dim p() As String
        Dim b() As String
        ReDim b(0)

        p = Split(txtIn, " ")
        b(0) = Space(LeftIndent)
        TextWidth = TextWidth + LeftIndent


        For i = 0 To UBound(p)
            If Len(b(UBound(b))) + Len(p(i)) > TextWidth Then
                b(UBound(b)) = Left(b(UBound(b)), Len(b(UBound(b))) - 1)
                b(UBound(b)) = b(UBound(b)) & Space(TextWidth + RightIndent - Len(b(UBound(b))))
                ReDim Preserve b(UBound(b) + 1)
                b(UBound(b)) = Space(LeftIndent) & p(i) & " "
            Else
                b(UBound(b)) = b(UBound(b)) & p(i) & " "
            End If
        Next
        b(UBound(b)) = Left(b(UBound(b)), Len(b(UBound(b))) - 1)
        b(UBound(b)) = b(UBound(b)) & Space(TextWidth + RightIndent - Len(b(UBound(b))))

        IndentWordWrap = b
        Erase b
        Erase p

    End Function

    Private Sub crearFormato(ByVal document As Document, ByVal xmldoc As Avances.Xml.XmlDoc, ByVal writer As PdfWriter, ByVal nombrePdf As String)
        Try
            Dim imagen As Image
            'Dim reader As PdfReader = New PdfReader(nombrePdf)
            Dim formatoPDF As New FileInfo(Path.GetTempFileName)
            tipoDoc = xmldoc.XmlReadAttribute("tipoDeComprobante", "Comprobante")


            Select Case xmldoc.XmlReadAttribute("rfc", "Emisor")
                Case "IMI0708287XA"
                    formatoPDF = imagenes.getTempResourceFile("Utiles.IACNAII.pdf")
                Case "IHE070904PW4"
                    formatoPDF = imagenes.getTempResourceFile("Utiles.IACNAHermosillo.pdf")
                Case "IMS070226LF6"
                    formatoPDF = imagenes.getTempResourceFile("Utiles.IACNAServicios.pdf")
                Case Else
                    formatoPDF = imagenes.getTempResourceFile("Utiles.IACNA.pdf")
            End Select

            Dim reader1 As PdfReader = New PdfReader(formatoPDF.FullName)
            Dim psize1 As Rectangle = reader1.GetPageSize(1)
            Dim width1 As Single = psize1.Width
            Dim height1 As Single = psize1.Height
            Dim document1 As Document = New Document(psize1, 50, 50, 50, 50)
            Dim formatoPDFResult As String = ""



            Dim cb As PdfContentByte = writer.DirectContent
            Dim n As Integer = reader1.NumberOfPages

            Dim pdfPath1 As New System.IO.FileInfo(formatoPDF.FullName)


            'Dim writer1 As PdfWriter = PdfWriter.GetInstance(document, New FileStream("c:/pdf_tmp.pdf", FileMode.Create))


            document.Open()

            document.NewPage()
            Dim page1 As PdfImportedPage = writer.GetImportedPage(reader1, 1)

            cb.AddTemplate(page1, 0, 0)


            'document.Close()

            'Dim logo As New FileInfo(Path.GetTempFileName)
            'logo = imagenes.getTempResourceFile("Utiles.logoMetaldyne.jpg")
            'imagen = Image.GetInstance(logo.FullName)
            'imagen.ScalePercent(70)
            'imagen.SetAbsolutePosition(25, 720)
            'doc.Add(imagen)
            'File.Delete(logo.FullName)

            'Dim ced As New FileInfo(Path.GetTempFileName)
            'ced = imagenes.getTempResourceFile("Utiles.cedula3.jpg")
            'imagen = Image.GetInstance(ced.FullName)
            'imagen.ScaleToFit(130, 130)
            'imagen.SetAbsolutePosition(27, 21)
            'doc.Add(imagen)
            'File.Delete(logo.FullName)
            'escribeLinea(writer, 7, BaseFont.TIMES_ROMAN, "ESTE DOCUMENTO ES UNA IMPRESION DE UN COMPROBANTE FISCAL DIGITAL", 45, 81)



        Catch de As DocumentException
            Console.Error.WriteLine(de.Message)
        Catch ioe As IOException
            Console.Error.WriteLine(ioe.Message)
        End Try
    End Sub
    Private Sub imprime()



        Dim psi As New ProcessStartInfo
        psi.Arguments = "/t " & Chr(34) & "c:/pdf/a66420.pdf" & Chr(34) & " " & Chr(34) & "\\desarrollo22\A" & Chr(34)
        psi.CreateNoWindow = False

        psi.RedirectStandardOutput = True
        psi.UseShellExecute = False
        psi.FileName = "C:/Program Files/Adobe/Reader 8.0/Reader/AcroRd32.exe"
        Process.Start(psi)

    End Sub


End Class
