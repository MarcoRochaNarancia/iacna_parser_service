using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace pager
{
    public class Class1
    {
        public Class1()
        {
            try
            {
                // we create a reader for a certain document
                PdfReader reader = new PdfReader(@"C:\lolo.pdf");
                // we retrieve the total number of pages
                int n = reader.NumberOfPages;
                // we retrieve the size of the first page
                Rectangle psize = reader.GetPageSize(1);
                float width = psize.Width;
                float height = psize.Height;

                // step 1: creation of a document-object
                Document document = new Document(psize, 50, 50, 50, 50);
                // step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream("C:/RESULT.pdf", FileMode.Create));
                // step 3: we open the document

                document.Open();
                // step 4: we Add content
                PdfContentByte cb = writer.DirectContent;
                int i = 0;
                int p = 0;

                document.NewPage();
                document.NewPage();

                p++;
                i++;
                PdfImportedPage page1 = writer.GetImportedPage(reader, i);
                cb.AddTemplate(page1, 0, 0);

                cb.SetRGBColorStroke(255, 0, 0);
                cb.MoveTo(0, height / 2);
                cb.LineTo(width, height / 2);
                cb.Stroke();
                cb.MoveTo(width / 2, height);
                cb.LineTo(width / 2, 0);

                cb.Stroke();
                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                cb.BeginText();
                cb.SetFontAndSize(bf, 14);
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "paFDSNFKSDJGKFGge", width / 2, 40, 0);
                cb.EndText();

                // step 5: we close the document
                document.Close();
            }
            catch (Exception de)
            {
                Console.Error.WriteLine(de.Message);
                Console.Error.WriteLine(de.StackTrace);
            }
        }

    }
}
