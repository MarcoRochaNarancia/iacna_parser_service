using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Avances
{
    public class Utils
    {
        public static FileInfo getTempResourceFile(string resource)
        {
            Assembly _assembly = Assembly.GetExecutingAssembly();
            FileInfo tmpfile = new FileInfo(Path.GetTempFileName());

            using (FileStream ofs = new FileStream(tmpfile.FullName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                using (StreamWriter xml_wtr = new StreamWriter(ofs, System.Text.Encoding.Default))
                {
                    string data;
                    StreamReader aby_rdr = new StreamReader(_assembly.GetManifestResourceStream(resource), System.Text.Encoding.Default);

                    data = aby_rdr.ReadToEnd();
                    aby_rdr.Close();

                    xml_wtr.Write(data);
                    xml_wtr.Flush();
                    xml_wtr.Close();
                }
            }

            return tmpfile;
        }
    }
}
