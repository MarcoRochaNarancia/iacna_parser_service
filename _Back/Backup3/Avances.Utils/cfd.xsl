<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">

    <xsl:element name="Comprobante">
      <xsl:attribute name="version">
        <xsl:text>2.2</xsl:text>
      </xsl:attribute>
      <xsl:if test="/Avances/Encabezado/Comprobante/@serie">
        <xsl:attribute name="serie">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="folio">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
      </xsl:attribute>
      <xsl:attribute name="fecha">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@fecha"/>
      </xsl:attribute>
      <xsl:attribute name="sello"/>
      <xsl:attribute name="noAprobacion">0</xsl:attribute>
      <xsl:attribute name="anoAprobacion">0</xsl:attribute>
      <xsl:attribute name="formaDePago">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@formaDePago"/>
      </xsl:attribute>

      <xsl:attribute name="noCertificado">00000000000000000000</xsl:attribute>
      <xsl:attribute name="certificado"/>
      <xsl:if test="string-length(/Avances/Encabezado/Comprobante/@condicionesDePago) &gt; 0">
        <xsl:attribute name="condicionesDePago">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@condicionesDePago"/>
        </xsl:attribute>
      </xsl:if>
      <!--<xsl:attribute name="subTotal">
        <xsl:value-of select="format-number(/Avances/Totales/@subTotalantesDeDescuentos,'############.00')"/>
      </xsl:attribute>-->
      <!--<xsl:attribute name="subTotal">
        <xsl:value-of select="/Avances/Totales/@subTotalantesDeDescuentos"/>
      </xsl:attribute>-->
      <xsl:choose>
        <xsl:when test ="contains( /Avances/Totales/@subTotalantesDeDescuentos, '.' )">
          <xsl:attribute name="subTotal">
            <xsl:value-of select="concat ( substring-before( /Avances/Totales/@subTotalantesDeDescuentos, '.' ) , '.' , substring ( substring-after ( /Avances/Totales/@subTotalantesDeDescuentos, '.' ) , 1 , 4 ) )"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="subTotal">
            <xsl:value-of select="format-number(/Avances/Totales/@subTotalantesDeDescuentos,'############.####')"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:if test="string-length(/Avances/Totales/@importeDescuento) &gt; 0">
        <xsl:choose>
          <xsl:when test ="contains( /Avances/Totales/@importeDescuento, '.' )">
            <xsl:attribute name="descuento">
              <xsl:value-of select="concat ( substring-before( /Avances/Totales/@importeDescuento, '.' ) , '.' , substring ( substring-after ( /Avances/Totales/@importeDescuento, '.' ) , 1 , 4 ) )"/>
            </xsl:attribute>
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="descuento">
              <xsl:value-of select="format-number(/Avances/Totales/@importeDescuento,'###########0.00')"/>
            </xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
        <!--<xsl:attribute name="descuento">
          <xsl:value-of select="/Avances/Totales/@importeDescuento"/>
        </xsl:attribute>-->
        <xsl:attribute name="motivoDescuento">
          <xsl:value-of select="/Avances/Totales/@motivoDescuento"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="string-length(/Avances/Encabezado/Moneda/@tipoCambio) &gt; 0">
        <xsl:attribute name="TipoCambio">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@tipoCambio"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="string-length(/Avances/Encabezado/Moneda/@codigoISO) &gt; 0">
        <xsl:attribute name="Moneda">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>
        </xsl:attribute>
      </xsl:if>
      <!--<xsl:attribute name="total">
        <xsl:value-of select="format-number(/Avances/Totales/@total,'############.00')"/>
      </xsl:attribute>-->
      <!--<xsl:attribute name="total">
        <xsl:value-of select="/Avances/Totales/@total"/>
      </xsl:attribute>-->
      <xsl:choose>
        <xsl:when test ="contains( /Avances/Totales/@total, '.' )">
          <xsl:attribute name="total">
            <xsl:value-of select="concat ( substring-before( /Avances/Totales/@total, '.' ) , '.' , substring ( substring-after ( /Avances/Totales/@total, '.' ) , 1 , 4 ) )"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="total">
            <xsl:value-of select="format-number(/Avances/Totales/@total,'###########0.00')"/>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:attribute name="metodoDePago">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@metodoDePago"/>
      </xsl:attribute>
      <xsl:attribute name="LugarExpedicion">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@LugarExpedicion"/>
      </xsl:attribute>
      <xsl:if test="string-length(/Avances/Encabezado/Comprobante/@numCtaPago) &gt; 0">
        <xsl:attribute name="NumCtaPago">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@numCtaPago"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="string-length(/Avances/Encabezado/Parcialidades/@folioFiscalOrig) &gt; 0">
        <xsl:attribute name="FolioFiscalOrig">
          <xsl:value-of select="/Avances/Encabezado/Parcialidades/@folioFiscalOrig"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="string-length(/Avances/Encabezado/Parcialidades/@serieFolioFiscalOrig) &gt; 0">
        <xsl:attribute name="SerieFolioFiscalOrig">
          <xsl:value-of select="/Avances/Encabezado/Parcialidades/@serieFolioFiscalOrig"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="string-length(/Avances/Encabezado/Parcialidades/@fechaFolioFiscalOrig) &gt; 0">
        <xsl:attribute name="FechaFolioFiscalOrig">
          <xsl:value-of select="/Avances/Encabezado/Parcialidades/@fechaFolioFiscalOrig"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="string-length(/Avances/Encabezado/Parcialidades/@montoFolioFiscalOrig) &gt; 0">
        <xsl:attribute name="MontoFolioFiscalOrig">
          <xsl:value-of select="/Avances/Encabezado/Parcialidades/@montoFolioFiscalOrig"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="tipoDeComprobante">
        <xsl:choose>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'INVOICE1'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'INVOICE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'DEBIT_NOTE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CREDIT_NOTE'">
            <xsl:text>egreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'NOTA DE CREDITO'">
            <xsl:text>egreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'CHARGE_NOTE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'HONORARY_RECIPT'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'PARTIAL_INVOICE'">
            <xsl:text>ingreso</xsl:text>
          </xsl:when>
          <xsl:when test="/Avances/Encabezado/Comprobante/@tipoDeComprobante = 'TRANSPORT_DOCUMENT'">
            <xsl:text>traslado</xsl:text>
          </xsl:when>
        </xsl:choose>
      </xsl:attribute>

      <xsl:element name="Emisor">
        <xsl:attribute name="rfc">
          <xsl:value-of select="/Avances/Encabezado/Emisor/@rfc"/>
        </xsl:attribute>
        <xsl:attribute name="nombre">
          <xsl:value-of select="/Avances/Encabezado/Emisor/@nombre"/>
        </xsl:attribute>
        <xsl:element name="DomicilioFiscal">
          <xsl:attribute name="calle">
            <xsl:value-of select="/Avances/Encabezado/Emisor/@calle"/>
          </xsl:attribute>
          <xsl:if test="/Avances/Encabezado/Emisor/@noExterior">
            <xsl:attribute name="noExterior">
              <xsl:value-of select="/Avances/Encabezado/Emisor/@noExterior"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/Avances/Encabezado/Emisor/@noInterior">
            <xsl:attribute name="noInterior">
              <xsl:value-of select="/Avances/Encabezado/Emisor/@noInterior"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/Avances/Encabezado/Emisor/@colonia">
            <xsl:attribute name="colonia">
              <xsl:value-of select="/Avances/Encabezado/Emisor/@colonia"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/Avances/Encabezado/Emisor/@localidad">
            <xsl:attribute name="localidad">
              <xsl:value-of select="/Avances/Encabezado/Emisor/@localidad"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/Avances/Encabezado/Emisor/@referencia">
            <xsl:attribute name="referencia">
              <xsl:value-of select="/Avances/Encabezado/Emisor/@referencia"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="municipio">
            <xsl:value-of select="/Avances/Encabezado/Emisor/@municipio"/>
          </xsl:attribute>
          <xsl:attribute name="estado">
            <xsl:value-of select="/Avances/Encabezado/Emisor/@estado"/>
          </xsl:attribute>
          <xsl:attribute name="pais">
            <xsl:value-of select="/Avances/Encabezado/Emisor/@pais"/>
          </xsl:attribute>
          <xsl:attribute name="codigoPostal">
            <xsl:value-of select="/Avances/Encabezado/Emisor/@codigoPostal"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:if test="/Avances/Encabezado/ExpedidoEn">
          <xsl:element name="ExpedidoEn">
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@calle">
              <xsl:attribute name="calle">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@calle"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@noExterior">
              <xsl:attribute name="noExterior">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@noExterior"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@noInterior">
              <xsl:attribute name="noInterior">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@noInterior"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@colonia">
              <xsl:attribute name="colonia">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@colonia"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@localidad">
              <xsl:attribute name="localidad">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@localidad"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@referencia">
              <xsl:attribute name="referencia">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@referencia"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@municipio">
              <xsl:attribute name="municipio">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@municipio"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@estado">
              <xsl:attribute name="estado">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@estado"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="pais">
              <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@pais"/>
            </xsl:attribute>
            <xsl:if test="/Avances/Encabezado/ExpedidoEn/@codigoPostal">
              <xsl:attribute name="codigoPostal">
                <xsl:value-of select="/Avances/Encabezado/ExpedidoEn/@codigoPostal"
                                />
              </xsl:attribute>
            </xsl:if>
          </xsl:element>
        </xsl:if>
        <xsl:element name="RegimenFiscal">
          <xsl:attribute name="Regimen">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@regimen"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:element>

      <xsl:element name="Receptor">
        <xsl:attribute name="rfc">
          <xsl:value-of select="/Avances/Encabezado/Receptor/@rfc"/>
        </xsl:attribute>
        <xsl:if test="/Avances/Encabezado/Receptor/@nombre">
          <xsl:attribute name="nombre">
            <xsl:value-of select="/Avances/Encabezado/Receptor/@nombre"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:element name="Domicilio">
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@calle) &gt; 0">
            <xsl:attribute name="calle">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@calle"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@noExterior) &gt; 0">
            <xsl:attribute name="noExterior">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@noExterior"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@noInterior) &gt; 0">
            <xsl:attribute name="noInterior">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@noInterior"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@colonia) &gt; 0">
            <xsl:attribute name="colonia">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@colonia"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@localidad) &gt; 0">
            <xsl:attribute name="localidad">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@localidad"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@referencia) &gt; 0">
            <xsl:attribute name="referencia">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@referencia"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="/Avances/Encabezado/Receptor/@municipio">
            <xsl:attribute name="municipio">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@municipio"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Receptor/@estado) &gt; 0">
            <xsl:attribute name="estado">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@estado"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:attribute name="pais">
            <xsl:value-of select="/Avances/Encabezado/Receptor/@pais"/>
          </xsl:attribute>
          <xsl:if test="/Avances/Encabezado/Receptor/@codigoPostal">
            <xsl:attribute name="codigoPostal">
              <xsl:value-of select="/Avances/Encabezado/Receptor/@codigoPostal"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:element>

      <xsl:element name="Conceptos">
        <xsl:for-each select="/Avances/Detalles/Detalle">
          <xsl:element name="Concepto">
            <xsl:attribute name="cantidad">
              <xsl:value-of select="@cantidad"/>
            </xsl:attribute>
            <xsl:attribute name="unidad">
              <xsl:value-of select="@unidad"/>
            </xsl:attribute>
            <xsl:if test ="@noIdentificacion">
              <xsl:attribute name="noIdentificacion">
                <xsl:value-of select="@noIdentificacion"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="descripcion">
              <xsl:value-of select="@descripcion"/>
            </xsl:attribute>
            <!--<xsl:attribute name="valorUnitario">
              <xsl:value-of select="format-number(@precioNeto,'############.00')"/>
            </xsl:attribute>
            <xsl:attribute name="importe">
              <xsl:value-of select="format-number(@importeNeto,'############.00')"/>
            </xsl:attribute>-->
            <!--<xsl:attribute name="valorUnitario">
              <xsl:value-of select="@precioNeto"/>
            </xsl:attribute>
            <xsl:attribute name="importe">
              <xsl:value-of select="@importeNeto"/>
            </xsl:attribute>-->
            <xsl:choose>
              <xsl:when test ="contains( @precioNeto, '.' )">
                <xsl:attribute name="valorUnitario">
                  <xsl:value-of select="concat ( substring-before( @precioNeto, '.' ) , '.' , substring ( substring-after ( @precioNeto, '.' ) , 1 , 4 ) )"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="valorUnitario">
                  <xsl:value-of select="format-number(@precioNeto,'###########0.0000')"/>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test ="contains( @importeNeto, '.' )">
                <xsl:attribute name="importe">
                  <xsl:value-of select="concat ( substring-before( @importeNeto, '.' ) , '.' , substring ( substring-after ( @importeNeto, '.' ) , 1 , 4 ) )"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="importe">
                  <xsl:value-of select="format-number(@importeNeto,'###########0.00')"/>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="string-length(@fechaAduana) &gt; 0">

              <xsl:element name="InformacionAduanera">
                <xsl:attribute name="numero">
                  <xsl:value-of select="@pedimentoAduana"/>
                </xsl:attribute>
                <xsl:attribute name="fecha">
                  <xsl:value-of select="@fechaAduana"/>
                </xsl:attribute>
                <xsl:attribute name="aduana">
                  <xsl:value-of select="@nombreAduana"/>
                </xsl:attribute>
              </xsl:element>
            </xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>

      <xsl:element name="Impuestos">
        <xsl:if test ="string-length(/Avances/Totales/@importeImpuestoRetencion) &gt; 0">
          <xsl:choose>
            <xsl:when test ="contains( /Avances/Totales/@importeImpuestoRetencion, '.' )">
              <xsl:attribute name="totalImpuestosRetenidos">
                <xsl:value-of select="concat ( substring-before( /Avances/Totales/@importeImpuestoRetencion, '.' ) , '.' , substring ( substring-after ( /Avances/Totales/@importeImpuestoRetencion, '.' ) , 1 , 4 ) )"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="totalImpuestosRetenidos">
                <xsl:value-of select="format-number(/Avances/Totales/@importeImpuestoRetencion,'###########0.0000')"/>
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>

          <!--<xsl:attribute name="totalImpuestosRetenidos">
            <xsl:value-of select="/Avances/Totales/@importeImpuestoRetencion"/>
          </xsl:attribute>-->
        </xsl:if>

        <xsl:if test ="string-length(/Avances/Totales/@importeImpuesto) &gt; 0">
          <xsl:choose>
            <xsl:when test ="contains(/Avances/Totales/@importeImpuesto, '.' )">
              <xsl:attribute name="totalImpuestosTrasladados">
                <xsl:value-of select="concat ( substring-before( /Avances/Totales/@importeImpuesto, '.' ) , '.' , substring ( substring-after ( /Avances/Totales/@importeImpuesto, '.' ) , 1 , 4 ) )"/>
              </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="totalImpuestosTrasladados">
                <xsl:value-of select="format-number(/Avances/Totales/@importeImpuesto,'###########0.0000')"/>
              </xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <!--<xsl:attribute name="totalImpuestosTrasladados">
            <xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
          </xsl:attribute>-->
        </xsl:if>


        <xsl:element name="Retenciones">
          <xsl:element name="Retencion">
            <xsl:attribute name ="impuesto">
              <xsl:text>IVA</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="importe">
              <xsl:choose>
                <xsl:when test ="/Avances/Totales/@importeImpuestoRetencion">
                  <xsl:value-of select="/Avances/Totales/@importeImpuestoRetencion"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text> 0.0000 </xsl:text>
                </xsl:otherwise>
              </xsl:choose>

            </xsl:attribute>
          </xsl:element>
        </xsl:element>


        <xsl:element name="Traslados">
          <xsl:element name="Traslado">
            <xsl:attribute name="impuesto">
              <xsl:text>IVA</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="tasa">
              <xsl:value-of select="/Avances/Totales/@porcentajeImpuesto"/>
            </xsl:attribute>
            <xsl:choose>
              <xsl:when test ="contains(/Avances/Totales/@importeImpuesto, '.' )">
                <xsl:attribute name="importe">
                  <xsl:value-of select="concat ( substring-before( /Avances/Totales/@importeImpuesto, '.' ) , '.' , substring ( substring-after ( /Avances/Totales/@importeImpuesto, '.' ) , 1 , 2 ) )"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="importe">
                  <xsl:value-of select="format-number(/Avances/Totales/@importeImpuesto,'###########0.0000')"/>
                </xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <!--<xsl:attribute name="importe">
              <xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
            </xsl:attribute>-->
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
