<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="Encabezado">
			<xsl:attribute name="oc">
				<xsl:value-of select="concat(substring-before(/Avances/Encabezado/Comprobante/@numeroOrden,'-'),substring-after(/Avances/Encabezado/Comprobante/@numeroOrden,'-'))"/>
			</xsl:attribute>
			<xsl:attribute name="folio">
				<xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
			</xsl:attribute>
			<xsl:attribute name="serie">
				<xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>
			</xsl:attribute>
			<xsl:attribute name="fecha">
				<xsl:value-of select="concat(substring(/Avances/Encabezado/Comprobante/@fecha,9,2),'/',substring(/Avances/Encabezado/Comprobante/@fecha,6,2),'/',substring(/Avances/Encabezado/Comprobante/@fecha,1,4))"/>
			</xsl:attribute>
			<xsl:attribute name="proveedor">
				<xsl:value-of select="/Avances/Encabezado/Emisor/@numeroProveedor"/>
			</xsl:attribute>
			<xsl:attribute name="subtotal">
				<xsl:value-of select="/Avances/Totales/@subTotalantesDeDescuentos"/>
			</xsl:attribute>
			<xsl:attribute name="iva">
				<xsl:value-of select="/Avances/Totales/@importeImpuesto"/>
			</xsl:attribute>
			<xsl:attribute name="total">
				<xsl:value-of select="/Avances/Totales/@total"/>
			</xsl:attribute>
			<xsl:attribute name="divisa">
				<xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>
			</xsl:attribute>
			<xsl:attribute name="tipoCambio">
				<xsl:text>1</xsl:text>
			</xsl:attribute>
      <xsl:attribute name="descuento1">
        <xsl:text>0</xsl:text>
      </xsl:attribute>

			<xsl:for-each select="/Avances/Detalles/Detalle">
				<xsl:element name ="Detalle">
					<xsl:attribute name="SKU">
						<xsl:value-of select="@SKU"/>
					</xsl:attribute>
					<xsl:attribute name="cantidad">
						<xsl:value-of select="@cantidad"/>
					</xsl:attribute>
					<xsl:attribute name="descripcion">
						<xsl:value-of select="@descripcion"/>
					</xsl:attribute>
					<xsl:attribute name="valor">
						<xsl:value-of select="@precioNeto"/>
					</xsl:attribute>
					<xsl:attribute name="unidad">
						<xsl:value-of select="@UnidaddeMedida"/>
					</xsl:attribute>
					<xsl:attribute name="IVA">
						<xsl:text>15</xsl:text>
					</xsl:attribute>
					<xsl:attribute name="vendorPack">
						<xsl:value-of select="1"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:for-each>
			
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>