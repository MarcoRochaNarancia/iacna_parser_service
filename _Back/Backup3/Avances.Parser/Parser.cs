using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Text;

namespace Avances
{
    public static class Parser
    {
        public static Dictionary<string, string>[] Parse(StreamReader tmpBuffer)
        {
            string sTmp;
            string[] sTmpArr;
            int facturas = 0;
            int indice = 0;
            Dictionary<string, string>[] buffer;
            Queue<string> tempQueue = new Queue<string>();

            //Se agrega cada linea del spool a una cola
            do
            {
                sTmp = tmpBuffer.ReadLine();

                if (sTmp != "")
                {
                    sTmp = sTmp.Trim();
                    tempQueue.Enqueue(sTmp);
                }
            } while (!tmpBuffer.EndOfStream);

            tmpBuffer = null;

            //Se cuenta el numero de facturas por spool, buscando una variable que no se repita por cada factura
            foreach (string s in tempQueue)
            {
                if (s.Contains("PRINTER"))
                    facturas++;
            }


            buffer = new Dictionary<string, string>[facturas];

            for (int i = 0; i < facturas; i++)
            {
                if (buffer[i] == null)
                    buffer[i] = new Dictionary<string, string>();
                bool finfact = false;
                do
                {
                    try
                    {
                        sTmp = tempQueue.Dequeue();
                    }
                    catch
                    {
                        finfact = true;
                    }
                    if (sTmp != "")                            
                    {
                        sTmpArr = sTmp.Split(" ".ToCharArray(), 2);//KeyPairParser(sTmp);
                        try
                        {
                            if (sTmpArr != null)
                            {
                                if (sTmpArr[0] == "DVBAP-KWMENG" || sTmpArr[0] == "DVBDPR-VRKME" || sTmpArr[0] == "DVBDPR-ARKTX" || sTmpArr[0] == "DKOMVD-KBETR"
                                    || sTmpArr[0] == "DKOMVD-KWERT" || sTmpArr[0] == "DVBDPR-IDNKD" || sTmpArr[0] == "DVBDPR-MATNR" || sTmpArr[0] == "DVBDPR-FKIMG"
                                    || sTmpArr[0] == "DVBDPR-IDNKD" || sTmpArr[0] == "DZVATTOT")
                                {
                                    sTmpArr[0] = sTmpArr[0].Trim();
                                    sTmpArr[1] = sTmpArr[1].Replace(",","").Trim();
                                    buffer[i].Add(sTmpArr[0] + "[" + indice + "]", sTmpArr[1]);
                                }
                                else
                                {
                                    sTmpArr[0] = sTmpArr[0].Trim();
                                    sTmpArr[1] = sTmpArr[1].Trim();
                                    buffer[i].Add(sTmpArr[0], sTmpArr[1]);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException ioore)
                        {
                            try
                            {
                                buffer[i].Add(sTmpArr[0], " ");
                            }
                            catch
                            {}
                        }
                        catch (ArgumentException ae)
                        {
                            if (sTmpArr[0] == "DVBDPR-IDNKD")
                            {
                                indice++;
                                sTmpArr[0] = sTmpArr[0].Trim();
                                sTmpArr[1] = sTmpArr[1].Trim();
                                buffer[i].Add(sTmpArr[0] + "[" + indice + "]", sTmpArr[1]);
                            }else if (sTmpArr[0] == "DKOMVD-KWERT")
                            {
                                if (buffer[i].ContainsKey("DKOMK-SUPOS"))
                                {
                                    if (buffer[i].ContainsKey("Subtotal"))
                                        buffer[i].Add("Iva", sTmpArr[1]);
                                    else
                                        buffer[i].Add("Subtotal", sTmpArr[1]);
                                }
                                     
                            }else{
                                if (sTmpArr[1].Contains("PRINTER"))
                                {
                                    finfact = true;
                                    buffer[i + 1] = new Dictionary<string, string>();
                                    sTmpArr[0] = sTmpArr[0].Trim();
                                    sTmpArr[1] = sTmpArr[1].Trim();
                                    buffer[i + 1].Add(sTmpArr[0], sTmpArr[1]);
                                    indice = 0;
                                }
                            }
                        }
                        
                    }
                } while (!finfact);

            

            }
            return buffer;
        }
        //que es esto??
        private static string[] KeyPairParser(string linea)
        {
            string[] ret = new string[2];

            if (linea.Contains("SERIE"))
            {
                ret[0] = "serie";
                ret[1] = linea.Replace("DSPELLAMOUNT", "");
            }
            else if (linea.Contains("FOLIO"))
            {
                ret[0] = "folio";
                ret[1] = linea.Replace("DSPELLAMOUNT", "");
            }
            else if (linea.Contains("DMEXSTAMP"))
            {
                ret[0] = "fecha";
                ret[1] = linea.Replace("DMEXSTAMP", "");
            }
            else if (linea.Contains("DSPELLAMOUNT"))
            {
                ret[0] = "importeConLetra";
                ret[1] = linea.Replace("DSPELLAMOUNT","");
            }
            else if (linea.Contains("DVBDKR-BSTNK"))
            {
                ret[0] = "numeroPedido";
                ret[1] = linea.Replace("DVBDKR-BSTNK", "");
            }
            else if (linea.Contains("DVBDKR-AUDAT_VAUF"))
            {
                ret[0] = "fechaPedido";
                ret[1] = linea.Replace("DVBDKR-AUDAT_VAUF", "");
            }
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "numeroProveedor";
                ret[1] = linea.Replace("DVENDOR", "");
            }

            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "rfcReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "nombreReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "calleReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "noIntReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "referenciaReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "municipioReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }
            //else if (linea.Contains("DVENDOR"))
            //{
            //    ret[0] = "paisReceptor";
            //    ret[1] = linea.Replace("DVENDOR", "");
            //}
            else if (linea.Contains("DVENDOR"))
            {
                ret[0] = "cpReceptor";
                ret[1] = linea.Replace("DVENDOR", "");
            }

            else
            {
                ret = null;
            }

            return ret;
        }
    }
}
