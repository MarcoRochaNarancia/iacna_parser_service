using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Xml;
using System.Text;

namespace Avances
{
    public class KPToXML
    {
        private Dictionary<string, string> buffer;
        private XmlDocument xmlstd;
        private XmlAttribute atributo;
        private int addenda;

        public KPToXML(Dictionary<string, string> tmp)
        {
            xmlstd = new XmlDocument();
            buffer = tmp;
            tmp = null;
            xmlstd.AppendChild(Encode("1.0", "UTF-8", "yes"));
            xmlstd.AppendChild(Avances());
            int x = 0;
        }
        public XmlDocument XmlStd
        {
            get
            {
                return xmlstd;
            }
        }

        private XmlDeclaration Encode(string ver, string enc, string stan)
        {
            return xmlstd.CreateXmlDeclaration(ver, enc, stan);
        }

        private XmlElement Avances()
        {
            XmlElement avances = xmlstd.CreateElement("Avances");
            avances.AppendChild(Encabezado());
            avances.AppendChild(Detalles());
            avances.AppendChild(Totales());
            avances.AppendChild(Resto());
            return avances;
        }

        private XmlElement Resto()
        {
            XmlElement otros = xmlstd.CreateElement("Otros");
            foreach (KeyValuePair<string, string> kvp1 in buffer)
            {
                string key = kvp1.Key.Replace('[', '_');
                key = key.Replace("]", "");
                otros.Attributes.Append(CrearAtributo1(ref buffer, key, kvp1.Key));
            }
            return otros;
        }
        private XmlElement Encabezado()
        {
            XmlElement encabezado = xmlstd.CreateElement("Encabezado");
            XmlElement tmp;
            encabezado.AppendChild(Comprobante());
            encabezado.AppendChild(Emisor());
            encabezado.AppendChild(EnviarA());
            encabezado.AppendChild(shipTo());
            encabezado.AppendChild(Receptor());
            encabezado.AppendChild(Moneda());
            encabezado.AppendChild(Addenda());

            return encabezado;
        }
        private XmlElement Detalles()
        {
            XmlElement detalles = xmlstd.CreateElement("Detalles");
            int i = 0;
            foreach (KeyValuePair<string, string> kvp1 in buffer)
            {
                if (kvp1.Key.Contains("unidad["))
                    i++;
            }
            for (int j = 0; j < i; j++)
            {
                detalles.AppendChild(Detalle(j + 1));
            }
            return detalles;
        }
        private XmlElement Totales()
        {
            XmlElement totales = xmlstd.CreateElement("Totales");
            try
            {
                if (buffer["totalPesos"] != "")
                {
                    totales.Attributes.Append(CrearAtributo1(ref buffer, "total", "Total"));
                    totales.Attributes.Append(CrearAtributo(ref buffer, "totalPesos", "totalPesos"));
                    totales.Attributes.Append(CrearAtributo(ref buffer, "totalDolares", "Total"));
                }
                else
                {
                    totales.Attributes.Append(CrearAtributo1(ref buffer, "total", "Total"));
                    totales.Attributes.Append(CrearAtributo1(ref buffer, "totalPesos", "Total"));
                    totales.Attributes.Append(CrearAtributo(ref buffer, "totalDolares", "Total"));
                }
            }
            catch
            {
                totales.Attributes.Append(CrearAtributo1(ref buffer, "total", "Total"));
                totales.Attributes.Append(CrearAtributo1(ref buffer, "totalPesos", "Total"));
                totales.Attributes.Append(CrearAtributo(ref buffer, "totalDolares", "Total"));
            }
            try
            {
                if (buffer["impuestoPesos"] != "")
                {
                    totales.Attributes.Append(CrearAtributo1(ref buffer, "importeImpuesto", "importeImpuesto"));
                    totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuestoPesos", "impuestoPesos"));
                    totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuestoDolares", "importeImpuesto"));
                }
                else
                {
                    totales.Attributes.Append(CrearAtributo1(ref buffer, "importeImpuesto", "importeImpuesto"));
                    totales.Attributes.Append(CrearAtributo1(ref buffer, "importeImpuestoPesos", "importeImpuesto"));
                    totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuestoDolares", "importeImpuesto"));
                }
            }
            catch
            {
                totales.Attributes.Append(CrearAtributo1(ref buffer, "importeImpuesto", "importeImpuesto"));
                totales.Attributes.Append(CrearAtributo1(ref buffer, "importeImpuestoPesos", "importeImpuesto"));
                totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuestoDolares", "importeImpuesto"));
            }

            //totales.Attributes.Append(CrearAtributo(ref buffer, "total", "Total"));
            totales.Attributes.Append(CrearAtributo(ref buffer, "porcentajeImpuesto", "porcentajeImpuesto"));
            //totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuesto", "importeImpuesto"));
            totales.Attributes.Append(CrearAtributo(ref buffer, "porcentajeImpuestoRetenido", "porcentajeImpuestoRetenido"));
            totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuestoRetencion", "ImpuestoRet"));
            totales.Attributes.Append(CrearAtributo(ref buffer, "subTotalantesDeDescuentos", "subTotalAD"));
            totales.Attributes.Append(CrearAtributo(ref buffer, "subTotaldespuesDeDescuentos", "subTotalDD"));

            //totales.Attributes.Append(CrearAtributo(ref buffer, "totalPesos", "totalPesos"));
            //totales.Attributes.Append(CrearAtributo(ref buffer, "importeImpuestoPesos", "impuestoPesos"));


            totales.Attributes.Append(CrearAtributo(ref buffer, "mstCuadrados", ""));
            totales.Attributes.Append(CrearAtributo(ref buffer, "piezas", ""));
            totales.Attributes.Append(CrearAtributo(ref buffer, "bultos", ""));
            totales.Attributes.Append(CrearAtributo(ref buffer, "peso", ""));

            return totales;
        }

        private XmlElement Comprobante()
        {
            XmlElement comprobante = xmlstd.CreateElement("Comprobante");

            comprobante.Attributes.Append(CrearAtributo(ref buffer, "cadenaOriginal", null));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "sello", null));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "anoAprobacion", null));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "noAprobacion", null));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "noCertificado", null));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "leyenda", "leyenda"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "PrinterName", "PrinterName"));
            comprobante.Attributes.Append(CrearAtributo1(ref buffer, "nota", "nota"));
            comprobante.Attributes.Append(CrearAtributo1(ref buffer, "observaciones", "observaciones"));
            try
            {
                if (buffer["serie"].ToString() != "")
                    comprobante.Attributes.Append(CrearAtributo(ref buffer, "serie", "serie"));
            }
            catch { }
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "folio", "folio"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "fecha", "fecha"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "formaDePago", "formaDePago"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "documentStatus", "documentStatus"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "referenceIdentification", "referenceIdentification"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "referenceIdentificationtype", "referenceIdentificationtype"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "AdditionalInformation", "AdditionalInformation"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "AdditionalInformationType", "AdditionalInformationType"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "alternatePartyIdentification", "alternatePartyIdentification"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "metodoDePago", "metodoDePago"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "tipoDeComprobante", "tipoDeComprobante"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "condicionesDePago", "condicionesDePago"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "importeConLetra", "importeConLetra"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "importeConLetraDecimales", ""));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "numeroOrden", "ordenCompra"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "numeroPedido", "numeroPedido"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "fechaPedido", "fechaPedido"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "numProveedor", "numProveedor"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "fechaEntrega", "fechaEntrega"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "addenda", "addenda"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "tipoAddenda", "tipoAddenda"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "tipoDoc", "tipoDoc"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "shipper", "shipper"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "numeroCliente", "numeroCliente"));

            //Version 2.2
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "regimen", "RegimenFiscal"));

            comprobante.Attributes.Append(CrearAtributo(ref buffer, "numCtaPago", "NumCtaPago"));

            comprobante.Attributes.Append(CrearAtributo(ref buffer, "folioFiscalOrig", "FolioFiscalOrig"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "serieFolioFiscalOrig", "SerieFolioFiscalOrig"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "fechaFolioFiscalOrig", "FechaFolioFiscalOrig"));
            comprobante.Attributes.Append(CrearAtributo(ref buffer, "montoFolioFiscalOrig", "MontoFolioFiscalOrig"));

            string LugarExpedicion = buffer["LugarExpedicion"];
            if (LugarExpedicion.Length > 1)
                LugarExpedicion = buffer["LugarExpedicion"];
            else
            {
                string calle = buffer["municipioExpedidoEn"];
                if (calle.Length > 0) LugarExpedicion = LugarExpedicion + calle + ", ";
                string noInterior = buffer["estadoExpedidoEn"];
                if (noInterior.Length > 0) LugarExpedicion = LugarExpedicion + noInterior + ", ";
                string planta = buffer["plantaEmisor"];
                if (planta.Length > 0) LugarExpedicion = LugarExpedicion + planta + ", ";

                LugarExpedicion = LugarExpedicion.Remove(LugarExpedicion.Length - 2, 2);
            }


            

            XmlAttribute atributoLugarExpedicion;
            atributoLugarExpedicion = xmlstd.CreateAttribute("LugarExpedicion");
            atributoLugarExpedicion.Value = LugarExpedicion;
            comprobante.Attributes.Append(atributoLugarExpedicion);

            return comprobante;
        }

        private XmlElement Emisor()
        {
            XmlElement emisor = xmlstd.CreateElement("Emisor");

            emisor.Attributes.Append(CrearAtributo(ref buffer, "numeroProveedor", "numProveedor"));
            XmlAttribute rfc = CrearAtributo(ref buffer, "rfc", "rfcEmisor");
            rfc.Value = rfc.Value.Replace("-", "");
            emisor.Attributes.Append(rfc);
            if (rfc.Value.Equals("IMS070226LF6"))
            {
                XmlAttribute nombreEmisor = xmlstd.CreateAttribute("nombre");
                nombreEmisor.Value = "IACNA Mexico Service Company S de RL de CV";
                emisor.Attributes.Append(nombreEmisor);
            }
            else
                emisor.Attributes.Append(CrearAtributo(ref buffer, "nombre", "nombreEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "plantaEmisor", "plantaEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "calle", "calleEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "noExterior", "noExtEmisor"));
            //emisor.Attributes.Append(CrearAtributo(ref buffer, "noInterior", "noInteriorEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "colonia", "coloniaEmisor"));
            //emisor.Attributes.Append(CrearAtributo(ref buffer, "localidad", "localidadEmisor"));
            //emisor.Attributes.Append(CrearAtributo(ref buffer, "referencia", "referenciaEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "municipio", "municipioEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "estado", "estadoEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "pais", "paisEmisor"));
            emisor.Attributes.Append(CrearAtributo(ref buffer, "codigoPostal", "cpEmisor"));

            return emisor;
        }
        private XmlElement Receptor()
        {
            XmlElement receptor = xmlstd.CreateElement("Receptor");

            receptor.Attributes.Append(CrearAtributo1(ref buffer, "numero", "numeroReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "rfc", "rfcReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "nombre", "nombreReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "calle", "calleReceptor"));
            //receptor.Attributes.Append(CrearAtributo(ref buffer, "noInterior", "noExtReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "noExterior", "noExtReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "colonia", "coloniaReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "municipio", "municipioReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "estado", "estadoReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "pais", "paisReceptor"));
            receptor.Attributes.Append(CrearAtributo(ref buffer, "codigoPostal", "cpReceptor"));

            return receptor;
        }
        private XmlElement EnviarA()
        {
            XmlElement EnviarA = xmlstd.CreateElement("EnviarA");

            EnviarA.Attributes.Append(CrearAtributo1(ref buffer, "numero", "numProveedor"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "rfc", "rfcEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "nombre", "nombreEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "calle", "calleEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "noExterior", "noExtEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "colonia", "coloniaEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "municipio", "municipioEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "estado", "estadoEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "pais", "paisEnviarA"));
            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "codigoPostal", "cpEnviarA"));

            EnviarA.Attributes.Append(CrearAtributo(ref buffer, "transportista", "transporte"));

            return EnviarA;
        }
        private XmlElement shipTo()
        {
            XmlElement shipTo = xmlstd.CreateElement("shipTo");

            shipTo.Attributes.Append(CrearAtributo1(ref buffer, "numero", "numProveedor"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "rfc", "rfcshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "nombre", "nombreshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "calle", "calleshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "noInterior", "noExtshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "colonia", "coloniashipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "municipio", "municipioshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "estado", "estadoshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "pais", "paisshipTo"));
            shipTo.Attributes.Append(CrearAtributo(ref buffer, "codigoPostal", "cpshipTo"));

            return shipTo;
        }
        //private XmlElement transportista()
        //{
        //    XmlElement transportista = xmlstd.CreateElement("transportista");
        //    transportista.Attributes.Append(CrearAtributo(ref buffer, "transporte", "transportista"));

        //    return transportista;
        //}
        private XmlElement Moneda()
        {
            XmlElement moneda = xmlstd.CreateElement("Moneda");

            moneda.Attributes.Append(CrearAtributo(ref buffer, "codigoISO", "divisa"));
            moneda.Attributes.Append(CrearAtributo(ref buffer, "tipoCambio", "tipoDeCambio"));
            //moneda.Attributes.Append(CrearAtributo(ref buffer, "divisa", "divisa"));

            return moneda;
        }
        private XmlElement Addenda()
        {
            XmlElement Addenda = xmlstd.CreateElement("Addenda");

            Addenda.Attributes.Append(CrearAtributo(ref buffer, "referenciaProveedor", "referenciaProveedor"));
            Addenda.Attributes.Append(CrearAtributo1(ref buffer, "montoTotal", "subTotalDD"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "proveedorCodigo", "proveedorCodigo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "proveedorNombre", "proveedorNombre"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "proveedorSufijo", "proveedorSufijo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "origenCodigo", "origenCodigo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "origenNombre", "origenNombre"));
            //Addenda.Attributes.Append(CrearAtributo(ref buffer, "origenSufijo", "origenSufijo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "destinoCodigo", "destinoCodigo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "destinoNombre", "destinoNombre"));
            //Addenda.Attributes.Append(CrearAtributo(ref buffer, "destinoSufijo", "destinoSufijo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "receivingCodigo", "receivingCodigo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "receivingNombre", "receivingNombre"));
            //Addenda.Attributes.Append(CrearAtributo(ref buffer, "receivingSufijo", "receivingSufijo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "deptNum", "deptNum"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "cuentaNum", "cuentaNum"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "nota", "nota"));


            Addenda.Attributes.Append(CrearAtributo(ref buffer, "referenciaChrysler", "refChrysler"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "consecutivo", "consecutivo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "montoLinea", "montoLinea"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "factura", "factura"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "codigoCargos", "codigoCargos"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "montoCargos", "montoCargo"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "Cancelasustituye", "Cancelasustituye"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "tipoAddenda", "tipoAddenda"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "correoContacto", "correoContacto"));

            Addenda.Attributes.Append(archivo(ref buffer, "datosArchivo", "Path_File"));
            Addenda.Attributes.Append(archivoExtension(ref buffer, "tipoArchivo", "Path_File"));

            Addenda.Attributes.Append(CrearAtributo(ref buffer, "nombreSolicitante", "nombreSolicitante"));
            Addenda.Attributes.Append(CrearAtributo(ref buffer, "correoSolicitante", "correoSolicitante"));



            return Addenda;
        }
        private XmlElement Detalle(int index)
        {
            XmlElement detalle = xmlstd.CreateElement("Detalle");

            detalle.Attributes.Append(CrearAtributo(ref buffer, "cantidad", "cantidad[" + index + "]"));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "unidad", "unidad[" + index + "]"));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "UnidaddeMedida", "UnidaddeMedida[" + index + "]")); 
            detalle.Attributes.Append(CrearAtributo(ref buffer, "noIdentificacion", "noIdentificacion[" + index + "]"));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "descripcion", "descripcion[" + index + "]"));

            detalle.Attributes.Append(CrearAtributo(ref buffer, "nombreAduana", "nombreAduana[" + index + "]"));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "numeroAduana", "numeroAduana[" + index + "]"));
            //detalle.Attributes.Append(CrearAtributo(ref buffer, "fechaAduana", "fechaAduana[" + index + "]"));

            //buffer["precioNeto["+index+"]"] = String.Format("{0:###0.00}", Convert.ToDecimal(buffer["precioNeto["+index+"]"]));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "precioNeto", "precioNeto[" + index + "]"));

            detalle.Attributes.Append(CrearAtributo(ref buffer, "precioBruto", "precioBruto[" + index + "]"));

            detalle.Attributes.Append(CrearAtributo(ref buffer, "importeNeto", "importeNeto[" + index + "]"));


            ////buffer["importeNeto[" + index + "]"] = String.Format("{0:###0.0000}", Convert.ToDecimal(buffer["importeNeto[" + index + "]"]))));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "importeBruto", "importeBruto[" + index + "]"));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "SKU", "SKU[" + index + "]"));
            detalle.Attributes.Append(CrearAtributo(ref buffer, "numero", "partNum[" + index + "]"));

            try
            {
                buffer["partFechaRecibo[" + index + "]"] = buffer["partFechaRecibo[" + index + "]"].Replace("/", "-");

            }
            catch
            {
                //buffer = "";
            }
            {
                detalle.Attributes.Append(CrearAtributo(ref buffer, "fechaRecibo", "partFechaRecibo[" + index + "]"));
                detalle.Attributes.Append(CrearAtributo(ref buffer, "ordenCompra", "ordenCompra[" + index + "]"));
                detalle.Attributes.Append(CrearAtributo(ref buffer, "Amendment", "Amendment[" + index + "]"));
                detalle.Attributes.Append(CrearAtributo(ref buffer, "releaseRequisicion", "releaseRequisicion[" + index + "]"));
                detalle.Attributes.Append(CrearAtributo1(ref buffer, "billOfLading", "billOfLading"));
                detalle.Attributes.Append(CrearAtributo1(ref buffer, "packingList", "packingList"));
                //detalle.Attributes.Append(CrearAtributo(ref buffer, "codigoCargosParte", "codigoCargosParte[" + index + "]"));  
                //detalle.Attributes.Append(CrearAtributo(ref buffer, "montoCargosParte", "montoCargosParte[" + index + "]"));
                //detalle.Attributes.Append(CrearAtributo(ref buffer, "notaParte", "notaParte[" + index + "]"));


                return detalle;
            }
        }

        private XmlAttribute CrearAtributo(ref Dictionary<string, string> buffer, string nombre, string llave)
        {
            XmlAttribute atributo;

            if (llave != null)
            {
                atributo = xmlstd.CreateAttribute(nombre);
                try
                {

                    atributo.Value = buffer[llave];
                    buffer.Remove(llave);
                }
                catch
                {
                    atributo.Value = "";
                }
            }
            else
            {
                atributo = xmlstd.CreateAttribute(nombre);
            }

            return atributo;
        }
        private XmlAttribute archivo(ref Dictionary<string, string> buffer, string nombre, string llave)
        {
            System.IO.FileStream inFile;
            byte[] binaryData;
            XmlAttribute atributo;
            atributo = xmlstd.CreateAttribute(nombre);
            if (llave != null)
            {

                try
                {
                    atributo.Value = buffer[llave];
                    //buffer.Remove(llave);
                }
                catch
                {
                    atributo.Value = "";
                }
            }
            //if (atributo.Value.Contains(".xls") || atributo.Value.Contains(".XLS"))
            //{
            if (string.IsNullOrEmpty(atributo.Value))
            {
                atributo.Value = "";
            }
            else
            {
                try
                {
                    inFile = new System.IO.FileStream(atributo.Value, FileMode.Open, FileAccess.Read);

                    binaryData = new Byte[inFile.Length];
                    long bytesRead = inFile.Read(binaryData, 0, (int)inFile.Length);
                    inFile.Close();

                    string base64String;

                    base64String = Convert.ToBase64String(binaryData, 0, binaryData.Length);
                    atributo.Value = base64String;
                }
                catch (Exception exs)
                {
                    throw exs;
                }
            }


            //}
            //else
            //{
            //    atributo.Value = "";
            //}
            return atributo;

        }
        private XmlAttribute archivoExtension(ref Dictionary<string, string> buffer, string nombre, string llave)
        {
            XmlAttribute atributo;

            if (llave != null)
            {
                atributo = xmlstd.CreateAttribute(nombre);
                try
                {
                    if (string.IsNullOrEmpty(buffer[llave]))
                        atributo.Value = "";
                    else
                        atributo.Value = Path.GetExtension(buffer[llave]).ToUpper().Replace(".", "");
                    buffer.Remove(llave);
                }
                catch
                {
                    atributo.Value = "";
                }
            }
            else
            {
                atributo = xmlstd.CreateAttribute(nombre);
            }

            return atributo;
        }
        private XmlAttribute CrearAtributo1(ref Dictionary<string, string> buffer, string nombre, string llave)
        {
            XmlAttribute atributo;

            if (llave != null)
            {
                atributo = xmlstd.CreateAttribute(nombre);
                try
                {

                    atributo.Value = buffer[llave];
                    //buffer.Remove(llave);
                }
                catch
                {
                    atributo.Value = "";
                }
            }
            else
            {
                atributo = xmlstd.CreateAttribute(nombre);
            }

            return atributo;
        }
    }
}