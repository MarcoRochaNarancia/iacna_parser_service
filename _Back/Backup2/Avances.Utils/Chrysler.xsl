<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:element name="factura">
			<xsl:attribute name="tipoDocumento">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoAddenda"/>
			</xsl:attribute>
      <xsl:attribute name="TipoDocumentoFiscal">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoDoc"/>
      </xsl:attribute>
      <xsl:attribute name="version">
        <xsl:text>1.0</xsl:text>
      </xsl:attribute>
      <xsl:if test ="/Avances/Encabezado/Comprobante/@serie">
        <xsl:attribute name="serie">
          <xsl:value-of select="/Avances/Encabezado/Comprobante/@serie"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="folioFiscal">
				<xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
			</xsl:attribute>
			<xsl:attribute name="fecha">
				<xsl:value-of select="substring(/Avances/Encabezado/Comprobante/@fecha,0,11)"/>
			</xsl:attribute>
			<xsl:attribute name="montoTotal">
				<xsl:value-of select="/Avances/Encabezado/Addenda/@montoTotal"/>
			</xsl:attribute>
			<xsl:attribute name="referenciaProveedor">
				<xsl:value-of select="/Avances/Encabezado/Addenda/@referenciaProveedor"/>
			</xsl:attribute>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@Cancelasustituye) &gt; 0">
      <xsl:element name ="Cancelaciones">
        <xsl:attribute name ="CancelaSustituye">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@Cancelasustituye"/>
        </xsl:attribute>
      </xsl:element>
      </xsl:if>
      <xsl:element name ="moneda">
        <xsl:attribute name ="tipoMoneda">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>          
        </xsl:attribute>
        <xsl:if test ="/Avances/Encabezado/Moneda/@tipoCambio">
          <xsl:attribute name ="tipoCambio">
            <xsl:value-of select="/Avances/Encabezado/Moneda/@tipoCambio"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:element>
      <xsl:element name ="proveedor">
        <xsl:attribute name ="codigo">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@proveedorCodigo"/>
        </xsl:attribute>
        <xsl:attribute name ="nombre">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@proveedorNombre"/>
        </xsl:attribute>
        <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@proveedorSufijo) &gt; 0">
          <xsl:attribute name ="sufijo">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@proveedorSufijo"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:element>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@origenCodigo) &gt; 0">
        <xsl:element name ="origen">
          <xsl:attribute name ="codigo">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@origenCodigo"/>
          </xsl:attribute>
          <xsl:attribute name ="nombre">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@origenNombre"/>
          </xsl:attribute>
          <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@origenSufijo) &gt; 0">
            <xsl:attribute name ="sufijo">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@origenSufijo"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:if>
      <xsl:element name ="destino">
        <xsl:attribute name ="codigo">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoCodigo"/>
        </xsl:attribute>
        <xsl:attribute name ="nombre">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoNombre"/>
        </xsl:attribute>
        <xsl:if test ="/Avances/Encabezado/Addenda/@destinoSufijo">
          <xsl:attribute name ="sufijo">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoSufijo"/>
          </xsl:attribute>
        </xsl:if>
      </xsl:element>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@receivingCodigo) &gt; 0">
        <xsl:element name ="receiving">
          <xsl:attribute name ="codigo">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@receivingCodigo"/>
          </xsl:attribute>
          <xsl:attribute name ="nombre">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@receivingNombre"/>
          </xsl:attribute>
          <xsl:if test ="/Avances/Encabezado/Addenda/@receivingSufijo">
            <xsl:attribute name ="sufijo">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@receivingSufijo"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:if>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@deptNum) &gt; 0">
        <xsl:element name ="departamento">
          <xsl:attribute name ="numero">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@deptNum"/>
          </xsl:attribute>
          <xsl:attribute name ="cuenta">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@cuentaNum"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:if>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@nota)&gt; 0">
        <xsl:element name ="nota">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@nota"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@referenciaChrysler) &gt; 1">
        <xsl:element name ="cargosCreditos">          
            <xsl:attribute name ="referenciaChrysler">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@referenciaChrysler"/>
            </xsl:attribute>
            <xsl:attribute name ="consecutivo">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@consecutivo"/>
            </xsl:attribute>
            <xsl:attribute name ="montoLinea">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@montoLinea"/>
            </xsl:attribute>
            <xsl:if test ="/Avances/Encabezado/Addenda/@factura">
              <xsl:attribute name ="factura">
                <xsl:value-of select="/Avances/Encabezado/Addenda/@factura"/>
              </xsl:attribute>
            </xsl:if>
          <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@archivo) &gt; 0">
            <xsl:attribute name="archivo">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@archivo"/>
            </xsl:attribute>
          </xsl:if>
        
        </xsl:element>
      </xsl:if>
      <xsl:element name ="otrosCargos">
        <xsl:attribute name ="codigo">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@codigoCargos"/>
        </xsl:attribute>
        <xsl:attribute name ="monto">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@montoCargos"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:element name ="partes">
			 <xsl:for-each select="/Avances/Detalles/Detalle">
				<xsl:element name ="part">
					<xsl:attribute name="numero">
						<xsl:value-of select="@numero"/>
					</xsl:attribute>
					<xsl:attribute name="cantidad">
						<xsl:value-of select="@cantidad"/>
					</xsl:attribute>
					<xsl:attribute name="unidadDeMedida">
						<xsl:value-of select="@UnidaddeMedida"/>
					</xsl:attribute>
					<xsl:attribute name="precioUnitario">
						<xsl:value-of select="@precioNeto"/>
					</xsl:attribute>
          <xsl:if test ="@importeNeto">
            <xsl:attribute name="montoDeLinea">
              <xsl:value-of select="@importeNeto"/>
            </xsl:attribute>
          </xsl:if>
          
          <xsl:if test ="@fechaRecibo">
            <xsl:attribute name="fechaRecibo">
              <xsl:value-of select="@fechaRecibo"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:element name ="references">
            <xsl:if test="@ordenCompra">
              <xsl:attribute name="ordenCompra">
                <xsl:value-of select="@ordenCompra"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="string-length(@releaseRequisicion) &gt; 0">
             <xsl:attribute name="releaseRequisicion">
               <xsl:value-of select="@releaseRequisicion"/>
             </xsl:attribute>
            </xsl:if>
            <xsl:if test="string-length(@billOfLading) &gt; 0">
              <xsl:attribute name="billOfLading">
                <xsl:value-of select="@billOfLading"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="string-length(@packingList) &gt; 0">
              <xsl:attribute name="packingList">
                <xsl:value-of select="@packingList"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test="@carVessel">
              <xsl:attribute name="carVessel">
                <xsl:value-of select="@carVessel"/>
              </xsl:attribute>
            </xsl:if>
            
            <xsl:if test="string-length(@Amendment) &gt; 0">
               <xsl:attribute name="ammendment">
                 <xsl:value-of select="@Amendment"/>
            </xsl:attribute>
        </xsl:if>
        </xsl:element>          
          <xsl:if test ="codigoCargosParte">
            <xsl:element name ="otrosCargos">
            <xsl:attribute name ="codigo">
              <xsl:value-of select="@codigoCargosParte"/>
            </xsl:attribute>
            <xsl:attribute name ="monto">
              <xsl:value-of select="@montoCargosParte"/>
            </xsl:attribute>
          </xsl:element>
          </xsl:if>
          <xsl:if test ="@notaParte">
            <xsl:element name ="nota">
              <xsl:value-of select="@notaParte"/>
            </xsl:element>
          </xsl:if>        
				</xsl:element>
			 </xsl:for-each>
      </xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>