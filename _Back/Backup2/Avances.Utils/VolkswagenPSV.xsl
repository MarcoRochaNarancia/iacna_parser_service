<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:PSV="http://www.vwnovedades.com/volkswagen/kanseilab/shcp/2009/Addenda/PSV">
  <xsl:template match="/">
    <xsl:element name="PSV:Factura">
      <xsl:attribute name="version">
        <xsl:text>1.0</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="tipoDocumentoFiscal">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoDoc"/>
      </xsl:attribute>
      <xsl:attribute name="tipoDocumentoVWM">
        <xsl:value-of select="/Avances/Encabezado/Comprobante/@tipoAddenda"/>
      </xsl:attribute>
      <xsl:attribute name="division">
        <xsl:text>VW</xsl:text>
      </xsl:attribute>
      <!--<xsl:if test ="string-length(/Avances/Encabezado/Addenda/@Cancelasustituye) &gt; 0">
        <xsl:element name ="PSV:Cancelaciones">
          <xsl:attribute name ="CancelaSustituye">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@Cancelasustituye"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:if>-->
      <xsl:element name ="PSV:Moneda">
        <xsl:attribute name ="tipoMoneda">
          <xsl:value-of select="/Avances/Encabezado/Moneda/@codigoISO"/>
        </xsl:attribute>
        <xsl:if test ="/Avances/Encabezado/Moneda/@tipoCambio">
          <xsl:attribute name ="tipoCambio">
            <xsl:value-of select="/Avances/Encabezado/Moneda/@tipoCambio"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name ="codigoImpuesto">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@codigoCargos"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:element name ="PSV:Proveedor">
        <xsl:attribute name ="codigo">
          <xsl:text>6001013936</xsl:text>
        </xsl:attribute>
        <xsl:attribute name ="nombre">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@proveedorNombre"/>
        </xsl:attribute>
        <xsl:attribute name ="correoContacto">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@correoContacto"/>
        </xsl:attribute>
        
      </xsl:element>
      <!--<xsl:if test ="string-length(/Avances/Encabezado/Addenda/@origenCodigo) &gt; 0">
        <xsl:element name ="PSV:Origen">
          <xsl:attribute name ="codigo">
            <xsl:text>6001013936</xsl:text>
          </xsl:attribute>
          <xsl:attribute name ="nombre">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@proveedorNombre"/>
          </xsl:attribute>

        </xsl:element>
      </xsl:if>-->
      <xsl:element name ="PSV:Destino">
        <xsl:attribute name ="codigo">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoCodigo"/>
        </xsl:attribute>
        <!--<xsl:attribute name ="naveReciboMaterial">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@destinoNombre"/>
        </xsl:attribute>-->
      </xsl:element>

      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@pesoBruto) &gt; 0">
        <xsl:element name ="PSV:Medidas">
          <xsl:attribute name ="pesoBruto">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@pesoBruto"/>
          </xsl:attribute>
          <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@pesoNeto) &gt; 0">
            <xsl:attribute name ="pesoNeto">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@pesoNeto"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@volumen) &gt; 0">
            <xsl:attribute name ="volumen">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@volumen"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@numeroPiezas) &gt; 0">
            <xsl:attribute name ="numeroPiezas">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@numeroPiezas"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@descripcion) &gt; 0">
            <xsl:attribute name ="descripcion">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@descripcion"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:if>
      <xsl:element name ="PSV:Referencias">
        <xsl:if test ="string-length(/Avances/Encabezado/Comprobante/@shipper) &gt; 0">
          <xsl:attribute name ="referenciaProveedor">
            <xsl:value-of select="/Avances/Encabezado/Comprobante/@shipper"/>
          </xsl:attribute>
        </xsl:if>
        <xsl:attribute name ="remision">
          <xsl:choose>
            <xsl:when test="string-length(/Avances/Encabezado/Addenda/@Cancelasustituye) &gt; 0">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@Cancelasustituye"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="/Avances/Encabezado/Comprobante/@folio"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </xsl:element>

      <xsl:if test="(string-length(/Avances/Encabezado/Addenda/@nombreSolicitante) &gt; 0) or (string-length(/Avances/Encabezado/Addenda/@correoSolicitante) &gt; 0)">
        <xsl:element name="PSV:Solicitante">
          <xsl:if test="string-length(/Avances/Encabezado/Addenda/@nombreSolicitante) &gt; 0">
            <xsl:attribute name="nombre">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@nombreSolicitante"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="string-length(/Avances/Encabezado/Addenda/@correoSolicitante) &gt; 0">
            <xsl:attribute name="correo">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@correoSolicitante"/>
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:if>
      
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@nota) &gt; 0">
        <xsl:element name ="PSV:Nota">
          <xsl:value-of select="/Avances/Encabezado/Addenda/@nota"/>
        </xsl:element>
      </xsl:if>
      <xsl:if test ="string-length(/Avances/Encabezado/Addenda/@datosArchivo) &gt; 0">
        <xsl:element name ="PSV:Archivo">
          <xsl:attribute name ="Datos">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@datosArchivo"/>
          </xsl:attribute>
          <xsl:attribute name ="Tipo">
            <xsl:value-of select="/Avances/Encabezado/Addenda/@tipoArchivo"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:if>

      <xsl:element name ="PSV:Partes">
        <xsl:for-each select="/Avances/Detalles/Detalle">
          <xsl:element name ="PSV:Parte">
            <xsl:attribute name="posicion">
              <xsl:value-of select="position()"/>
            </xsl:attribute>
            <xsl:attribute name="numeroMaterial">
              <xsl:value-of select="@numero"/>
            </xsl:attribute>
            <xsl:attribute name="descripcionMaterial">
              <xsl:value-of select="@descripcion"/>
            </xsl:attribute>
            <xsl:attribute name="cantidadMaterial">
              <xsl:value-of select="@cantidad"/>
            </xsl:attribute>
            <xsl:attribute name="unidadMedida">
              <xsl:value-of select="@UnidaddeMedida"/>
            </xsl:attribute>
            <xsl:attribute name="precioUnitario">
              <xsl:value-of select="@precioNeto"/>
            </xsl:attribute>            
            <xsl:attribute name="montoLinea">
              <xsl:value-of select="@importeNeto"/>
            </xsl:attribute>
            <xsl:if test ="@pesoBruto">
              <xsl:attribute name="pesoBruto">
                <xsl:value-of select="@pesoBruto"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:if test ="@pesoNeto">
              <xsl:attribute name="pesoNeto">
                <xsl:value-of select="@pesoNeto"/>
              </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="codigoImpuesto">
              <xsl:value-of select="/Avances/Encabezado/Addenda/@codigoCargos"/>
            </xsl:attribute>
            <xsl:element name ="PSV:Referencias">
              <xsl:if test="@ordenCompra">
                <xsl:attribute name="ordenCompra">
                  <xsl:value-of select="@ordenCompra"/>
                </xsl:attribute>
              </xsl:if>
            </xsl:element>
            <xsl:if test ="@notaParte">
              <xsl:element name ="PSV:Nota">
                <xsl:value-of select="@notaParte"/>
              </xsl:element>
            </xsl:if>
          </xsl:element>
        </xsl:for-each>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>