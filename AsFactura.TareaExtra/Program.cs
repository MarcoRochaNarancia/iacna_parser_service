﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace AsFactura.TareaExtra
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string _GUIDArchivo = args[0];
                string _RutaCFDI = args[1];
                string _RutaPDF = args[2];
                string _RutaCopiaCFDI = args[3];
                string _RutaCodigoBarras = args[4];

                if (!string.IsNullOrEmpty(_RutaCopiaCFDI))
                {
                    string sArchivoRenombrado = Path.Combine(Path.GetFullPath(_RutaCFDI).Replace(Path.GetFileName(_RutaCFDI), ""), Path.GetFileName(_RutaCopiaCFDI));

                    if (File.Exists(sArchivoRenombrado))
                        File.Delete(sArchivoRenombrado);

                    File.Copy(_RutaCFDI, sArchivoRenombrado);

                    if (File.Exists(_RutaCopiaCFDI))
                        File.Delete(_RutaCopiaCFDI);

                    File.Copy(sArchivoRenombrado, _RutaCopiaCFDI);

                    bool EnUso = true;

                    while (EnUso)
                    {
                        FileStream stream = null;
                        FileInfo file = new FileInfo(sArchivoRenombrado);
                        try
                        {
                            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                            EnUso = false;
                        }
                        catch (IOException)
                        {
                            EnUso = true;
                        }
                        finally
                        {
                            if (stream != null)
                                stream.Close();
                        }
                    }
                    
                    File.Delete(sArchivoRenombrado);
                    try
                    {
                        File.Delete(_RutaCodigoBarras);
                    }
                    catch { }
                }
            }
            catch (Exception e)
            {
                EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                ApplicationEventLog.Source = "AsFacturaV3";
                ApplicationEventLog.WriteEntry(e.Message.ToString(), EventLogEntryType.Error);
            }
        }
    }
}
