﻿using ASFactura.TareasFinales;
using Avances;
using Avances.Xml;
using Narancia.Pdf4;
using Narancia.Pdf4.Entidades;
using StructureMap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ParserService
{
    public class ParserService : ServiceBase
    {
        #region Component Designer generated code
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "ParserService";
        }
        #endregion

        #region Propiedades

        private FileInfo fiStd;
        private FileInfo fiXsl;

        private bool _enEjecucion;
        private string sTmp;
        private readonly short _intervalo;

        private readonly string _rutaTemp;
        private readonly string _rutaAsFactura;
        private readonly string _tareasFinales;
        private readonly string _fileWatcherPath;
        private readonly string _rutaCopiaCGI;
        private readonly string _rutaCopiaCFDI;
        private readonly string[] _lstRfcCopiaCFDI;
        private string _plantillaPath;
        private readonly string _rutaCodigoBarras;

        private readonly Thread _Origen;

        private string sRutaCGI = string.Empty;

        Guid facturaGuid;

        #endregion

        public ParserService()
        {
            InitializeComponent();

            _fileWatcherPath = ConfigurationManager.AppSettings["RutaCGI"].ToString();
            _plantillaPath = string.Empty;
            _rutaTemp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp");
            _rutaAsFactura = ConfigurationManager.AppSettings["RutaAsFactura"].ToString();
            _tareasFinales = ConfigurationManager.AppSettings["TareasFinales"].ToString();
            _intervalo = Convert.ToInt16(ConfigurationManager.AppSettings["Intervalo"].ToString());
            _plantillaPath = ConfigurationManager.AppSettings["RutaPlantillas"].ToString();
            _rutaCopiaCGI = ConfigurationManager.AppSettings["RutaCopiaCGI"].ToString();
            _rutaCopiaCFDI = ConfigurationManager.AppSettings["RutaCopiaCFDI"].ToString();
            _lstRfcCopiaCFDI = ConfigurationManager.AppSettings["RfcCopiaCFDI"].Split(new char[] { ';', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            _rutaCodigoBarras = ConfigurationManager.AppSettings["RutaCodigoBarras"].ToString();

            if (!Directory.Exists(_rutaTemp))
            {
                Directory.CreateDirectory(_rutaTemp);
            }

            try
            {
                _Origen = new Thread(() =>
                                TaskMonitorear(_fileWatcherPath)
                    ) { Name = "Carga CGI" };

            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Error Servicio Parser: " + e.Message, EventLogEntryType.Error);
                Dispose();
            }
        }

        private void TaskMonitorear(string _ruta)
        {
            bool terminate = false;
            int numMaxIntentos = 5;
            int numIntentos = 0;

            while (terminate == false)
            {
                try
                {
                    if (!Directory.Exists(_ruta)) Directory.CreateDirectory(_ruta);

                    do
                    {
                        int counter = 0;
                        string archivoTemp = string.Empty;

                        foreach (var archivo in Directory.EnumerateFiles(_ruta, "*.txt"))
                        {
                            var fileName = Path.GetFileName(archivo);

                            if (fileName == null)
                            {
                                continue;
                            }

                            if (!Directory.Exists(_rutaTemp))
                                Directory.CreateDirectory(_rutaTemp);

                            archivoTemp = Path.Combine(_rutaTemp, fileName);

                            if (File.Exists(archivoTemp))
                            {
                                File.Delete(archivoTemp);
                            }

                            bool mueve = true;

                            while (mueve)
                            {
                                try
                                {
                                    if (numIntentos < numMaxIntentos)
                                    {
                                        File.Move(archivo, archivoTemp);
                                        counter++;
                                        mueve = false;
                                        break;
                                    }
                                    else
                                    {
                                        numIntentos = 0;
                                        break;
                                    }
                                }
                                catch
                                {
                                    numIntentos++;
                                }
                            }

                            if (!mueve)
                            {
                                break;
                            }
                        }

                        foreach (var archivo in Directory.EnumerateFiles(_rutaTemp, "*.txt"))
                        {
                            sRutaCGI = Path.Combine(_rutaCopiaCGI, Path.GetFileName(archivo));
                            if (File.Exists(sRutaCGI))
                                File.Delete(sRutaCGI);

                            File.Copy(archivo, sRutaCGI);

                            bool EnUso = true;

                            while (EnUso)
                            {
                                FileStream stream = null;
                                FileInfo file = new FileInfo(archivo);
                                try
                                {
                                    stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                                    EnUso = false;
                                }
                                catch (IOException)
                                {
                                    EnUso = true;
                                }
                                finally
                                {
                                    if (stream != null)
                                        stream.Close();
                                }
                            }

                            CGIParse(archivo, _rutaAsFactura, _tareasFinales);

                            if(File.Exists(archivo))
                                File.Delete(archivo);
                        }
                        
                        Thread.Sleep(TimeSpan.FromSeconds(_intervalo));
                    } while (_enEjecucion);
                }
                catch (Exception e){
                    EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                    ApplicationEventLog.Source = "AsFactura";
                    ApplicationEventLog.WriteEntry(e.ToString(), EventLogEntryType.Error);
                }
            }
        }

        private string GenerarBarCode(string Valor)
        {
            string sRutaImagen = string.Empty;

            try
            {
                var lCodigoBarras = new BarcodeLib.Barcode
                {
                    Alignment = BarcodeLib.AlignmentPositions.CENTER
                   ,IncludeLabel = true
                   ,RotateFlipType = System.Drawing.RotateFlipType.RotateNoneFlipNone
                   ,LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                   ,Width = 300
                   ,Height = 100
                   ,ForeColor = Color.Black
                   ,BackColor = Color.White
                };

                Image imgResult = lCodigoBarras.Encode(BarcodeLib.TYPE.CODE128A, Valor.Trim());

                if (!Directory.Exists(_rutaCodigoBarras))
                    Directory.CreateDirectory(_rutaCodigoBarras);

                sRutaImagen = Path.Combine(_rutaCodigoBarras, facturaGuid.ToString()) + ".png";
                if (File.Exists(sRutaImagen))
                    File.Delete(sRutaImagen);

                imgResult.Save(sRutaImagen, System.Drawing.Imaging.ImageFormat.Png);
            }
            catch { }

            return sRutaImagen;
        }

        private void CGIParse(string spoolPath, string asfactpath, string tareasFinalesPath)
        {
            string fiResult = string.Empty;
            System.IO.StreamReader lBuffer = null;

            try
            {
                lBuffer = new System.IO.StreamReader(spoolPath, Encoding.Default);
                
                var sAddenda = string.Empty;

                Dictionary<string, string>[] buffer = Avances.Parsers.CGIParser.Parse(lBuffer);

                for (int i = 0; i < buffer.Length; i++)
                {
                    buffer[i]["serie"] = buffer[i]["serie"].ToUpper();

                    if (buffer[i]["ImpuestoRet"] == "0.00")
                    {
                        buffer[i]["ImpuestoRet"] = "";
                    }

                    fiStd = new FileInfo(Path.GetTempFileName());
                    fiXsl = new FileInfo(Path.GetTempFileName());

                    #region Generar XML

                    KPToXML xml = new KPToXML(buffer[i]);
                    xml.XmlStd.Save(fiStd.FullName);

                    XmlDoc std = new XmlDoc(fiStd.FullName);

                    fiXsl = Utils.getTempResourceFile("Avances.Utils.DocumentoAvances_CFDI.xslt");

                    byte[] bTmp = std.XmlTransform(fiXsl.FullName);
                    sTmp = Encoding.UTF8.GetString(bTmp, 1, bTmp.Length - 1);
                    sTmp = sTmp.Replace("<Comprobante", "<Comprobante xmlns=\"http://www.sat.gob.mx/cfd/2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd\"");

                    if (sTmp[0] != '<')
                    {
                        sTmp = sTmp.Remove(0, sTmp.IndexOf("<"));
                    }

                    XmlDoc xmlCFDI = new XmlDoc(sTmp);
                    switch (std.XmlReadAttribute("addenda", "Comprobante"))
                    {
                        case "4":
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.HomeDepot.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);
                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                        case "10":
                        {
                            switch (std.XmlReadAttribute("tipoAddenda", "Comprobante"))
                            {
                                case "PAB":
                                    fiXsl = Utils.getTempResourceFile("Avances.Utils.ChryslerPAB.xsl");
                                    break;
                                default:
                                    fiXsl = Utils.getTempResourceFile("Avances.Utils.Chrysler.xsl");
                                    break;
                            }
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);
                            sTmp = sTmp.Replace("<PAB:factura", "<PAB:factura xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
                            sTmp = sTmp.Replace("xmlns:PAB", "xsi:schemaLocation=\"http://www.dfdchryslerdemexico.com.mx/Addenda/PAB http://www.dfdchryslerdemexico.com.mx/Addenda/PAB/PAB.XSD\" xmlns:PAB");
                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                        }
                        case "13":
                            switch (std.XmlReadAttribute("tipoAddenda", "Comprobante"))
                            {
                                case "PSV":
                                    fiXsl = Utils.getTempResourceFile("Avances.Utils.VolkswagenPSV.xsl");
                                    break;
                                default:
                                    fiXsl = Utils.getTempResourceFile("Avances.Utils.Volkswagen.xsl");
                                    break;
                            }
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);
                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                        case "15":
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.Nissan.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);
                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                        case "16":
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.Honda.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                        case "17":
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.GM.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                        case "18":
                            fiXsl = Utils.getTempResourceFile("Avances.Utils.Ford.xsl");
                            bTmp = std.XmlTransform(fiXsl.FullName);
                            sTmp = Encoding.UTF8.GetString(bTmp, 41, bTmp.Length - 41);

                            sAddenda = sTmp;

                            xmlCFDI.xdoc.InnerXml = xmlCFDI.xdoc.InnerXml.Replace("<Addenda>", "").Replace("<Addenda />", "");
                            break;
                    }

                    facturaGuid = Guid.NewGuid();
                    fiResult = facturaGuid.ToString() + ".xml";
                    xmlCFDI.Save(Path.Combine(new[] { asfactpath, fiResult }));
                    #endregion

                    #region Generar Pdf
                        #region Configuracion Miltipagina

                        string sRutaCodigoBarras = GenerarBarCode(buffer[i]["serie"].Trim() + buffer[i]["folio"].Trim());
                        var lsImagenes = new List<Imagen>();

                        lsImagenes.Add(
                                        new Imagen
                                        { 
                                            Ruta = sRutaCodigoBarras, Alto = 35, Ancho = 105, Coordenadas = new Coordenadas { X = 367, Y = 10 } 
                                        });

                        var configuracion = new Configuracion { AltoLinea = 8, LineasDespuesDeDetalle = 1 };

                        var formatoUnicaHoja = new FormatoFactura
                        {
                            AreaDetalle = new AreaDetalle { CoordenadaYInicio = 290, CoordenadaYFin = 550 },
                            RutaPlantilla = this._plantillaPath + "Pagina1.pdf",
                            Imagenes = lsImagenes
                        };
                        configuracion.UnicaHoja = formatoUnicaHoja;

                        var formatoPrimeraHoja = new FormatoFactura
                        {
                            AreaDetalle = new AreaDetalle { CoordenadaYInicio = 290, CoordenadaYFin = 763 },
                            RutaPlantilla = this._plantillaPath + "Pagina2.pdf",
                            Imagenes = lsImagenes
                        };
                        configuracion.PrimeraHoja = formatoPrimeraHoja;

                        var formatoSegundaHoja = new FormatoFactura
                        {
                            AreaDetalle = new AreaDetalle { CoordenadaYInicio = 124, CoordenadaYFin = 771 },
                            RutaPlantilla = this._plantillaPath + "Pagina3.pdf",
                            Imagenes = lsImagenes
                        };
                        configuracion.SegundaHoja = formatoSegundaHoja;

                        var formatoTerceraHoja = new FormatoFactura
                        {
                            AreaDetalle = new AreaDetalle { CoordenadaYInicio = 120, CoordenadaYFin = 522 },
                            RutaPlantilla = this._plantillaPath + "Pagina4.pdf",
                            Imagenes = lsImagenes
                        };
                        configuracion.TerceraHoja = formatoTerceraHoja;

                        #endregion

                        #region Llenado de campos

                        var acrofield = new Acrofield();
                        acrofield.Nombre = "varFolio";
                        acrofield.Texto = buffer[i]["serie"] + " " + buffer[i]["folio"];
                        configuracion.Acrofields.Add(acrofield);

                        string tipoComprobante = string.Empty;
                        switch(buffer[i]["tipoDeComprobante"])
                        {
                            case "INVOICE":
                                tipoComprobante = "FACTURA";
                                break;
                            case "CREDIT_NOTE":
                                tipoComprobante = "NOTA DE CREDITO";
                                break;
                            case "DEBIT_NOTE":
                                tipoComprobante = "NOTA DE DEBITO";
                                break;
                            case "TRANSPORT_DOCUMENT":
                                tipoComprobante = "TRASLADO";
                                break;
                        }
                    
                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTipoDoc";
                        acrofield.Texto = tipoComprobante;
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varRazonSocial";
                        acrofield.Texto = buffer[i]["nombreEmisor"];
                        configuracion.Acrofields.Add(acrofield);

                        string sDireccion = buffer[i]["calleEmisor"] + " No. " + buffer[i]["noExtEmisor"] + " " + buffer[i]["coloniaEmisor"]
                                            + ", " + buffer[i]["municipioEmisor"] + ", " + buffer[i]["estadoEmisor"] + " " + buffer[i]["paisEmisor"]
                                            + Environment.NewLine + buffer[i]["rfcEmisor"];

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varDireccion";
                        acrofield.Texto = sDireccion;
                        configuracion.Acrofields.Add(acrofield);

                        string sFacturaA = buffer[i]["nombreReceptor"] + Environment.NewLine + buffer[i]["calleReceptor"] + Environment.NewLine +
                                           buffer[i]["noExtReceptor"] + " " + buffer[i]["coloniaReceptor"] + " ," +
                                           buffer[i]["municipioReceptor"] + ", " + buffer[i]["estadoReceptor"] + " " +
                                          buffer[i]["paisReceptor"] + " CP. " + buffer[i]["cpReceptor"] + Environment.NewLine + buffer[i]["rfcReceptor"];

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varFacturar";
                        acrofield.Texto = sFacturaA;
                        configuracion.Acrofields.Add(acrofield);

                        var varRemitir = string.Empty;
                        if (!string.IsNullOrEmpty(buffer[i]["nombreExpedidoEn"]))
                        {
                            varRemitir =
                            buffer[i]["nombreExpedidoEn"] + Environment.NewLine + buffer[i]["calleExpedidoEn"] + ", " + buffer[i]["noExtExpedidoEn"] + " " +
                            buffer[i]["coloniaExpedidoEn"] + Environment.NewLine + buffer[i]["municipioExpedidoEn"] + ", " + buffer[i]["estadoExpedidoEn"] + " " +
                            buffer[i]["paisExpedidoEn"] + " CP. " + buffer[i]["cpExpedidoEn"] + Environment.NewLine +
                            buffer[i]["rfcEmisor"];
                        }
                        else if (!string.IsNullOrEmpty(buffer[i]["nombreEmisor"]))
                        {
                            varRemitir =
                            buffer[i]["nombreEmisor"] + Environment.NewLine + buffer[i]["calleEmisor"] + ", " + buffer[i]["noExtEmisor"] + " " +
                            buffer[i]["coloniaEmisor"] + Environment.NewLine + buffer[i]["municipioEmisor"] + ", " + buffer[i]["estadoEmisor"] + " " +
                            buffer[i]["paisEmisor"] + " CP. " + buffer[i]["cpEmisor"] + Environment.NewLine +
                            buffer[i]["rfcEmisor"];
                        }

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varRemitir";
                        acrofield.Texto = varRemitir;
                        configuracion.Acrofields.Add(acrofield);

                        string sEmbarcarA = buffer[i]["nombreEnviarA"] + Environment.NewLine + buffer[i]["calleEnviarA"] + ", " + buffer[i]["noExtEnviarA"] +
                                            " " + buffer[i]["coloniaEnviarA"] + ", " + buffer[i]["municipioEnviarA"] + ", " + buffer[i]["estadoEnviarA"] + " " +
                                          buffer[i]["paisEnviarA"] + " CP. " + buffer[i]["cpEnviarA"] + Environment.NewLine + buffer[i]["rfcEnviarA"];

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varEmbarcar";
                        acrofield.Texto = sEmbarcarA;
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTerminos";
                        acrofield.Texto = buffer[i]["condicionesDePago"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varCliente";
                        acrofield.Texto = buffer[i]["numeroCliente"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTrans";
                        acrofield.Texto = buffer[i]["transporte"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varSupCode";
                        acrofield.Texto = buffer[i]["numProveedor"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varShipper";
                        acrofield.Texto = buffer[i]["shipper"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varFecha";
                        acrofield.Texto = buffer[i]["fecha"].Replace("T", " ");
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varLugarFyH";
                        acrofield.Texto = buffer[i]["LugarExpedicion"] + " " + buffer[i]["fecha"].Replace("T"," ");
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varImpLetra";

                        if (buffer[i]["importeConLetra"].LastIndexOf(")") > -1)
                        {
                            acrofield.Texto = buffer[i]["importeConLetra"].ToUpper();
                        }
                        else
                        {
                            acrofield.Texto = "(" + buffer[i]["importeConLetra"].ToUpper() + ")";
                        }

                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varRegimen";
                        acrofield.Texto = buffer[i]["RegimenFiscal"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTipoPago";
                        acrofield.Texto = buffer[i]["formaDePago"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varMetPago";
                        acrofield.Texto = buffer[i]["metodoDePago"];
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varNoCtaPago";
                        acrofield.Texto = buffer[i]["NumCtaPago"];
                        configuracion.Acrofields.Add(acrofield);

                        string sValorCampo = string.Empty;

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varMoneda";

                        if (buffer[i].TryGetValue("divisa", out sValorCampo))
                        {
                            acrofield.Texto = sValorCampo;
                        }
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTipo";

                        if (buffer[i].TryGetValue("tipoCambio", out sValorCampo))
                        {
                            acrofield.Texto = sValorCampo;
                        }
                        else if (buffer[i].TryGetValue("tipoDeCambio", out sValorCampo))
                        {
                            acrofield.Texto = sValorCampo;
                        }
                        configuracion.Acrofields.Add(acrofield);

                        string sTitTot = "Sub-Total:" + Environment.NewLine;
                        string sValTot = "$" + String.Format("{0:#,##0.00####}", Convert.ToDecimal(buffer[i]["subTotalAD"])) + Environment.NewLine;

                        sTitTot += "(" + buffer[i]["porcentajeImpuesto"].ToString() + "%) " + "IVA:" + Environment.NewLine;
                        sValTot += "$" + String.Format("{0:#,##0.00####}", Convert.ToDecimal(buffer[i]["importeImpuesto"])) + Environment.NewLine;

                        string sValor = string.Empty;
                        if (buffer[i].TryGetValue("ImpuestoRet", out sValor) && sValor != "0.00" && sValor != "")
                        {
                            sTitTot += "Sub-Total:" + Environment.NewLine;
                            sValTot += "$" + String.Format("{0:#,##0.00####}", Convert.ToDecimal(buffer[i]["subTotalDD"])) + Environment.NewLine;

                            sTitTot += "Imp. Retenido:";
                            sValTot += "$" + String.Format("{0:#,##0.00##}", Convert.ToDecimal(sValor));
                        }

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTitTot";
                        acrofield.Texto = sTitTot;
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varValTot";
                        acrofield.Texto = sValTot;
                        configuracion.Acrofields.Add(acrofield);

                        acrofield = new Acrofield();
                        acrofield.Nombre = "varTotal";
                        acrofield.Texto = "$" + String.Format("{0:#,##0.00##}", Convert.ToDecimal(buffer[i]["Total"]));
                        configuracion.Acrofields.Add(acrofield);

                        //Numerador de pagina
                        configuracion.Numerador.Fuente.Nombre = "Courier";
                        configuracion.Numerador.Fuente.Alineacion = Narancia.Pdf4.Enumeraciones.Alineacion.Centro;
                        configuracion.Numerador.Separador = "/";
                        configuracion.Numerador.Coordenadas.X = 458;
                        configuracion.Numerador.Coordenadas.Y = 80;
                        configuracion.Numerador.EsNumeracionLarga = true;
                        configuracion.Numerador.Fuente.Tamano = 8;

                        #endregion

                        #region Llenado de detalle

                        var xDoc = XDocument.Parse(xml.XmlStd.InnerXml);
                        var tmpdetalles = (from a in xDoc.Root.Descendants("Detalles").Descendants() select a).ToList();

                        //for (int x = 0; x < 40; x++)
                        //{
                        int contDetalles = 1;
                        foreach (var detalleADibujar in tmpdetalles)
                        {
                            var detalle = new Detalle();
                            var columna = new Columna { EspacioX = 10 };
                            var fuenteCentro = new Fuente { Alineacion = Narancia.Pdf4.Enumeraciones.Alineacion.Centro, Tamano = 7 };
                            var fuenteDerecha = new Fuente { Alineacion = Narancia.Pdf4.Enumeraciones.Alineacion.Derecha, Tamano = 7 };
                            var fuenteIzquierda = new Fuente { Alineacion = Narancia.Pdf4.Enumeraciones.Alineacion.Izquierda, Tamano = 7 };

                            var renglon = new RenglonColumna()
                            {
                                Texto = detalleADibujar.Attribute("ordenCompra").Value,
                                EspacioX = 2,
                                MaximoNumeroDeCaracteres = 12,
                                Fuente = fuenteIzquierda
                            };

                            columna.RenglonesColumna.Add(renglon);
                            detalle.Columnas.Add(columna);

                            columna = new Columna { EspacioX = 10 };
                            renglon = new RenglonColumna()
                            {
                                Texto = Math.Round(Convert.ToDecimal(detalleADibujar.Attribute("cantidad").Value)).ToString(),
                                EspacioX = 76,
                                MaximoNumeroDeCaracteres = 10,
                                Fuente = fuenteCentro
                            };

                            columna.RenglonesColumna.Add(renglon);
                            detalle.Columnas.Add(columna);

                            columna = new Columna { EspacioX = 10 };
                            renglon = new RenglonColumna()
                            {
                                Texto = detalleADibujar.Attribute("unidad").Value,
                                EspacioX = 111,
                                MaximoNumeroDeCaracteres = 4,
                                Fuente = fuenteCentro
                            };

                            columna.RenglonesColumna.Add(renglon);
                            detalle.Columnas.Add(columna);

                            columna = new Columna { EspacioX = 10 };
                            renglon = new RenglonColumna()
                            {
                                Texto = detalleADibujar.Attribute("noIdentificacion").Value,
                                EspacioX = 125,
                                MaximoNumeroDeCaracteres = 22,
                                Fuente = fuenteIzquierda
                            };

                            columna.RenglonesColumna.Add(renglon);
                            detalle.Columnas.Add(columna);

                            columna = new Columna { EspacioX = 10 };
                            renglon = new RenglonColumna()
                            {
                                Texto = buffer[i]["descripcion[" + contDetalles.ToString() + "]"],
                                EspacioX = 220,
                                MaximoNumeroDeCaracteres = 48,
                                Fuente = fuenteIzquierda
                            };
                            contDetalles++;

                            columna.RenglonesColumna.Add(renglon);
                            detalle.Columnas.Add(columna);

                            columna = new Columna { EspacioX = 10 };

                            renglon = new RenglonColumna()
                            {
                                Texto = String.Format("{0:#,##0.0000}", Convert.ToDecimal(detalleADibujar.Attribute("precioNeto").Value)),
                                EspacioX = 502,
                                MaximoNumeroDeCaracteres = 18,
                                Fuente = fuenteDerecha
                            };

                            columna.RenglonesColumna.Add(renglon);
                            detalle.Columnas.Add(columna);

                            columna = new Columna { EspacioX = 10 };
                            renglon = new RenglonColumna()
                            {
                                Texto = String.Format("{0:#,##0.00####}", Convert.ToDecimal(detalleADibujar.Attribute("importeNeto").Value)),
                                EspacioX = 589,
                                MaximoNumeroDeCaracteres = 19,
                                Fuente = fuenteDerecha
                            };

                            columna.RenglonesColumna.Add(renglon);

                            detalle.Columnas.Add(columna);
                            configuracion.Detalles.Add(detalle);
                            //}
                        }

                        var detalleNota = new Detalle();
                        var columnaNota = new Columna { EspacioX = 8 };
                        var fuenteIzq = new Fuente { Alineacion = Narancia.Pdf4.Enumeraciones.Alineacion.Izquierda,Tamano=9 };
                        var renglonNota = new RenglonColumna();

                        renglonNota = new RenglonColumna()
                        {
                            Texto = buffer[i]["nota"],
                            EspacioX = 4,
                            MaximoNumeroDeCaracteres = 108,
                            Fuente = fuenteIzq
                        };

                        columnaNota.RenglonesColumna.Add(renglonNota);
                        detalleNota.Columnas.Add(columnaNota);
                        configuracion.Detalles.Add(detalleNota);

                        detalleNota = new Detalle();
                        columnaNota = new Columna { EspacioX = 8 };
                        fuenteIzq = new Fuente { Alineacion = Narancia.Pdf4.Enumeraciones.Alineacion.Izquierda, Tamano = 9 };
                        renglonNota = new RenglonColumna();

                        renglonNota = new RenglonColumna()
                        {
                            Texto = buffer[i]["observaciones"],
                            EspacioX = 4,
                            MaximoNumeroDeCaracteres = 108,
                            Fuente = fuenteIzq
                        };

                        columnaNota.RenglonesColumna.Add(renglonNota);
                        detalleNota.Columnas.Add(columnaNota);
                        configuracion.Detalles.Add(detalleNota);
                        #endregion

                        //Configuracion QR
                        configuracion.CodigoQr.Alto = 85;
                        configuracion.CodigoQr.Ancho = 85;
                        configuracion.CodigoQr.Coordenadas.X = 7;
                        configuracion.CodigoQr.Coordenadas.Y = 520;
                        //configuracion.CodigoQr.Ruta = @"c:\PruebaParser\Iacna\qr\prueba.gif";

                        //configuracion.NombreFinal = @"c:\PruebaParser\Iacna\tareasFinales\" + Guid.NewGuid().ToString() + ".PDF";
                        configuracion.TextoRelleno = string.Empty;

                        //var pdf4 = new Pdf4();
                        //pdf4.Dibujar(configuracion);

                        //return;
                        string sRutaCFDI = _rutaCopiaCFDI;
                        string sNombreCopiaXml = string.Empty;

                        if (_lstRfcCopiaCFDI.Length > 0)
                        {
                            foreach (var Rfc in _lstRfcCopiaCFDI)
                            {
                                if (buffer[i]["rfcReceptor"].ToUpper() == Rfc.ToUpper())
                                {
                                    sNombreCopiaXml = buffer[i]["plantaEmisor"].Trim() + "_" + buffer[i]["numProveedor"].Trim() + "_" + buffer[i]["rfcReceptor"].Trim() + "_" +
                                                       buffer[i]["rfcEmisor"].Trim() + "_" + buffer[i]["serie"].Trim() + "_" + buffer[i]["folio"].Trim() + ".xml";
                                    sRutaCFDI = Path.Combine(sRutaCFDI, sNombreCopiaXml);
                                    break;
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(sNombreCopiaXml))
                            sRutaCFDI = string.Empty;

                        var configuracionTareasFinales = new ConfiguracionTareasFinales()
                        {
                            Addenda = sAddenda,
                            GeneradorPdf = null,
                            GeneradorPdf4 = configuracion,
                            Tareas = new List<TareaFinal>()
                            {
                                new TareaFinal()
                                {
                                    Argumentos = new List<string>()
                                    {
                                        //Nombre CFDI copia
                                         sRutaCFDI,
                                         sRutaCodigoBarras
                                    },
                                    PathEjecutable = AppDomain.CurrentDomain.BaseDirectory + "AsFactura.TareaExtra.exe"
                                },
                                new TareaFinal()
                                {
                                    Argumentos = new List<string>()
                                    {
                                        //Actualizar registro con Moneda y Tipo Cambio
                                         ConfigurationManager.AppSettings["ASFacturaDb"],
                                         buffer[i]["rfcReceptor"],
                                         buffer[i]["serie"].Trim(),
                                         buffer[i]["folio"].Trim(),
                                         buffer[i]["Total"].Trim()
                                    },
                                    PathEjecutable = AppDomain.CurrentDomain.BaseDirectory + "ASFactura.ActualizarMonedaTipoCambio.exe"
                                }
                            }
                        };

                        //Guardar Configuración de Tareas Finales
                        string guardadoCorrectamente = configuracionTareasFinales.GuardarConfiguracion(Path.Combine(tareasFinalesPath, "ctf_" + facturaGuid.ToString()));
                        #endregion                    
                }
            }
            catch (Exception e)
            {
                EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                ApplicationEventLog.Source = "AsFactura";
                ApplicationEventLog.WriteEntry(e.ToString(), EventLogEntryType.Error);
            }
            finally
            {
                try
                {
                    if(File.Exists(fiResult))
                        File.Delete(fiResult);

                    if (fiStd != null && File.Exists(fiStd.FullName))
                        File.Delete(fiStd.FullName);

                    if (fiXsl!= null && File.Exists(fiXsl.FullName))
                        File.Delete(fiXsl.FullName);

                    if (lBuffer != null)
                    {
                        lBuffer.Close();
                        lBuffer.Dispose();
                    }

                    //Limpieza de directorio de tareas finales
                    var directorioTareasFinales = new DirectoryInfo(tareasFinalesPath);
                    foreach (var archivo in directorioTareasFinales.EnumerateFiles())
                    {
                        if (archivo.CreationTime.Date < DateTime.Now.Date)
                        {
                            if (File.Exists(archivo.FullName))
                                File.Delete(archivo.FullName);
                        }
                    }

                    //Limpieza de directorio de tareas finales
                    directorioTareasFinales = new DirectoryInfo(_rutaCodigoBarras);
                    foreach (var archivo in directorioTareasFinales.EnumerateFiles())
                    {
                        if (archivo.CreationTime.Date < DateTime.Now.Date)
                        {
                            if (File.Exists(archivo.FullName))
                                File.Delete(archivo.FullName);
                        }
                    }
                }
                catch (Exception e)
                {
                    EventLog ApplicationEventLog = new System.Diagnostics.EventLog();
                    ApplicationEventLog.Source = "AsFactura";
                    ApplicationEventLog.WriteEntry(e.ToString(), EventLogEntryType.Error);
                }
            }
        }

#if DEBUG
        public void Start()
        {
            OnStart(null);
        }
#endif

        protected override void OnStart(string[] args)
        {
            _enEjecucion = true;
            _Origen.Start();
        }

        protected override void OnStop()
        {
            _enEjecucion = false;
        }
    }
}
