﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace ParserService
{
    public class ParallelQueue
    {
        readonly object _lock = new object();
        readonly Thread[] _threads;
        readonly Queue<Action> _tareas = new Queue<Action>();

        public ParallelQueue(int threads = 1)
        {
            _threads = new Thread[threads];

            for (var i = 0; i < threads; i++)
                (_threads[i] = new Thread(Consume) { Name = string.Format("Proceso {0}", i) }).Start();
        }

        public void Shutdown(bool waitForWorkers = true)
        {
            foreach (var t in _threads)
                EnqueueTask(null);

            if (waitForWorkers)
                foreach (var worker in _threads)
                    worker.Join();
        }

        public void EnqueueTask(Action item)
        {
            lock (_lock)
            {
                _tareas.Enqueue(item);
                Monitor.Pulse(_lock);
            }
        }

        void Consume()
        {
            while (true)
            {
                Action item;
                lock (_lock)
                {
                    while (_tareas.Count == 0) Monitor.Wait(_lock);
                    item = _tareas.Dequeue();
                }
                if (item == null) return;
                item();
            }
        }
    }
}