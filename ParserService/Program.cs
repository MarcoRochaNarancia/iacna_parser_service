﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ParserService
{
    static class Program
    {
        static void Main()
        {
#if !DEBUG
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new ParserService() 
                };
                ServiceBase.Run(ServicesToRun);
#else
            using (var service = new ParserService())
                service.Start();
#endif
        }
    }
}
